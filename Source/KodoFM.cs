﻿using System;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

using Kodo.Graphics;
using Kodo.Graphics.Style;
using Kodo.Lastfm;

using Kodo.Utilities.Configuration;
using Kodo.Utilities.Logging;

using KodoFM.Assets;
using KodoFM.Audio;

// ReSharper disable AssignNullToNotNullAttribute

//
// Assembly information.
//
[assembly: Guid( "3c85459f-408b-4db4-bfa8-58b6e3676adb" )]
[assembly: AssemblyTitle( "kodo-fm" )]
[assembly: AssemblyVersion( "0.2.19.*" )]
[assembly: AssemblyCopyright( "Copyright © Joonas Anttonen 2016" )]

namespace KodoFM
{
    public sealed class GlobalConfiguration : Configuration
    {
        [Gategory( "appearance" )]
        public AppearanceConfig Appearance { get; } = new AppearanceConfig();
        [Gategory( "playback" )]
        public PlaybackConfig Playback { get; } = new PlaybackConfig();
        [Gategory( "last.fm" )]
        public LastfmConfig Lastfm { get; } = new LastfmConfig();
        [Gategory( "hotkeys" )]
        public HotkeysConfig Hotkeys { get; } = new HotkeysConfig();

        public sealed class AppearanceConfig
        {
            [Configuration( "area" )]
            public Rectangle Area { get; set; } = Rectangle.FromXYWH( 500, 500, 1001, 808 );
            [Configuration( "pls-slimming" )]
            public bool Slimming { get; set; } = false;
            [Configuration( "pls-slimming-w" )]
            public int SlimmingWidth { get; set; } = 600;
            [Configuration( "state" )]
            public KodoFMMainWindow.UIStateMemory UIState { get; set; }
        }

        public sealed class PlaybackConfig
        {
            [Configuration( "active-list" )]
            public string ActiveList { get; set; } = AudioDatabase.MasterPlaylistName;
            [Configuration( "active-track" )]
            public int ActiveItem { get; set; } = AudioPlaylist.InvalidIndex;
            [Configuration( "volume" )]
            public float Volume { get; set; } = 0.5f;
            [Configuration( "mute" )]
            public bool Mute { get; set; }
            [Configuration( "repeat" )]
            public bool Repeat { get; set; }
            [Configuration( "shuffle" )]
            public bool Shuffle { get; set; }
            [Configuration( "loop" )]
            public bool Loop { get; set; } = true;
        }

        public sealed class LastfmConfig
        {
            [Configuration( "enabled" )]
            public bool Enabled { get; set; } = false;
            [Configuration( "integration" )]
            public bool Integration { get; set; } = false;
            [Configuration( "corrections" )]
            public bool Corrections { get; set; } = true;
            [Configuration( "scrobble" )]
            public float ThresholdOfScrobble { get; set; } = 0.6f;
            [Configuration( "nowplaying" )]
            public float ThresholdOfNowPlaying { get; set; } = 0.03f;
            [Configuration( "session" )]
            public LastfmSession Session { get; set; } = LastfmSession.Invalid;
        }

        public sealed class HotkeysConfig
        {
            [Configuration( "play" )]
            public Hotkey Play { get; set; } = new Hotkey( HotkeyName.Play, Key.MediaPlayPause );
            [Configuration( "hot-frw" )]
            public Hotkey Forward { get; set; } = new Hotkey( HotkeyName.Forward, Key.MediaNextTrack );
            [Configuration( "hot-brw" )]
            public Hotkey Backward { get; set; } = new Hotkey( HotkeyName.Backward, Key.MediaPreviousTrack );
            [Configuration( "volume-up" )]
            public Hotkey VolumeUp { get; set; } = new Hotkey( HotkeyName.VolumeUp, Key.VolumeUp );
            [Configuration( "volume-down" )]
            public Hotkey VolumeDown { get; set; } = new Hotkey( HotkeyName.VolumeDown, Key.VolumeDown );
            [Configuration( "hot-mute" )]
            public Hotkey VolumeMute { get; set; } = new Hotkey( HotkeyName.VolumeMute, Key.VolumeMute );
            [Configuration( "hot-shuffle" )]
            public Hotkey Shuffle { get; set; } = new Hotkey( HotkeyName.Shuffle, Key.S, HotkeyModifier.AltCtrl );
            [Configuration( "hot-repeat" )]
            public Hotkey Repeat { get; set; } = new Hotkey( HotkeyName.Repeat, Key.R, HotkeyModifier.AltCtrl );
        }
    }

    public class GlobalTextStyles
    {
        public static readonly string DefaultFontName = "Montserrat";

        public TextStyle Large { get; } = new TextStyle( DefaultFontName, 12 );
        public TextStyle LargeAndBold { get; } = new TextStyle( DefaultFontName, 12, FontWeight.Bold );
        public TextStyle Big { get; } = new TextStyle( DefaultFontName, 14 );
        public TextStyle BigAndBold { get; } = new TextStyle( DefaultFontName, 14, FontWeight.Bold );
        public TextStyle Small { get; } = new TextStyle( DefaultFontName, 10 );
        public TextStyle SmallAndBold { get; } = new TextStyle( DefaultFontName, 10, FontWeight.Bold );
    }

    public class GlobalIcons
    {
        public static readonly Size BigSize    = new Size( 42, 42 );
        public static readonly Size MediumSize = new Size( 32, 32 );
        public static readonly Size SmallSize  = new Size( 20, 20 );

        public BitmapAsset Play { get; } = new BitmapAsset( Resources.play106, BigSize );
        public BitmapAsset Backward { get; } = new BitmapAsset( Resources.rewind45, MediumSize );
        public BitmapAsset Forward { get; } = new BitmapAsset( Resources.fast46, MediumSize );
        public BitmapAsset Shuffle { get; } = new BitmapAsset( Resources.shuffle24, SmallSize );
        public BitmapAsset Repeat { get; } = new BitmapAsset( Resources.refresh55, SmallSize );
        public BitmapAsset Pause { get; } = new BitmapAsset( Resources.pause44, BigSize );
        public BitmapAsset VolumeMute { get; } = new BitmapAsset( Resources.Volume, SmallSize );
        public BitmapAsset Back { get; } = new BitmapAsset( Resources.left216, MediumSize );
        public BitmapAsset Account { get; } = new BitmapAsset( Resources.Account, MediumSize );
        public BitmapAsset LoveEmpty { get; } = new BitmapAsset( Resources.LoveEmpty, MediumSize );
        public BitmapAsset LoveFull { get; } = new BitmapAsset( Resources.LoveFull, MediumSize );
        public BitmapAsset Home { get; } = new BitmapAsset( Resources.Home, SmallSize );
        public BitmapAsset Add { get; } = new BitmapAsset( Resources.Plus, MediumSize );
        public BitmapAsset Edit { get; } = new BitmapAsset( Resources.Edit, SmallSize );
        public BitmapAsset Delete { get; } = new BitmapAsset( Resources.Delete, MediumSize );
        public BitmapAsset Right { get; } = new BitmapAsset( Resources.RightArrow, MediumSize );
        public BitmapAsset Check { get; } = new BitmapAsset( Resources.Check, MediumSize );
        public BitmapAsset Search { get; } = new BitmapAsset( Resources.Search, SmallSize );
        public BitmapAsset Settings { get; } = new BitmapAsset( Resources.settings49, SmallSize );
        public BitmapAsset Now { get; } = new BitmapAsset( Resources.music123, SmallSize );
        public BitmapAsset Tracks { get; } = new BitmapAsset( Resources.book_1, SmallSize );
        public BitmapAsset Artists { get; } = new BitmapAsset( Resources.music_6, SmallSize );
        public BitmapAsset Albums { get; } = new BitmapAsset( Resources.music, SmallSize );
        public BitmapAsset Playlists { get; } = new BitmapAsset( Resources.music_4353451, SmallSize );
        public BitmapAsset Update { get; } = new BitmapAsset( Resources.round52, SmallSize );
        public BitmapAsset AddArt { get; } = new BitmapAsset( Resources.add180, MediumSize );
        public BitmapAsset ThreeDots { get; } = new BitmapAsset( Resources.three170, MediumSize );
    }

    public class Global
    {
        //
        // Application paths.
        //
        public static readonly string ApplicationName         = "kodo-fm";
        public static readonly string ApplicationWorkingPath  = $"{ Environment.CurrentDirectory }";

        public static readonly string ApplicationConfigPath   = $"{ ApplicationWorkingPath }\\{ ApplicationName }.ini";
        public static readonly string ApplicationDatabasePath = $"{ ApplicationWorkingPath }\\{ ApplicationName }.db";

        public static readonly string ApplicationCachePath    = $"{ ApplicationWorkingPath }\\Cache";

        public static readonly string ApplicationLogPath      = $"{ ApplicationCachePath }\\{ DateTime.Now.Ticks }.log";
        public static readonly string ApplicationUpdatePath   = $"{ ApplicationCachePath }\\{ ApplicationName }-update.zip";
        public static readonly string ApplicationUpdaterPath  = $"{ ApplicationCachePath }\\{ ApplicationName }-update.exe";
        public static readonly string ApplicationUpdateUrl    = "http://kodo-fm.duckdns.org/";

        /// <summary> 
        /// The global log. 
        /// </summary>
        public static Log Log { get; } = new Log();

        /// <summary>
        /// The global configuration.
        /// </summary>
        public static GlobalConfiguration Config { get; } = new GlobalConfiguration();

        /// <summary>
        /// The global icons.
        /// </summary>
        public static GlobalIcons Icons { get; } = new GlobalIcons();

        /// <summary>
        /// The global <see cref="TextStyle"/>s.
        /// </summary>
        public static readonly GlobalTextStyles TextStyles = new GlobalTextStyles();

        /// <summary>
        /// Run the application updater.
        /// </summary>
        /// <returns>Did the updater start succesfully?</returns>
        public static bool RunUpdate()
        {
            if ( File.Exists( ApplicationUpdaterPath ) )
            {
                var processStartInfo = new System.Diagnostics.ProcessStartInfo();
                processStartInfo.FileName = ApplicationUpdaterPath;
                processStartInfo.Arguments = "wait";
                processStartInfo.WorkingDirectory = ApplicationCachePath;
                System.Diagnostics.Process.Start( processStartInfo );
                return true;
            }

            return false;
        }

        /// <summary>
        /// Fetch an update for the application.
        /// </summary>
        /// <returns>Did we get an update?</returns>
        public static async Task<bool> FetchUpdate()
        {
            try
            {
                var requestVersion = Assembly.GetExecutingAssembly().GetName().Version;
                var requestUri = $"{ ApplicationUpdateUrl }update?ver={ requestVersion.Major }.{ requestVersion.Minor }.{ requestVersion.Build }";
                var request = WebRequest.CreateHttp( requestUri );
                var response = (HttpWebResponse)await request.GetResponseAsync();

                if ( response != null && response.StatusCode == HttpStatusCode.OK )
                {
                    using ( var fileStream = new FileStream( ApplicationUpdatePath, FileMode.Create ) )
                    using ( var netStream = response.GetResponseStream() )
                    {
                        if ( netStream != null )
                        {
                            await netStream.CopyToAsync( fileStream );
                        }
                    }

                    using ( var archive = ZipFile.Open( ApplicationUpdatePath, ZipArchiveMode.Update ) )
                    {
                        var entry = archive.GetEntry( Path.GetFileName( ApplicationUpdaterPath ) );
                        entry.ExtractToFile( ApplicationUpdaterPath, true );
                        entry.Delete();
                    }

                    Log.CallerInformation( "We have a new update!" );

                    return true;
                }
            }
            catch ( WebException we )
            {
                Log.CallerWarning( we.Message );
            }
            catch ( Exception e )
            {
                Log.CallerError( e.Message );
            }

            // There might still be an old update.
            if ( File.Exists( ApplicationUpdaterPath ) &&
                 File.Exists( ApplicationUpdatePath ) )
            {
                Log.CallerInformation( "We have an old pending update!" );
                return true;
            }

            return false;
        }

        [STAThread]
        static void Main( string[] args )
        {
            using ( var mutex = KodoFMUtil.GetGlobalMutex() )
            {
                // Only continue starting up if we are a unique instance or we have been told not to care.
                if ( !KodoFMUtil.IsUniqueInstance( mutex ) && !KodoFMUtil.HasArg( args, "AllowMultiInstance" ) )
                    return;

                try
                {
                    // Make sure the cache folder exists.
                    if ( !Directory.Exists( ApplicationCachePath ) )
                        Directory.CreateDirectory( ApplicationCachePath );

                    // Enter the application proper.
                    KodoMain();
                }
                catch ( Exception e )
                {
                    OnUnhandledException( e );
                }

                // Dump the global log if requested.
                if ( KodoFMUtil.HasArg( args, "CanNeverHaveTooMuchLogging" ) )
                {
                    Log.Dump( ApplicationLogPath, true );
                }
            }
        }

        static void KodoMain()
        {
            // Read the configuration.
            Config.Read( ApplicationConfigPath );

            using ( var manager = new WindowManager( OnUnhandledException ) )
            {
                //
                // Load
                //

                AudioPlayer player;
                AudioDatabase database;
                LoadAudio( out database, out player );

                LastfmInterface lastfm;
                LoadLastfm( out lastfm );

                GlobalHotkeys hotkeys;
                LoadHotkeys( out hotkeys );

                var window = LoadWindow( manager, database, player, lastfm, hotkeys );

                //
                // Run
                //

                manager.Run( window );

                //
                // Save
                //

                SaveAudio( database, player );
                SaveHotkeys( hotkeys );
                SaveLastfm( lastfm );
                SaveWindow( window );
            }

            // Save the configuration.
            Config.Write( ApplicationConfigPath );
        }

        static void OnUnhandledException( Exception exception )
        {
            Log.Error( exception );
            Log.Dump( ApplicationLogPath );
        }

        static void LoadLastfm( out LastfmInterface lastfm )
        {
            // Configure the last.fm global properties.
            LastfmGlobal.AppName = ApplicationName;
            LastfmGlobal.AppKey = "f6c7bb9a8d7ee7de5756f9574bee409e";
            LastfmGlobal.AppSecret = "c1d3368a8e5fc39f8984f7aa1f2b4440";
            LastfmGlobal.TimeBetweenRequests = TimeSpan.FromSeconds( 5 );

            lastfm = new LastfmInterface( Config.Lastfm.Session )
            {
                LimitOfScrobble = Config.Lastfm.ThresholdOfScrobble,
                LimitOfNowPlaying = Config.Lastfm.ThresholdOfNowPlaying
            };
        }

        static void SaveLastfm( LastfmInterface lastfm )
        {
            Config.Lastfm.Session = lastfm.Session;
            Config.Lastfm.ThresholdOfScrobble = lastfm.LimitOfScrobble;
            Config.Lastfm.ThresholdOfNowPlaying = lastfm.LimitOfNowPlaying;
        }

        static void LoadHotkeys( out GlobalHotkeys hotkeys )
        {
            hotkeys = new GlobalHotkeys();
            hotkeys.Register( Config.Hotkeys.Play );
            hotkeys.Register( Config.Hotkeys.Forward );
            hotkeys.Register( Config.Hotkeys.Backward );
            hotkeys.Register( Config.Hotkeys.Repeat );
            hotkeys.Register( Config.Hotkeys.Shuffle );
            hotkeys.Register( Config.Hotkeys.VolumeDown );
            hotkeys.Register( Config.Hotkeys.VolumeUp );
            hotkeys.Register( Config.Hotkeys.VolumeMute );
        }

        static void SaveHotkeys( GlobalHotkeys hotkeys )
        {
            Config.Hotkeys.Play = hotkeys[ HotkeyName.Play ];
            Config.Hotkeys.Forward = hotkeys[ HotkeyName.Forward ];
            Config.Hotkeys.Backward = hotkeys[ HotkeyName.Backward ];
            Config.Hotkeys.Repeat = hotkeys[ HotkeyName.Repeat ];
            Config.Hotkeys.Shuffle = hotkeys[ HotkeyName.Shuffle ];
            Config.Hotkeys.VolumeDown = hotkeys[ HotkeyName.VolumeDown ];
            Config.Hotkeys.VolumeUp = hotkeys[ HotkeyName.VolumeUp ];
            Config.Hotkeys.VolumeMute = hotkeys[ HotkeyName.VolumeMute ];
        }

        static void LoadAudio( out AudioDatabase db, out AudioPlayer player )
        {
            //system = new AudioEventSystem( System.Threading.SynchronizationContext.Current );
            AudioEventSystem.SetSynchronizationContext( System.Threading.SynchronizationContext.Current );
            AudioCache.AlbumArtCache = ApplicationCachePath;
            db = new AudioDatabase( ApplicationDatabasePath );

            AudioPlaylist lastPlaylist;
            var lastPlaylistName = Config.Playback.ActiveList;
            var lastPlaylistActiveIndex = Config.Playback.ActiveItem;

            if ( !db.ContainsPlaylist( lastPlaylistName ) )
            {
                if ( lastPlaylistName.StartsWith( AudioDatabase.ArtistPrefix ) )
                {
                    var artistName = lastPlaylistName.Substring( AudioDatabase.ArtistPrefix.Length );

                    if ( db.HasArtist( artistName ) )
                    {
                        lastPlaylist = db.CreatePlaylist( lastPlaylistName );
                        lastPlaylist.AddRange( db.GetTracksFromArtist( artistName ) );
                        lastPlaylist.SetActiveIndex( lastPlaylistActiveIndex );
                    }
                    else
                    {
                        lastPlaylist = db.GetPlaylist( AudioDatabase.MasterPlaylistName );
                    }
                }
                else if ( lastPlaylistName.StartsWith( AudioDatabase.AlbumPrefix ) )
                {
                    var albumName = lastPlaylistName.Substring( AudioDatabase.AlbumPrefix.Length );

                    if ( db.HasAlbum( albumName ) )
                    {
                        lastPlaylist = db.CreatePlaylist( lastPlaylistName );
                        lastPlaylist.AddRange( db.GetTracksFromAlbum( albumName ) );
                        lastPlaylist.SetActiveIndex( lastPlaylistActiveIndex );
                    }
                    else
                    {
                        lastPlaylist = db.GetPlaylist( AudioDatabase.MasterPlaylistName );
                    }
                }
                else
                {
                    lastPlaylist = db.GetPlaylist( AudioDatabase.MasterPlaylistName );
                }
            }
            else
            {
                lastPlaylist = db.GetPlaylist( lastPlaylistName );
                lastPlaylist.SetActiveIndex( lastPlaylistActiveIndex );
            }

            // Create the player and load all appropriate settings.
            player = new AudioPlayer
            {
                Playlist = lastPlaylist,
                Volume = Config.Playback.Volume,
                Mute = Config.Playback.Mute,
                Repeat = Config.Playback.Repeat,
                Shuffle = Config.Playback.Shuffle,
                Looping = Config.Playback.Loop
            };
        }

        static void SaveAudio( AudioDatabase db, AudioPlayer player )
        {
            db.Write();

            Config.Playback.Volume = player.Volume;
            Config.Playback.Mute = player.Mute;
            Config.Playback.Shuffle = player.Shuffle;
            Config.Playback.Repeat = player.Repeat;

            Config.Playback.ActiveList = player.Playlist.Name;
            Config.Playback.ActiveItem = player.Playlist.GetActiveIndex();
        }

        static KodoFMMainWindow LoadWindow( WindowManager manager, AudioDatabase db, AudioPlayer player, LastfmInterface lfmInterface, GlobalHotkeys binds )
        {
            var windowSettings = new WindowSettings
            {
                AcceptFiles = true,
                Area = Config.Appearance.Area,
                Name = ApplicationName
            };
            var window = new KodoFMMainWindow( manager, windowSettings, db, player, lfmInterface, binds )
            {
                TextStyle = TextStyles.Large,
                StateOfUI = Config.Appearance.UIState
            };
            return window;
        }

        static void SaveWindow( KodoFMMainWindow wnd )
        {
            Config.Appearance.Area = wnd.Area;
            Config.Appearance.UIState = wnd.StateOfUI;
        }
    }
}
