﻿using System;
using System.Threading;
using System.Reflection;
using System.Runtime.InteropServices;

namespace KodoFM
{
    public static class KodoFMUtil
    {
        public static bool HasArg( string[] args, params string[] commands )
        {
            if ( args != null && args.Length > 0 && commands != null && commands.Length > 0 )
            {
                foreach ( var arg in args )
                {
                    foreach ( var command in commands )
                    {
                        if ( arg.Equals( "-" + command, StringComparison.OrdinalIgnoreCase ) )
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        public static Mutex GetGlobalMutex()
        {
            var name = ((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes( typeof( GuidAttribute ), false ).GetValue( 0 )).Value.ToString();
            return new Mutex( false, name );
        }

        public static bool IsUniqueInstance( Mutex mutex )
        {
            try
            { if ( !mutex.WaitOne( 0, false ) ) { return false; } }
            catch ( AbandonedMutexException ) { }

            return true;
        }

        public static int Clamp( int value, int min, int max )
        {
            return Math.Max( Math.Min( value, max ), min );
        }

        public static float Clamp( float value, float min, float max )
        {
            return Math.Max( Math.Min( value, max ), min );
        }
    }
}
