﻿using System;
using System.Collections.Generic;

using Kodo.Graphics.Style;

namespace KodoFM
{
    /// <summary> 
    /// The method that receives the hotkey event.
    /// </summary>
    public delegate void OnHotkeyDelegate( Hotkey key );

    /// <summary> 
    /// The different actions of the hotkeys. 
    /// </summary>
    public enum HotkeyName
    {
        Play = 1,
        Forward = 2,
        Backward = 3,
        VolumeUp = 4,
        VolumeDown = 5,
        VolumeMute = 6,
        Shuffle = 7,
        Repeat = 8,
        Stop = 9,
        Pause = 10,
        Back = 11,
        Account = 12,
        LoveEmpty = 13,
        LoveFull = 14,
        Home = 15,
        Add = 16,
        Edit = 17,
        Delete = 18,
        Right = 19,
        Check = 20,
        Search = 21,
        Settings = 22,

        Tracks = 23,
        Artists = 24,
        Albums = 25,
        Playlists = 26,
        Now = 27,

        Update = 28,

        AddArt = 29,

        ThreeDots = 30,
    }

    /// <summary> 
    /// The modifiers of the hotkeys. 
    /// </summary>
    public enum HotkeyModifier
    {
        None = 0,
        Alt = 0x0001,
        Ctrl = 0x0002,
        NoRepeat = 0x4000,
        Shift = 0x0004,
        Win = 0x0008,

        AltCtrl = Alt | Ctrl,
        AltShift = Alt | Shift,
    };

    /// <summary> 
    /// Represents a hotkey.
    /// </summary>
    public struct Hotkey
    {
        public HotkeyName Action { get; set; }
        public Key Key { get; set; }
        public HotkeyModifier Modifer { get; set; }

        public Hotkey( HotkeyName id, Key key, HotkeyModifier modifier = HotkeyModifier.None )
        {
            Action = id;
            Key = key;
            Modifer = modifier;
        }

        public override string ToString()
        {
            return $"{ (int)Action },{ (int)Key },{ (int)Modifer }";
        }
    }

    public class GlobalHotkeys
    {
        // ________________ Extern ___________________________________________________________

        [System.Runtime.InteropServices.DllImport( "User32" )]
        extern static bool RegisterHotKey( IntPtr hWnd, int id, uint modifiers, uint vk );

        [System.Runtime.InteropServices.DllImport( "User32" )]
        extern static bool UnregisterHotKey( IntPtr hWnd, int id );

        // ________________ Fields ___________________________________________________________

        Listener listener;
        Dictionary<HotkeyName, Hotkey> hotkeys;

        // ________________ Properties ___________________________________________________________

        /// <summary> 
        /// Occurs when one of the hotkeys is triggered. 
        /// </summary>
        public event OnHotkeyDelegate OnKey;

        /// <summary> 
        /// Get the specified hotkey. 
        /// </summary>
        /// <param name="action">Identifier of the hotkey.</param>
        public Hotkey this[ HotkeyName action ]
        {
            get { return hotkeys[ action ]; }
        }

        // ________________ Publics ___________________________________________________________

        /// <summary> 
        /// Create a new <see cref="GlobalHotkeys"/>. 
        /// </summary>
        public GlobalHotkeys()
        {
            listener = new Listener( OnMessage );
            hotkeys = new Dictionary<HotkeyName, Hotkey>();
        }

        /// <summary> 
        /// Register a hotkey.
        /// </summary>
        /// <param name="key">The hotkey.</param>
        public bool Register( Hotkey key )
        {
            var succeeded = false;

            if ( !hotkeys.ContainsKey( key.Action ) )
            {
                succeeded = RegisterHotKey( listener.Handle, (int)key.Action, (uint)key.Modifer, (uint)key.Key );

                if ( succeeded )
                {
                    hotkeys.Add( key.Action, key );
                }
            }

            return succeeded;
        }

        /// <summary> 
        /// Unregister a hotkey. 
        /// </summary>
        /// <param name="key">The hotkey.</param>
        public bool Unregister( Hotkey key )
        {
            var succeeded = false;

            if ( hotkeys.ContainsKey( key.Action ) )
            {
                succeeded = UnregisterHotKey( listener.Handle, (int)key.Action );

                if ( succeeded )
                {
                    hotkeys.Remove( key.Action );
                }
            }

            return succeeded;
        }

        /// <summary> 
        /// Unregister all hotkeys. 
        /// </summary>
        public void UnregisterAll()
        {
            foreach ( var bind in hotkeys.Values )
            {
                Unregister( bind );
            }
        }

        // ________________ Message Filter ___________________________________________________________

        bool OnMessage( ref Message msg )
        {
            const int WM_HOTKEY = 0x0312;

            if ( msg.Msg == WM_HOTKEY )
            {
                if ( OnKey != null )
                {
                    OnKey( hotkeys[ (HotkeyName)((int)msg.WParam) ] );
                    return true;
                }
            }

            return false;
        }
    }
}
