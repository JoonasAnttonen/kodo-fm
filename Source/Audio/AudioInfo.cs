﻿using System;
using System.Text;

namespace KodoFM.Audio
{
    public class AudioInfo
    {
        public static readonly string UnknownArtistString = "Unknown Artist";
        public static readonly string UnknownAlbumString = "Unknown Album";

        string title;
        string artist;
        string album;
        string year;
        string track;
        string genre;
        string comment;

        void SetTextField( string value, ref string field, string defaultValue = "" )
        {
            if ( string.IsNullOrWhiteSpace( value ) )
            {
                field = defaultValue;
            }
            else
            {
                field = value.Trim();
            }
        }

        public AudioID ID { get; private set; }

        public string Path
        {
            get { return ID.Data; }
            set { ID = new AudioID( value ); }
        }
        public string Title
        {
            get { return title; }
            set { SetTextField( value, ref title ); }
        }
        public string Artist
        {
            get { return artist; }
            set { SetTextField( value, ref artist, UnknownArtistString ); }
        }
        public string Album
        {
            get { return album; }
            set { SetTextField( value, ref album, UnknownAlbumString ); }
        }
        public string Year
        {
            get { return year; }
            set { SetTextField( value, ref year ); }
        }
        public string Track
        {
            get { return track; }
            set { SetTextField( value, ref track ); }
        }
        public string Genre
        {
            get { return genre; }
            set { SetTextField( value, ref genre ); }
        }
        public string Comment
        {
            get { return comment; }
            set { SetTextField( value, ref comment ); }
        }

        public uint Length
        {
            get;
            set;
        }

        public bool Equals( TagLib.Tag tag )
        {
            return Title == tag.Title &&
                    Artist == tag.FirstPerformer &&
                    Album == tag.Album &&
                    Year == tag.Year.ToString() &&
                    Track == tag.Track.ToString() &&
                    Genre == tag.FirstGenre &&
                    Comment == tag.Comment;
        }

        public bool Equals( AudioInfo other )
        {
            return ID == other.ID &&
                    Title == other.Title &&
                    Artist == other.Artist &&
                    Album == other.Album &&
                    Year == other.Year &&
                    Track == other.Track &&
                    Genre == other.Genre &&
                    Comment == other.Comment;
        }

        /// <summary> 
        /// Create a new <see cref="AudioInfo"/> by copying from an existing one. 
        /// </summary>
        /// <param name="other"><see cref="AudioInfo"/> to copy from.</param>
        public AudioInfo( AudioInfo other )
        {
            Length = other.Length;
            ID = other.ID;
            Title = other.Title;
            Artist = other.Artist;
            Album = other.Album;
            Year = other.Year;
            Track = other.Track;
            Genre = other.Genre;
            Comment = other.Comment;
        }

        public static AudioInfo Copy( AudioInfo other )
        {
            return new AudioInfo( other );
        }

        /// <summary> 
        /// Create a new <see cref="AudioInfo"/>. 
        /// </summary>
        public AudioInfo()
        {
            Title = string.Empty;
            Artist = string.Empty;
            Album = string.Empty;
            Year = string.Empty;
            Track = string.Empty;
            Genre = string.Empty;
            Comment = string.Empty;
        }

        /// <summary> 
        /// Create a new <see cref="AudioInfo"/>.
        /// </summary>
        public AudioInfo( string path )
            : this()
        {
            Path = path;
        }

        public AudioItem ToAudioItem()
        {
            return new AudioItem( Path,
                                  MakeTitleString(),
                                  MakeFieldString( Artist, UnknownArtistString ),
                                  MakeFieldString( Album, UnknownAlbumString ),
                                  MakeLengthString(),
                                  Track,
                                  AudioItemFlag.Exists | AudioItemFlag.Loaded );
        }

        public string MakeTitleString()
        {
            return string.IsNullOrEmpty( Title ) ? System.IO.Path.GetFileName( Path ) : Title;
        }

        public string MakeFieldString( string field, string alt )
        {
            return string.IsNullOrEmpty( field ) ? alt : field;
        }

        public static string MillisecondsToString( uint milliseconds ) { return TimeSpanToString( TimeSpan.FromMilliseconds( milliseconds ) ); }
        public static string TimeSpanToString( TimeSpan time )
        {
            var builder = new StringBuilder();

            if ( time.Minutes < 10 )
                builder.Append( '0' );

            builder.Append( time.Minutes );
            builder.Append( ':' );

            if ( time.Seconds < 10 )
                builder.Append( '0' );

            builder.Append( time.Seconds );

            return builder.ToString();
        }

        public string MakeLengthString()
        {
            return MillisecondsToString( Length );
        }
    }
}
