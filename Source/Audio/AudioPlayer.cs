﻿using System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using FMOD;
using Kodo.Graphics.Style;
// ReSharper disable AssignNullToNotNullAttribute

namespace KodoFM.Audio
{
    public class AudioException : Exception
    {
        public AudioException( string message )
            : base( message )
        {
        }
    }

    public enum AudioStatus
    {
        Unknown,
        BadFile,
        Ok,
        CanNotFind,
        CanNotOpen,
        CanNotRead,
        CanNotSeek,
        CanNotWrite
    }

    public class AudioStream
    {
        public static readonly AudioStream Empty = new AudioStream();

        public AudioID ID { get { return Info.ID; } }
        public AudioInfo Info { get; set; }
        public bool Playing { get; set; }
        public uint Played { get; set; }
        public uint Position { get; set; }
        public uint Length { get { return Info.Length; } }
        public DateTime Timestamp { get; set; }

        public string Name
        {
            get
            {
                var builder = new StringBuilder( Info.Title.Length + 3 + Info.Artist.Length );
                builder.Append( Info.MakeTitleString() );
                builder.Append( " | " );
                builder.Append( Info.MakeFieldString( Info.Artist, AudioInfo.UnknownArtistString ) );
                return builder.ToString();
            }
        }

        public string Path { get { return Info.Path; } }

        public AudioStream()
        {
            Info = new AudioInfo();
        }
    }

    static class AudioPlayerExtensions
    {
        public static void Raise( this AudioPlayer.StreamDelegate d, AudioStream a ) { if ( d != null ) d( a ); }
        public static void Raise( this AudioPlayer.StreamErrorDelegate d, AudioID a, AudioStatus b, string c ) { if ( d != null ) d( a, b, c ); }
    }

    public sealed class AudioPlayer : IDisposable
    {
        /// <summary>
        /// The file extensions of the supported audio types.
        /// </summary>
        public static readonly string[] SupportedTypes = { ".mp3", ".flac", ".ogg", ".wma", ".wav" };

        public delegate void StreamErrorDelegate( AudioID id, AudioStatus status, string detail );
        public delegate void StreamDelegate( AudioStream stream );

        enum AudioPlayerState
        {
            Idle = 0,
            Active,
            ActiveButPaused
        }

        // ________________ Fields ___________________________________________________________

        AudioStream stream = AudioStream.Empty;
        AudioPlaylist playlist = AudioPlaylist.Empty;
        AudioPlaylist queue = AudioPlaylist.Empty;
        AudioPlayerState state;

        float volume;
        bool looping;
        bool mute;
        bool repeat;
        bool shuffle;
        int shuffleIndex;
        int[] shuffleIndices = new int[ 0 ];

        DSP fmodDSP;
        ChannelGroup fmodChannelGroup;
        FMOD.System fmodSystem;
        Sound fmodSound;
        Channel fmodChannel;

        Timer streamTimer;
        Counter streamCounter;

        // ________________ Events ___________________________________________________________

        /// <summary> 
        /// Occurs when a stream encounters an error. 
        /// </summary>
        public event StreamErrorDelegate OnStreamError;
        /// <summary> 
        /// Occurs when a new stream has begun. 
        /// </summary>
        public event StreamDelegate OnStreamBegin;
        /// <summary> 
        /// Occurs when the current stream is updated. 
        /// </summary>
        public event StreamDelegate OnHeartbeat;
        /// <summary> 
        /// Occurs when the current stream has ended. 
        /// </summary>
        public event StreamDelegate OnStreamEnd;

        // ________________ Attributes ___________________________________________________________

        /// <summary>
        /// Get the current stream.
        /// </summary>
        public AudioStream Stream
        {
            get
            {
                Update();
                return stream;
            }
        }

        /// <summary>
        /// Indicates whether or not the current <see cref="AudioStream"/> is playing from the queue.
        /// </summary>
        public bool PlayingFromQueue
        {
            get;
            private set;
        }

        /// <summary>
        /// Enable or disable mute.
        /// </summary>
        public bool Mute
        {
            get { return mute; }
            set
            {
                mute = value;
                ErrorIfFailed( fmodChannelGroup.setMute( mute ) );
            }
        }

        /// <summary>
        /// Get or set the volume.
        /// </summary>
        public float Volume
        {
            get { return volume; }
            set
            {
                volume = value;
                ErrorIfFailed( fmodChannelGroup.setVolume( volume ) );
            }
        }

        /// <summary>
        /// Enable or disable track repeating.
        /// </summary>
        public bool Repeat
        {
            get { return repeat; }
            set { repeat = value; }
        }

        /// <summary>
        /// Enable or disable playlist looping.
        /// </summary>
        public bool Looping
        {
            get { return looping; }
            set { looping = value; }
        }

        /// <summary>
        /// Enable or disable playlist shuffling. Will re-shuffle on every enable.
        /// </summary>
        public bool Shuffle
        {
            get { return shuffle; }
            set
            {
                shuffle = value;

                // Everytime the shuffle is enabled, re-shuffle the list.
                if ( shuffle )
                {
                    UpdateShuffle();
                }
            }
        }

        /// <summary>
        /// Get or set the current playlist.
        /// </summary>
        public AudioPlaylist Playlist
        {
            get { return playlist; }
            set
            {
                if ( playlist != value )
                {
                    if ( playlist != null )
                    {
                        playlist.OnModified -= OnPlaylistModified;
                        playlist.OnActivated -= OnPlaylistActivated;
                    }

                    playlist = value;

                    if ( playlist != null )
                    {
                        playlist.OnModified += OnPlaylistModified;
                        playlist.OnActivated += OnPlaylistActivated;

                        if ( stream == AudioStream.Empty )
                        {
                            Play( playlist.GetActiveItem().Path, true );
                        }
                    }
                    else
                    {
                        playlist = AudioPlaylist.Empty;
                    }

                    UpdateShuffle();
                }
            }
        }

        // ________________ Publics ___________________________________________________________

        /// <summary>
        /// Create a new instance of <see cref="AudioPlayer"/>.
        /// </summary>
        public AudioPlayer()
        {
            if ( Factory.System_Create( out fmodSystem ) != RESULT.OK )
                throw new AudioException( "Failed the most basic initialization!" );

            ErrorIfFailed( fmodSystem.init( 10, INITFLAGS.NORMAL, IntPtr.Zero ) );
            ErrorIfFailed( fmodSystem.createDSPByType( DSP_TYPE.FFT, out fmodDSP ) );
            ErrorIfFailed( fmodSystem.createChannelGroup( "kodo-fm", out fmodChannelGroup ) );
            ErrorIfFailed( fmodDSP.setParameterInt( (int)DSP_FFT.WINDOWSIZE, 512 ) );
            ErrorIfFailed( fmodChannelGroup.addDSP( 0, fmodDSP ) );
            ErrorIfFailed( fmodChannelGroup.setVolume( volume ) );
            ErrorIfFailed( fmodChannelGroup.setMute( mute ) );

            streamCounter = new Counter();
            streamTimer = new Timer( Heartbeat );
            streamTimer.Interval = 300;
        }

        void IDisposable.Dispose()
        {
            //
            // Release all FMOD resources.
            //

            fmodChannelGroup?.release();
            fmodSound?.release();
            fmodSystem?.release();
        }

        AudioStatus OpenStream( string path )
        {
            switch ( fmodSystem.createStream( path, MODE._2D | MODE.MPEGSEARCH, out fmodSound ) )
            {
                case RESULT.OK:
                    return AudioStatus.Ok;
                case RESULT.ERR_FILE_NOTFOUND:
                    return AudioStatus.CanNotFind;
                case RESULT.ERR_FORMAT:
                case RESULT.ERR_FILE_BAD:
                    return AudioStatus.BadFile;
                default:
                    return AudioStatus.Unknown;
            }
        }

        /// <summary>
        /// Play the specified track.
        /// </summary>
        /// <param name="path">Path of the track.</param>
        /// <param name="startPaused">Whether or not have the track start as paused.</param>
        /// <param name="startPosition">Track start position.</param>
        public void Play( string path, bool startPaused = false, uint startPosition = 0 )
        {
            if ( StateIsNot( AudioPlayerState.Idle ) )
                Stop();

            if ( string.IsNullOrEmpty( path ) )
                return;

            var streamStatus = OpenStream( path );

            if ( streamStatus != AudioStatus.Ok )
            {
                OnStreamError.Raise( new AudioID( path ), streamStatus, "failed to open the stream" );
                return;
            }

            var streamInfo = AudioDatabase.ReadStreamInfo( path );
            var streamLengthReliable = 0u;

            ErrorIfFailed( fmodSound.getLength( out streamLengthReliable, TIMEUNIT.MS ) );

            stream = new AudioStream();
            stream.Timestamp = DateTime.Now;
            stream.Info = streamInfo;
            stream.Info.Length = streamLengthReliable;
            stream.Position = startPosition;

            // Play the sound.
            ErrorIfFailed( fmodSystem.playSound( fmodSound, fmodChannelGroup, true, out fmodChannel ) );

            SetState( AudioPlayerState.ActiveButPaused );

            if ( startPosition > 0u )
            {
                // Seek to start position.
                ErrorIfFailed( fmodChannel.setPosition( startPosition, TIMEUNIT.MS ) );
            }

            if ( !startPaused )
            {
                // Start playback.
                Play();
            }

            Update();

            OnStreamBegin.Raise( stream );

            streamTimer.Start();
        }

        /// <summary>
        /// Toggle playback.
        /// </summary>
        public bool TogglePlayback()
        {
            if ( StateIs( AudioPlayerState.Active ) )
            {
                Pause();
            }
            else
            {
                Play();
            }

            return StateIs( AudioPlayerState.Active );
        }

        public void Replay()
        {
            PlayNext( 0 );
        }

        /// <summary>
        /// Start playback.
        /// </summary>
        public void Play()
        {
            switch ( state )
            {
                case AudioPlayerState.Active:
                    return;
                case AudioPlayerState.ActiveButPaused:
                    ErrorIfFailed( fmodChannel.setPaused( false ) );
                    streamCounter.Start();
                    SetState( AudioPlayerState.Active );
                    break;
                case AudioPlayerState.Idle:
                    PlayNext( 1 );
                    break;
            }
        }

        /// <summary>
        /// Pause playback.
        /// </summary>
        public void Pause()
        {
            if ( StateIsNot( AudioPlayerState.Active ) )
                return;

            ErrorIfFailed( fmodChannel.setPaused( true ) );
            streamCounter.Stop();
            SetState( AudioPlayerState.ActiveButPaused );
        }

        /// <summary>
        /// Stop playback.
        /// </summary>
        public void Stop()
        {
            if ( StateIs( AudioPlayerState.Idle ) )
                return;

            fmodChannel.setPaused( true );
            fmodChannel.stop();

            ErrorIfFailed( fmodSound.release() );

            streamTimer.Stop();
            streamCounter.Stop();

            SetState( AudioPlayerState.Idle );

            OnStreamEnd.Raise( stream );

            stream = AudioStream.Empty;
            streamCounter.Reset();
        }

        /// <summary>
        /// Play the next track from the current <see cref="AudioPlaylist"/>.
        /// </summary>
        public void Next()
        {
            if ( StateIs( AudioPlayerState.Idle ) && !looping )
                return;

            PlayNext( 1 );
        }

        /// <summary>
        /// Play the previous track from the current <see cref="AudioPlaylist"/>.
        /// </summary>
        public void Previous()
        {
            if ( StateIs( AudioPlayerState.Idle ) && !looping )
                return;

            PlayNext( -1 );
        }

        /// <summary>
        /// Seek the current track to the specified position.
        /// </summary>
        /// <param name="position">Position to seek to.</param>
        public void Seek( uint position )
        {
            if ( StateIs( AudioPlayerState.Idle ) )
                return;

            ErrorIfFailed( fmodChannel.setPosition( position, TIMEUNIT.MS ) );
        }

        /// <summary>
        /// Read the spectrum data of the current <see cref="AudioStream"/>.
        /// </summary>
        /// <param name="spectrum">The spectrum data array.</param>
        public void ReadSpectrum( float[] spectrum )
        {
            if ( StateIs( AudioPlayerState.Idle ) )
                return;

            IntPtr data;
            uint dataLength;
            ErrorIfFailed( fmodDSP.getParameterData( (int)DSP_FFT.SPECTRUMDATA, out data, out dataLength ) );

            var fft = (DSP_PARAMETER_FFT)Marshal.PtrToStructure( data, typeof( DSP_PARAMETER_FFT ) );

            var divisor = volume > 0 ? volume : 1;

            for ( var i = 0; i < fft.length; i++ )
                spectrum[ i ] = fft.spectrum[ 0 ][ i ] / divisor;
        }

        /// <summary>
        /// Write file tags asynchronously based <see cref="AudioInfo"/>.
        /// </summary>
        /// <param name="info">The AudioInfo to write.</param>
        /// <param name="albumArt">The optional album art.</param>
        public async Task<AudioStatus> WriteStreamInfoAsync( AudioInfo info, System.Drawing.Bitmap albumArt = null )
        {
            //
            // All of this is wrapped in a try because we don't currently much care if this fails.
            //

            try
            {
                // Check for readonly flag.
                if ( File.GetAttributes( info.Path ).HasFlag( FileAttributes.ReadOnly ) )
                {
                    return AudioStatus.CanNotWrite;
                }

                // Load the tag currently on file.
                var tagFile = await Task.Run( () => TagLib.File.Create( info.Path ) );

                // If the tag on file is up to date, we can stop here.
                if ( albumArt == null && info.Equals( tagFile.Tag ) )
                    return AudioStatus.Ok;

                // Fill in the tag data.
                tagFile.Tag.Title = info.Title;
                tagFile.Tag.Performers = new[] { info.Artist };
                tagFile.Tag.Album = info.Album;
                tagFile.Tag.Year = uint.Parse( info.Year );
                tagFile.Tag.Track = uint.Parse( info.Track );
                tagFile.Tag.Genres = new[] { info.Genre };

                if ( albumArt != null )
                {
                    var albumArtFilename = Path.Combine( Path.GetDirectoryName( info.Path ), info.Album + AudioCache.AlbumArtFileExtension );

                    try
                    {
                        await Task.Run( () =>
                        {
                            using ( var fileStream = new FileStream( albumArtFilename, FileMode.Create ) )
                            using ( var memoryStream = new MemoryStream() )
                            {
                                albumArt.Save( fileStream, System.Drawing.Imaging.ImageFormat.Jpeg );
                                albumArt.Save( memoryStream, System.Drawing.Imaging.ImageFormat.Jpeg );

                                memoryStream.Seek( 0, SeekOrigin.Begin );
                                tagFile.Tag.Pictures = new TagLib.IPicture[] { new TagLib.Picture( TagLib.ByteVector.FromStream( memoryStream ) ) };
                            }
                        } );
                    }
                    catch ( Exception e )
                    {
                        Global.Log.CallerError( e.Message, e.ToString() );
                    }
                }

                // Handle the scenario where this file is currently playing.
                if ( stream.Info.ID == info.ID )
                {
                    var continuePaused = !stream.Playing;
                    var continuePosition = stream.Position;

                    StopSilently();

                    await Task.Run( () => { tagFile.Save(); } );

                    stream.Info = info;

                    ContinueSilently( continuePaused, continuePosition );
                }
                else
                {
                    tagFile.Save();
                }
            }
            catch ( Exception e )
            {
                Global.Log.CallerError( e.Message, e.ToString() );
                return AudioStatus.Unknown;
            }

            return AudioStatus.Ok;
        }

        /// <summary>
        /// Write file tags based on <see cref="AudioInfo"/>.
        /// </summary>
        /// <param name="info">The AudioInfo to write.</param>
        public AudioStatus WriteStreamInfo( AudioInfo info )
        {
            //
            // All of this is wrapped in a try because we don't currently much care if this fails.
            //

            try
            {
                // Check for readonly flag.
                if ( File.GetAttributes( info.Path )
                    .HasFlag( FileAttributes.ReadOnly ) )
                {
                    return AudioStatus.CanNotWrite;
                }

                // Load the tag currently on file.
                using ( var tagFile = TagLib.File.Create( info.Path ) )
                {
                    // If the tag on file is up to date, we can stop here.
                    if ( info.Equals( tagFile.Tag ) )
                        return AudioStatus.Ok;

                    // Fill in the tag data.
                    tagFile.Tag.Title = info.Title;
                    tagFile.Tag.Performers = new[] { info.Artist };
                    tagFile.Tag.Album = info.Album;
                    tagFile.Tag.Year = uint.Parse( info.Year );
                    tagFile.Tag.Track = uint.Parse( info.Track );
                    tagFile.Tag.Genres = new[] { info.Genre };

                    // Handle the scenario where this file is currently playing.
                    if ( stream.Info.ID == info.ID )
                    {
                        var continuePaused = !stream.Playing;
                        var continuePosition = stream.Position;

                        StopSilently();

                        tagFile.Save();
                        stream.Info = AudioDatabase.ReadStreamInfo( info.Path );

                        ContinueSilently( continuePaused, continuePosition );
                    }
                    else
                    {
                        tagFile.Save();
                    }
                }
            }
            catch { return AudioStatus.Unknown; }

            return AudioStatus.Ok;
        }

        void StopSilently()
        {
            ErrorIfFailed( fmodChannel.setPaused( true ) );
            ErrorIfFailed( fmodChannel.stop() );
            ErrorIfFailed( fmodSound.release() );

            streamTimer.Stop();
            streamCounter.Stop();
        }

        void ContinueSilently( bool startPaused, uint startPosition )
        {
            var streamStatus = OpenStream( stream.Path );

            if ( streamStatus != AudioStatus.Ok )
            {
                OnStreamError.Raise( stream.ID, streamStatus, "failed to open the stream" );
                return;
            }

            ErrorIfFailed( fmodSystem.playSound( fmodSound, fmodChannelGroup, true, out fmodChannel ) );
            ErrorIfFailed( fmodChannel.setPosition( startPosition, TIMEUNIT.MS ) );

            SetState( AudioPlayerState.ActiveButPaused );

            if ( !startPaused )
            {
                // Start playback.
                Play();
            }

            Update();

            streamTimer.Start();
        }

        // ________________ Playlist events handlers ___________________________________________________________

        void OnPlaylistActivated( AudioID item )
        {
            if ( !item.Equals( AudioID.Empty ) )
                Play( item.Data );
            else
                Play( string.Empty );
        }

        void OnPlaylistModified()
        {
            UpdateShuffle();
        }

        // ________________ Privates ___________________________________________________________

        void PlayNext( int direction )
        {
            if ( repeat && stream != AudioStream.Empty && direction != 0 )
            {
                Play( stream.Path );
            }
            else if ( false )
            {
                //PlayingFromQueue = true;
                /*AudioID id = stream_queue.front();

                if ( !repeat )
                {
                    stream_queue.pop();
                }

                Play( db.Get( id ) );*/
            }
            else
            {

                // Advance the appropriate index.
                var index = shuffle ? shuffleIndex + direction : playlist.GetActiveIndex() + direction;
                var indexCount = playlist.Count;

                if ( index < 0 ) // beyond the beginning.
                {
                    index = looping || shuffle ? indexCount - 1 : -1;
                }
                else if ( index >= indexCount ) // beyond the end.
                {
                    // Shuffle always loops the playlist.
                    index = looping || shuffle ? 0 : -1;
                }

                if ( shuffle ) // Get the new index from the shuffled indices.
                {
                    shuffleIndex = index;

                    if ( index >= 0 && index < shuffleIndices.Length )
                        index = shuffleIndices[ shuffleIndex ];

                    playlist.SetActiveIndex( index, true );
                }
                else
                {
                    playlist.SetActiveIndex( index, looping );
                }
            }
        }

        void Heartbeat()
        {
            Update();
            OnHeartbeat.Raise( stream );
        }

        void Update()
        {
            if ( StateIs( AudioPlayerState.Idle ) )
                return;

            if ( fmodSystem == null || fmodSound == null || fmodChannel == null )
                return;

            OPENSTATE streamState = OPENSTATE.ERROR;
            uint streamBufferPercentage;
            bool streamStarving;
            bool streamDiskBusy;

            // Get the latest stream info.
            if ( fmodSystem != null )
                ErrorIfFailed( fmodSystem.update() );
            if ( fmodSound != null )
                ErrorIfFailed( fmodSound.getOpenState( out streamState, out streamBufferPercentage, out streamStarving, out streamDiskBusy ) );

            if ( streamState == OPENSTATE.READY )
            {
                Next();
            }
            else
            {
                bool streamPaused;
                uint streamPosition;

                ErrorIfFailed( fmodChannel.getPaused( out streamPaused ) );
                ErrorIfFailed( fmodChannel.getPosition( out streamPosition, TIMEUNIT.MS ) );

                stream.Position = streamPosition;
                stream.Playing = !streamPaused;
                stream.Played = (uint)(streamCounter.Elapsed / 10000L);
            }
        }


        void ErrorIfFailed( RESULT result, [CallerMemberName] string callerName = "" )
        {
            if ( result != RESULT.OK && result != RESULT.ERR_INVALID_HANDLE && result != RESULT.ERR_INVALID_PARAM )
            {
                Global.Log.Error( $"Audio System | {callerName}", result.ToString() );

                OnStreamError.Raise( stream.ID, AudioStatus.Unknown, result.ToString() );
            }
        }

        bool StateIs( AudioPlayerState s )
        {
            return state == s;
        }

        bool StateIsNot( AudioPlayerState s )
        {
            return state != s;
        }

        void SetState( AudioPlayerState s )
        {
            state = s;
        }

        void UpdateShuffle()
        {
            // Reset the shuffle index.
            shuffleIndex = 0;

            // Make sure the shuffle list is of correct size.
            Array.Resize( ref shuffleIndices, playlist.Count );

            // Fill the list with indices.
            for ( var i = 0; i < shuffleIndices.Length; i++ )
                shuffleIndices[ i ] = i;

            //
            // Fisher–Yates shuffle.
            //

            var random = new Random();
            var n = shuffleIndices.Length;

            while ( n > 1 )
            {
                n--;
                var k = random.Next( n + 1 );
                var temp = shuffleIndices[ k ];
                shuffleIndices[ k ] = shuffleIndices[ n ];
                shuffleIndices[ n ] = temp;
            }
        }
    }
}
