﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KodoFM.Audio
{
    public class AudioDatabase : IEnumerable<AudioItem>
    {
        /// <summary>
        /// Read file information asynchronously. Call Task.Run so you don't have to.
        /// </summary>
        public static async Task<AudioInfo> ReadStreamInfoAsync( string path )
        {
            return await Task.Run( () => ReadStreamInfo( path ) );
        }

        /// <summary>
        /// Read file information.
        /// </summary>
        /// <param name="path">The file to read from.</param>
        public static AudioInfo ReadStreamInfo( string path )
        {
            var result = new AudioInfo( path );

            //
            // All of this is wrapped in a try because we don't currently much care if this fails.
            //

            try
            {
                using ( var tagFile = TagLib.File.Create( path ) )
                {
                    result.Title = tagFile.Tag.Title;
                    result.Artist = tagFile.Tag.FirstPerformer;

                    //
                    // Fixes the case where taglib-sharp reads for example, "AC/DC" as two artists...
                    //
                    if ( tagFile.Tag.Performers.Length > 1 )
                    {
                        foreach ( var performer in tagFile.Tag.Performers.Skip( 1 ) )
                            result.Artist += "/" + performer;
                    }

                    result.Album = tagFile.Tag.Album;
                    result.Genre = tagFile.Tag.FirstGenre;
                    result.Comment = tagFile.Tag.Comment;

                    result.Year = tagFile.Tag.Year.ToString();
                    result.Track = tagFile.Tag.Track.ToString();
                    result.Length = (uint)tagFile.Properties.Duration.TotalMilliseconds;
                }
            }
            // ReSharper disable once EmptyGeneralCatchClause
            catch { }

            return result;
        }

        /// <summary>
        /// The name of the master playlist.
        /// </summary>
        public const string MasterPlaylistName = "All";
        public const string ArtistPrefix = "artist:";
        public const string AlbumPrefix = "album:";

        bool refreshing;

        readonly string databaseFilename;
        readonly AudioPlaylist masterPlaylist = new AudioPlaylist( MasterPlaylistName );
        readonly Dictionary<AudioID, AudioItem> items = new Dictionary<AudioID, AudioItem>();
        readonly Dictionary<AudioID, AudioPlaylist> playlists = new Dictionary<AudioID, AudioPlaylist>();
        readonly SortedDictionary<string, AudioArtistInfo> artists = new SortedDictionary<string, AudioArtistInfo>();
        readonly SortedDictionary<string, AudioAlbumInfo> albums = new SortedDictionary<string, AudioAlbumInfo>();

        /// <summary>
        /// Occurs when an <see cref="AudioItem"/> in the database is modified.
        /// </summary>
        public event AudioCollectionItemDelegate OnItemModified;

        /// <summary>
        /// The number of <see cref="AudioItem"/>s in the database.
        /// </summary>
        public int NumberOfItems => items.Count;

        /// <summary>
        /// The number of <see cref="AudioPlaylist"/>s in the database.
        /// </summary>
        public int NumberOfPlaylists => playlists.Count;

        public bool Refreshing => refreshing;

        public List<AudioItem> GetTracks()
        {
            return null;
        }

        public bool HasArtist( string artist )
        {
            return artists.ContainsKey( artist );
        }

        public bool HasAlbum( string album )
        {
            return albums.ContainsKey( album );
        }

        public List<AudioListingArtist> GetArtists()
        {
            return artists.Select( artist => new AudioListingArtist( artist.Key, artist.Value.Albums.Count, artist.Value.Tracks.Count ) ).ToList();
        }

        public List<AudioListingAlbum> GetAlbums()
        {
            return albums.Select( album => new AudioListingAlbum( album.Key, album.Value.Artist, album.Value.NumberOfTracks ) ).ToList();
        }

        public List<AudioItem> GetTracksFromAlbum( string album )
        {
            return albums[ album ].GetTracks();
        }

        public List<AudioItem> GetTracksFromArtist( string artist )
        {
            return artists.ContainsKey( artist ) ? artists[ artist ].Tracks.Values.ToList() : new List<AudioItem>();
        }

        public List<AudioListingAlbum> GetAlbumsFromArtist( string artist )
        {
            return artists[ artist ].Albums.Select( album => new AudioListingAlbum( album.Key, album.Value.Artist, album.Value.NumberOfTracks ) ).ToList();
        }

        public AudioAlbumInfo GetAlbumListing( string albumName )
        {
            return albums[ albumName ];
        }

        public AudioArtistInfo GetArtistListing( string artistName )
        {
            return artists[ artistName ];
        }

        void AddListing( AudioItem item )
        {
            // Add to the albums.
            if ( !string.IsNullOrEmpty( item.Album ) )
            {
                if ( !albums.ContainsKey( item.Album ) )
                {
                    albums.Add( item.Album, new AudioAlbumInfo( item.Album, item.Artist ) );
                }

                albums[ item.Album ].AddTrack( item );
            }

            // Add to the artists.
            if ( !string.IsNullOrEmpty( item.Artist ) )
            {
                if ( !artists.ContainsKey( item.Artist ) )
                {
                    artists.Add( item.Artist, new AudioArtistInfo( item.Artist ) );
                }

                artists[ item.Artist ].AddTrack( item );
            }
        }

        void RemoveListing( AudioItem item )
        {
            // Remove from artists.
            if ( !string.IsNullOrEmpty( item.Artist ) )
            {
                if ( artists.ContainsKey( item.Artist ) )
                {
                    var artist = artists[ item.Artist ];
                    artist.RemoveTrack( item );
                    //artists[ item.Artist ].RemoveTrack( item );
                }
            }

            // Remove from albums.
            if ( !string.IsNullOrEmpty( item.Album ) )
            {
                if ( albums.ContainsKey( item.Album ) )
                {
                    albums[ item.Album ].RemoveTrack( item );
                }
            }

            //
            // Make sure there are no empty artists or albums.
            //

            var albumsToRemove = (from album in albums
                                  where album.Value.NumberOfTracks == 0
                                  select album.Key).ToList();
            var artistsToRemove = (from artist in artists
                                   where artist.Value.NumberOfTracks == 0
                                   select artist.Key).ToList();

            foreach ( var albumToRemove in albumsToRemove )
                albums.Remove( albumToRemove );

            foreach ( var artistToRemove in artistsToRemove )
                artists.Remove( artistToRemove );
        }

        /// <summary>
        /// Create a new <see cref="AudioDatabase"/> from the specified file.
        /// </summary>
        /// <param name="filename">The file to load the database from.</param>
        public AudioDatabase( string filename )
        {
            databaseFilename = filename;
            playlists[ masterPlaylist.ID ] = masterPlaylist;
            masterPlaylist.OnItemRemoved += OnMasterPlaylistItemRemoved;

            using ( var stream = new FileStream( filename, FileMode.OpenOrCreate, FileAccess.ReadWrite ) )
            using ( var reader = new StreamReader( stream ) )
            {
                var playlistsLine = string.Empty;

                while ( !reader.EndOfStream )
                {
                    var itemLine = reader.ReadLine();

                    if ( string.IsNullOrEmpty( itemLine ) )
                        continue;

                    if ( itemLine.StartsWith( "{", StringComparison.OrdinalIgnoreCase ) )
                    {
                        ReadItem( itemLine );
                    }
                    else
                    {
                        playlistsLine = itemLine;
                    }
                }

                ReadPlaylists( playlistsLine );
            }
        }

        void OnMasterPlaylistItemRemoved( AudioID item, int at )
        {
            Remove( item, true );
        }

        /// <summary>
        /// Asynchronously refresh the <see cref="AudioDatabase"/> by loading all missing tag information.
        /// </summary>
        public async Task RefreshAsync()
        {
            if ( refreshing )
                return;

            refreshing = true;

            var ids = (from i in this where !i.IsLoaded select i.ID).ToList();

            // Load the items, awaiting on the read operation.
            foreach ( var item in ids )
            {
                var audioInfo = await ReadStreamInfoAsync( item.Data );
                var audioItem = audioInfo.ToAudioItem();
                Set( audioItem );
            }

            // Update the playlists.
            foreach ( var pls in playlists.Values )
            {
                pls.Sort();
            }

            refreshing = false;
        }

        /// <summary>
        /// Create a new <see cref="AudioPlaylist"/> with the specified name.
        /// </summary>
        /// <param name="pls">Name of the playlist.</param>
        /// <returns>The playlist that was created or an existing playlist with the same name.</returns>
        public AudioPlaylist CreatePlaylist( string pls )
        {
            return CreatePlaylist( new AudioID( pls ) );
        }
        /// <summary>
        /// Create a new <see cref="AudioPlaylist"/> with the specified <see cref="AudioID"/>.
        /// </summary>
        /// <param name="pls"><see cref="AudioID"/> of the playlist.</param>
        /// <returns>The playlist that was created or an existing playlist with the same <see cref="AudioID"/>.</returns>
        public AudioPlaylist CreatePlaylist( AudioID pls )
        {
            if ( !ContainsPlaylist( pls ) )
            {
                playlists.Add( pls, new AudioPlaylist( pls ) );
            }

            return playlists[ pls ];
        }

        public void RemovePlaylist( string pls )
        {
            RemovePlaylist( new AudioID( pls ) );
        }
        public void RemovePlaylist( AudioID pls )
        {
            playlists.Remove( pls );

            try
            {
                File.Delete( $"{pls}.kpls" );
            }
            // ReSharper disable once EmptyGeneralCatchClause
            catch { }
        }

        public List<AudioPlaylist> GetPlaylists()
        {
            return playlists.Values.Where( pls => !pls.Name.Equals( MasterPlaylistName ) &&
                                                  !pls.Name.StartsWith( ArtistPrefix ) &&
                                                  !pls.Name.StartsWith( AlbumPrefix ) ).ToList();
        }

        public AudioPlaylist GetPlaylistMaster()
        {
            return masterPlaylist;
        }

        public AudioPlaylist GetPlaylist( string pls )
        {
            return GetPlaylist( new AudioID( pls ) );
        }
        public AudioPlaylist GetPlaylist( AudioID pls )
        {
            return ContainsPlaylist( pls ) ? playlists[ pls ] : AudioPlaylist.Empty;
        }

        public bool ContainsPlaylist( string pls )
        {
            return ContainsPlaylist( new AudioID( pls ) );
        }
        public bool ContainsPlaylist( AudioID pls )
        {
            return playlists.ContainsKey( pls );
        }

        void ReadPlaylists( string value )
        {
            if ( value == null || value.Length <= 2 )
                return;

            var values = value.Substring( 1, value.Length - 2 ).Split( ',' );
            if ( values.Length <= 0 )
                return;

            foreach ( var plsName in values )
            {
                var plsID = new AudioID( plsName );
                var plsPath = $"{plsID.Data}.kpls";

                // Reject playlist that would overwrite the master.
                if ( plsID.Equals( MasterPlaylistName ) )
                    continue;

                if ( !File.Exists( plsPath ) )
                    continue;

                // Create the new playlist and add it to the playlist collection.
                var pls = playlists[ plsID ] = new AudioPlaylist( plsID );

                using ( var reader = new StreamReader( plsPath ) )
                {
                    while ( !reader.EndOfStream )
                    {
                        var line = reader.ReadLine();

                        if ( string.IsNullOrEmpty( line ) )
                            continue;

                        var item = new AudioID( line );

                        if ( Contains( item ) )
                        {
                            pls.Add( Get( item ) );
                        }
                    }
                }
            }
        }

        void ReadItem( string value )
        {
            if ( value == null || value.Length < 7 )
                return;

            var values = value.Substring( 1, value.Length - 2 ).Split( ';' );
            if ( values.Length != 6 )
                return;

            var path = values[ 0 ];
            var title = values[ 1 ];
            var artist = values[ 2 ];
            var album = values[ 3 ];
            var length = values[ 4 ];

            int flags;
            if ( int.TryParse( values[ 5 ], out flags ) == false )
                return;

            var item = new AudioItem( path,
                                      title,
                                      artist,
                                      album,
                                      length,
                                      "",
                                      (AudioItemFlag)flags );

            Add( item );
        }

        public void Write() { Write( databaseFilename ); }
        public void Write( string path )
        {
            var builder = new StringBuilder();
            builder.Append( '(' );

            // Use GetPlaylists to only get the relevant playlists.
            foreach ( var pls in GetPlaylists() )
            {
                using ( var plsWriter = new StreamWriter( $"{pls.Name}.kpls", false ) )
                {
                    foreach ( var item in pls )
                    {
                        plsWriter.WriteLine( item.Path );
                    }
                }

                builder.Append( pls.Name );
                builder.Append( ',' );
            }

            builder.Length -= 1;
            builder.Append( ')' );

            using ( var writer = new StreamWriter( path, false ) )
            {
                foreach ( var value in items.Values )
                {
                    writer.Write( '{' );
                    writer.Write( value.ID.ToString() );
                    writer.Write( ';' );
                    writer.Write( value.Title );
                    writer.Write( ';' );
                    writer.Write( value.Artist );
                    writer.Write( ';' );
                    writer.Write( value.Album );
                    writer.Write( ';' );
                    writer.Write( value.Length );
                    writer.Write( ';' );
                    writer.Write( ((int)value.Flags).ToString() );
                    writer.Write( '}' );
                    writer.WriteLine();
                }

                writer.WriteLine( builder.ToString() );
            }
        }

        /// <summary>
        /// Indicates whether or not the database contains an <see cref="AudioItem"/> with the specified <see cref="AudioID"/>.
        /// </summary>
        /// <param name="id">The <see cref="AudioID"/> to look for.</param>
        public bool Contains( AudioID id )
        {
            return items.ContainsKey( id );
        }

        /// <summary>
        /// Get an <see cref="AudioItem"/> that has the specified <see cref="AudioID"/>. Returns <see cref="AudioItem.Empty"/> if not found.
        /// </summary>
        /// <param name="id">The <see cref="AudioID"/> to look for.</param>
        public AudioItem Get( AudioID id )
        {
            AudioItem item;
            return items.TryGetValue( id, out item ) ? item : AudioItem.Empty;
        }

        /// <summary>
        /// Set item data.
        /// </summary>
        /// <param name="item">The item to set.</param>
        public void Set( AudioItem item )
        {
            if ( items.ContainsKey( item ) == false )
                return;

            // Grab the original.
            var original = items[ item ];

            // Update the item collection.
            items[ item ] = item;

            // Update the listing.
            RemoveListing( original );
            AddListing( item );

            // Update the playlists.
            foreach ( var pls in playlists.Values )
            {
                pls.Set( item );
            }

            OnItemModified?.Invoke( item );

        }

        public void Add( AudioItem item )
        {
            if ( items.ContainsKey( item ) )
                return;

            items.Add( item, item );

            // Update the listing.
            AddListing( item );

            // Make sure the default playlist contains all items.
            masterPlaylist.Add( item );

        }

        public void AddRange( IEnumerable<AudioItem> itemRange )
        {
            foreach ( var item in itemRange )
            {
                Add( item );
            }

            // Update the playlists.
            foreach ( var pls in playlists.Values )
            {
                pls.Sort();
            }
        }

        public void Remove( AudioID item, bool ignoreMaster = false )
        {
            items.Remove( item );

            // Remove from the playlists.
            foreach ( var pls in playlists.Values )
            {
                pls.Remove( item );
            }
        }

        public void RemoveAll( IEnumerable<AudioItem> range )
        {
            foreach ( var item in range )
            {
                items.Remove( item );

                foreach ( var pls in playlists.Values )
                    pls.Remove( item );
            }
        }

        public void RemoveAll( Predicate<AudioID> match ) { RemoveAll( items.Keys.Where( item => match( item ) ) ); }
        public void RemoveAll( IEnumerable<AudioID> range )
        {
            foreach ( var item in range )
            {
                items.Remove( item );

                foreach ( var pls in playlists.Values )
                    pls.Remove( item );
            }
        }

        public void RemoveAll()
        {
            items.Clear();

            // Clear the connected playlists.
            foreach ( var pls in playlists.Values )
                pls.RemoveAll();
        }

        IEnumerator IEnumerable.GetEnumerator() { return GetEnumerator(); }
        public IEnumerator<AudioItem> GetEnumerator()
        {
            return items.Select( item => item.Value ).GetEnumerator();
        }
    }
}
