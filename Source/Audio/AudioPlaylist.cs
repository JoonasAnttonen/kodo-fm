﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace KodoFM.Audio
{
    public delegate void AudioCollectionChangedDelegate();
    public delegate void AudioCollectionItemDelegate( AudioID item );

    public class AudioPlaylist : IEnumerable<AudioItem>
    {
        public delegate void AudioPlaylistIndicesSwappedDelegate( int left, int right );
        public delegate void AudioPlaylistItemDelegate( AudioID item, int at );

        public static readonly AudioPlaylist Empty = new AudioPlaylist( "Empty" );
        public const int InvalidIndex = -1;

        int activeIndex = InvalidIndex;

        List<AudioItem> itemList = new List<AudioItem>();
        Dictionary<AudioID, int> itemLookup = new Dictionary<AudioID, int>();

        AudioSorter sorter = new AudioSorter( AudioSorting.Path );

        // ________________ Events ___________________________________________________________

        public event AudioPlaylistIndicesSwappedDelegate OnItemsSwapped;
        public event AudioPlaylistItemDelegate OnItemRemoved;

        public event AudioCollectionItemDelegate OnItemAdded;
        public event AudioCollectionItemDelegate OnItemUpdated;

        public event AudioCollectionItemDelegate OnActivated;
        public event AudioCollectionChangedDelegate OnModified;


        // ________________ Properties___________________________________________________________

        /// <summary>
        /// Get the <see cref="AudioID"/> of the <see cref="AudioPlaylist"/>.
        /// </summary>
        public AudioID ID { get; }

        /// <summary>
        /// Get the name of the <see cref="AudioPlaylist"/>.
        /// </summary>
        public string Name
        {
            get { return ID.Data; }
        }

        /// <summary>
        /// Get the number of items in the <see cref="AudioPlaylist"/>.
        /// </summary>
        public int Count
        {
            get { return itemList.Count; }
        }

        /// <summary>
        /// Returns an iterator that iterates through the <see cref="AudioPlaylist"/>.
        /// </summary>
        public IEnumerator<AudioItem> GetEnumerator()
        {
            return itemList.GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// Create a new <see cref="AudioPlaylist"/>.
        /// </summary>
        /// <param name="name">Name of the playlist.</param>
        public AudioPlaylist( string name ) : this( new AudioID( name ) ) { }
        /// <summary>
        /// Create a new <see cref="AudioPlaylist"/>.
        /// </summary>
        /// <param name="name">Name of the playlist.</param>
        public AudioPlaylist( AudioID name )
        {
            ID = name;
        }

        /// <summary>
        /// Sort the playlist using the specified <see cref="AudioSorter"/>.
        /// </summary>
        /// <param name="comparer">The <see cref="AudioSorter"/>.</param>
        public void Sort( AudioSorter comparer = null )
        {
            if ( comparer != null )
            {
                sorter = comparer;
            }

            itemList.Sort( sorter );
            RebuildInternal();
            OnModified?.Invoke();
        }

        /// <summary>
        /// Get the index of the active <see cref="AudioItem"/>.
        /// </summary>
        public int GetActiveIndex()
        {
            return activeIndex;
        }

        /// <summary>
        /// Get the active <see cref="AudioItem"/>.
        /// </summary>
        public AudioItem GetActiveItem()
        {
            return activeIndex != InvalidIndex ? itemList[ activeIndex ] : AudioItem.Empty;
        }

        /// <summary>
        /// Set the active item on the playlist.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="force">Whether or not to force an active item incase the item was not in the list.</param>
        public void SetActiveItem( AudioItem item, bool force = false )
        {
            SetActiveIndex( IndexOf( item ), force );
        }

        /// <summary>
        /// Set the active item on the playlist.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="force">Whether or not to force an active index incase the index was not in the list.</param>
        public void SetActiveIndex( int index, bool force = false )
        {
            AudioItem item;

            if ( index >= 0 && index < itemList.Count )
            {
                item = itemList[ index ];
                activeIndex = index;
            }
            else if ( force && itemList.Count > 0 )
            {
                item = itemList[ 0 ];
                activeIndex = 0;
            }
            else
            {
                item = AudioItem.Empty;
                activeIndex = InvalidIndex;
            }

            OnActivated?.Invoke( item );
        }

        /// <summary>
        /// Swap two items.
        /// </summary>
        /// <param name="left">The index of the left item.</param>
        /// <param name="right">The index of the right item.</param>
        public void SwapByIndex( int left, int right )
        {
            // Make sure the active index stays correct.
            if ( left == activeIndex )
                activeIndex = right;
            else if ( right == activeIndex )
                activeIndex = left;

            // Swap the items.
            var item = itemList[ left ];
            itemList[ left ] = itemList[ right ];
            itemList[ right ] = item;

            // Update the lookup table.
            itemLookup[ itemList[ left ].ID ] = left;
            itemLookup[ itemList[ right ].ID ] = right;

            OnItemsSwapped?.Invoke( left, right );
            OnModified?.Invoke();
        }

        /// <summary>
        /// Returns the item with the specified index.
        /// </summary>
        /// <param name="index">The index.</param>
        public AudioItem Get( int index )
        {
            return itemList[ index ];
        }

        /// <summary>
        /// Returns the item with the specified <see cref="AudioID"/>. Returns <see cref="AudioItem.Empty"/> if not found.
        /// </summary>
        /// <param name="id">The <see cref="AudioID"/>.</param>
        public AudioItem Get( AudioID id )
        {
            return !Contains( id ) ? AudioItem.Empty : itemList[ itemLookup[ id ] ];
        }

        /// <summary>
        /// Returns the index of the item with the specified id. Returns InvalidIndex if not found.
        /// </summary>
        /// <param name="id">ID to get.</param>
        public int IndexOf( AudioID id )
        {
            int result;
            return itemLookup.TryGetValue( id, out result ) ? result : InvalidIndex;
        }

        /// <summary>
        /// Indicates whether or not the playlist contains an item with the specified <see cref="AudioID"/>.
        /// </summary>
        /// <param name="id">ID to check.</param>
        public bool Contains( AudioID id )
        {
            return itemLookup.ContainsKey( id );
        }

        /// <summary>
        /// Add an item. 
        /// Does nothing if item already exists.
        /// </summary>
        /// <param name="item">Item to add.</param>
        public void Add( AudioItem item )
        {
            if ( itemLookup.ContainsKey( item ) )
                return;

            AddInternal( item );

            OnItemAdded?.Invoke( item );
            OnModified?.Invoke();
        }

        /// <summary>
        /// Add a range of items.
        /// </summary>
        /// <param name="range">Range to add from.</param>
        public void AddRange( IEnumerable<AudioItem> range )
        {
            foreach ( var item in range )
            {
                if ( itemLookup.ContainsKey( item ) )
                    return;

                AddInternal( item );

                OnItemAdded?.Invoke( item );
            }

            OnModified?.Invoke();
        }

        /// <summary>
        /// Set an item in the list.
        /// </summary>
        /// <param name="item">The item.</param>
        public void Set( AudioItem item )
        {
            if ( itemLookup.ContainsKey( item ) )
            {
                itemList[ itemLookup[ item ] ] = item;
                OnItemUpdated?.Invoke( item );
                OnModified?.Invoke();
            }
        }

        bool IsIndexInRange( int index )
        {
            return index >= 0 && index < itemList.Count;
        }

        /// <summary>
        /// Remove an item at the specified index.
        /// </summary>
        /// <param name="index">The index.</param>
        public void RemoveAt( int index )
        {
            if ( !IsIndexInRange( index ) )
                throw new ArgumentOutOfRangeException( nameof( index ) );

            Remove( itemList[ index ] );
        }

        /// <summary>
        /// Remove the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        public void Remove( AudioID item )
        {
            int itemIndex;

            if ( itemLookup.TryGetValue( item, out itemIndex ) )
            {
                itemList.RemoveAt( itemIndex );
                itemLookup.Remove( item );

                RebuildInternal();
                OnItemRemoved?.Invoke( item, itemIndex );
            }

            OnModified?.Invoke();
        }

        /// <summary>
        /// Remove a range of items.
        /// </summary>
        /// <param name="range">The range.</param>
        public void RemoveRange( List<AudioItem> range )
        {
            if ( range == null )
                throw new ArgumentNullException( nameof( range ) );

            foreach ( var item in range )
            {
                int itemIndex;

                if ( itemLookup.TryGetValue( item, out itemIndex ) )
                {
                    RemoveInternal( item, itemIndex );

                    OnItemRemoved?.Invoke( item, itemIndex );
                }
            }

            CleanInternal();
            RebuildInternal();

            OnModified?.Invoke();
        }

        /// <summary>
        /// Remove all items.
        /// </summary>
        public void RemoveAll()
        {
            itemList.Clear();
            itemLookup.Clear();

            activeIndex = InvalidIndex;

            OnModified?.Invoke();
        }

        void AddInternal( AudioItem item )
        {
            itemList.Add( item );
            itemLookup.Add( item, itemList.Count - 1 );
        }

        void RemoveInternal( AudioID item, int index )
        {
            if ( index == activeIndex )
                activeIndex = InvalidIndex;

            itemList[ index ] = AudioItem.Empty;
            itemLookup.Remove( item );
        }

        void CleanInternal()
        {
            itemList.RemoveAll( i => i.Equals( AudioItem.Empty ) );
        }

        void RebuildInternal()
        {
            for ( var i = 0; i < itemList.Count; i++ )
                itemLookup[ itemList[ i ] ] = i;
        }
    }
}