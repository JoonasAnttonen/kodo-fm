﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace KodoFM.Audio
{
    public struct AudioListingArtist
    {
        public readonly string Name;
        public readonly int Albums;
        public readonly int Tracks;

        public AudioListingArtist( string name, int albums, int tracks )
        {
            Name = name;
            Albums = albums;
            Tracks = tracks;
        }
    }

    public struct AudioListingAlbum
    {
        public readonly string Name;
        public readonly string Artist;
        public readonly int Tracks;

        public AudioListingAlbum( string name, string artist, int tracks )
        {
            Name = name;
            Artist = artist;
            Tracks = tracks;
        }
    }

    public class AudioAlbumInfo : IEnumerable<AudioItem>, IComparable<AudioAlbumInfo>
    {
        public string Name { get; }
        public string Artist { get; }

        SortedDictionary<AudioID, AudioItem> Tracks { get; } = new SortedDictionary<AudioID, AudioItem>();

        public int NumberOfTracks => Tracks.Count;

        public AudioAlbumInfo( string albumName, string artistName )
        {
            Name = albumName;
            Artist = artistName;
        }

        public List<AudioItem> GetTracks()
        {
            return Tracks.Values.ToList();
        }

        public void AddTrack( AudioItem track )
        {
            if ( !HasTrack( track ) )
            {
                Tracks.Add( track, track );
            }
        }

        public void RemoveTrack( AudioItem track )
        {
            if ( HasTrack( track ) )
            {
                Tracks.Remove( track );
            }
        }

        public bool HasTrack( AudioID track )
        {
            return Tracks.ContainsKey( track );
        }

        IEnumerator IEnumerable.GetEnumerator() { return GetEnumerator(); }
        public IEnumerator<AudioItem> GetEnumerator()
        {
            return Tracks.Values.GetEnumerator();
        }

        public int CompareTo( AudioAlbumInfo other )
        {
            return string.CompareOrdinal( Name, other.Name );
        }
    }

    public class AudioArtistInfo
    {
        public string Name { get; }
        public SortedDictionary<AudioID, AudioItem> Tracks { get; } = new SortedDictionary<AudioID, AudioItem>();
        public SortedDictionary<string, AudioAlbumInfo> Albums { get; } = new SortedDictionary<string, AudioAlbumInfo>();
        public int NumberOfTracks => Tracks.Count;

        public AudioArtistInfo( string name )
        {
            Name = name;
        }

        public bool HasTrack( AudioID track )
        {
            return Tracks.ContainsKey( track );
        }

        public void RemoveTrack( AudioItem track )
        {
            if ( HasTrack( track ) )
            {
                Tracks.Remove( track );

                var albumsToRemove = new List<string>();

                foreach ( var album in Albums )
                {
                    album.Value.RemoveTrack( track );

                    if ( album.Value.NumberOfTracks == 0 )
                        albumsToRemove.Add( album.Key );
                }

                foreach ( var albumToRemove in albumsToRemove )
                {
                    Albums.Remove( albumToRemove );
                }
            }
        }

        public void AddTrack( AudioItem track )
        {
            if ( !HasTrack( track ) )
            {
                Tracks.Add( track, track );
            }

            if ( !string.IsNullOrWhiteSpace( track.Album ) )
            {
                if ( Albums.ContainsKey( track.Album ) )
                {
                    Albums[ track.Album ].AddTrack( track );
                }
                else
                {
                    Albums.Add( track.Album, new AudioAlbumInfo( track.Album, track.Artist ) );
                    Albums[ track.Album ].AddTrack( track );
                }
            }
        }
    }
}
