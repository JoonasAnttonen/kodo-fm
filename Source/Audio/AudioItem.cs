﻿using System;
using System.Collections.Generic;

namespace KodoFM.Audio
{
    /// <summary> 
    /// Represents an unique identifier of <see cref="AudioItem"/>.
    /// </summary>
    public struct AudioID : IEquatable<AudioID>, IComparable<AudioID>
    {
        /// <summary> 
        /// Represents the empty identifier. 
        /// </summary>
        public static readonly AudioID Empty = new AudioID( string.Empty );

        /// <summary> 
        /// The data associated with this identifier. 
        /// </summary>
        public readonly string Data;

        /// <summary> 
        /// The value associated with this identifier. 
        /// </summary>
        public readonly int Value;

        /// <summary> 
        /// Create a new <see cref="AudioID"/>. 
        /// </summary>
        /// <param name="data">Data of the identifier.</param>
        public AudioID( string data )
        {
            if ( data == null )
                data = string.Empty;

            Data = data;
            Value = data.GetHashCode();
        }

        public override string ToString()
        {
            return Data;
        }

        // ________________ Comparison Stuff ___________________________________________________________

        public static bool operator !=( AudioID left, AudioID right ) { return !left.Equals( right ); }
        public static bool operator ==( AudioID left, AudioID right ) { return left.Equals( right ); }

        public int CompareTo( AudioID other )
        {
            return string.CompareOrdinal( Data, other.Data );
        }

        public override int GetHashCode() { return Value; }

        public override bool Equals( object obj ) { return ((AudioID)obj).Equals( this ); }

        public bool Equals( string str )
        {
            return Data.Equals( str );
        }

        public bool Equals( AudioID id )
        {
            return (Value == id.Value) && (Data == id.Data);
        }
    }

    [Flags]
    public enum AudioItemFlag
    {
        None = 0,
        Loaded = 1,
        Exists = 2,
        TypeLocal = 4,
        TypeStream = 8,
        TypeInvalid = 16,
        Loved = 32,
    }

    public enum AudioSorting
    {
        Path,
        Artist,
        Album,
    }

    public enum AudioSortingOrder
    {
        Ascending,
        Descending,
    }

    /// <summary>
    /// Sorting comparer for <see cref="AudioItem"/>s.
    /// </summary>
    public class AudioSorter : IComparer<AudioItem>
    {
        public AudioSorting Sorting
        {
            get;
            set;
        }

        public AudioSortingOrder Order
        {
            get;
            set;
        }

        public AudioSorter( AudioSorting sorting, AudioSortingOrder order = AudioSortingOrder.Descending )
        {
            Sorting = sorting;
            Order = order;
        }

        public int Compare( AudioItem x, AudioItem y )
        {
            const StringComparison comparison = StringComparison.OrdinalIgnoreCase;

            int result;

            switch ( Sorting )
            {
                case AudioSorting.Path:
                    result = string.Compare( x.Path, y.Path, comparison );
                    break;
                case AudioSorting.Artist:
                    result = string.Compare( x.Artist, y.Artist, comparison );

                    if ( result == 0 )
                    {
                        result = string.Compare( x.Album, y.Album, comparison );

                        if ( result == 0 )
                        {
                            result = string.Compare( x.Path, y.Path, comparison );
                        }
                    }
                    break;
                case AudioSorting.Album:
                    result = string.Compare( x.Album, y.Album, comparison );

                    if ( result == 0 )
                    {
                        result = string.Compare( x.Path, y.Path, comparison );
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return Order == AudioSortingOrder.Descending ? result : -result;
        }
    }

    public class AudioItem : IEquatable<AudioItem>, IComparable<AudioItem>
    {
        /// <summary> 
        /// Represents the empty item. 
        /// </summary>
        public static readonly AudioItem Empty = new AudioItem( string.Empty );

        /// <summary> 
        /// Get the <see cref="AudioID"/> associated with this item.
        /// </summary>
        public AudioID ID { get; }

        /// <summary> 
        /// Get the path of this item. 
        /// </summary>
        public string Path
        {
            get { return ID.Data; }
        }

        /// <summary> 
        /// Get or set the flags of this item. 
        /// </summary>
        public AudioItemFlag Flags { get; set; }

        /// <summary>
        /// Indicates whether or not the item exists.
        /// </summary>
        public bool IsReal
        {
            get { return Flags.HasFlag( AudioItemFlag.Exists ); }
        }

        /// <summary>
        /// Indicates whether or not the item has been loaded.
        /// </summary>
        public bool IsLoaded
        {
            get { return Flags.HasFlag( AudioItemFlag.Loaded ); }
        }

        public string Title { get; }
        public string Artist { get; }
        public string Album { get; }
        public string Length { get; }
        public string Number { get; }

        /// <summary>
        /// Implicit conversion to <see cref="AudioID"/>.
        /// </summary>
        /// <param name="item">The item.</param>
        public static implicit operator AudioID( AudioItem item )
        {
            return item.ID;
        }

        /// <summary> 
        /// Create a new <see cref="AudioItem"/>. 
        /// </summary>
        /// <param name="path">Path of the item.</param>
        public AudioItem( string path )
        {
            ID = new AudioID( path );
            Title = path;
            Artist = string.Empty;
            Album = string.Empty;
            Length = string.Empty;
            Number = string.Empty;
            Flags = AudioItemFlag.None;
        }

        /// <summary>
        /// Create a new <see cref="AudioItem"/>.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="title"></param>
        /// <param name="artist"></param>
        /// <param name="album"></param>
        /// <param name="length"></param>
        /// <param name="number"></param>
        /// <param name="flag"></param>
        public AudioItem( string path, string title, string artist, string album, string length, string number, AudioItemFlag flag )
        {
            ID = new AudioID( path );
            Title = title;
            Artist = artist;
            Album = album;
            Length = length;
            Number = number;
            Flags = flag;
        }

        // ________________ Comparison Stuff ___________________________________________________________

        public override int GetHashCode()
        {
            return ID.GetHashCode();
        }

        public override bool Equals( object obj ) { return ((AudioItem)obj).Equals( this ); }
        public bool Equals( AudioItem item )
        {
            return ID.Equals( item.ID );
        }

        public int CompareTo( AudioItem other )
        {
            return ID.CompareTo( other );
        }
    }
}
