﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace KodoFM.Audio
{
    [Flags]
    public enum AudioEventType
    {
        ItemAdded,
        ItemAddedDB,

        ItemUpdated,

        ItemRemoved,
        ItemRemovedDB,

        StreamBegin,
        StreamEnd,
    }

    public class AudioEventData
    {
        public AudioEventType Type { get; }

        public AudioEventData( AudioEventType type )
        {
            Type = type;
        }
    }

    public class AudioEventItem : AudioEventData
    {
        public AudioItem Item { get; }

        public AudioEventItem( AudioEventType type, AudioItem item )
            : base( type )
        {
            Item = item;
        }
    }

    public class AudioEventStream : AudioEventData
    {
        public AudioStream Stream { get; }

        public AudioEventStream( AudioEventType type, AudioStream stream )
            : base( type )
        {
            Stream = stream;
        }
    }

    public delegate void AudioEventDelegate( AudioEventData data );

    public delegate void AudioSystemDelegate();

    public class AudioEventSubscription
    {
        public AudioEventType Type { get; }
        public AudioEventDelegate Handler { get; }

        public AudioEventSubscription( AudioEventType type, AudioEventDelegate handler )
        {
            Type = type;
            Handler = handler;
        }
    }

    public static class AudioEventSystem
    {
        static int isRunning;
        static SynchronizationContext eventContext;
        static ConcurrentQueue<AudioEventData> eventQueue = new ConcurrentQueue<AudioEventData>();
        static ConcurrentBag<AudioEventSubscription> eventHandlers = new ConcurrentBag<AudioEventSubscription>();


        /*public AudioEventSystem( SynchronizationContext synchronizationContext )
        {
            eventContext = synchronizationContext;
        }*/

        public static void SetSynchronizationContext( SynchronizationContext synchronizationContext )
        {
            eventContext = synchronizationContext;
        }

        public static AudioEventSubscription Subscribe( AudioEventType type, AudioEventDelegate handler )
        {
            var subscription = new AudioEventSubscription( type, handler );
            eventHandlers.Add( subscription );
            return subscription;
        }

        public static void Publish( AudioEventData eventData )
        {
            eventQueue.Enqueue( eventData );

            if ( Interlocked.CompareExchange( ref isRunning, 1, 1 ) == 0 )
            {
                eventContext.Post( Run, null );
            }
        }

        static void Run( object state )
        {
            Interlocked.Exchange( ref isRunning, 1 );

            while ( eventQueue.Count > 0 )
            {
                AudioEventData eventData;

                if ( eventQueue.TryDequeue( out eventData ) )
                {
                    foreach ( var eventHandler in eventHandlers )
                    {
                        if ( eventHandler.Type == eventData.Type )
                        {
                            eventHandler.Handler.Invoke( eventData );
                        }
                    }
                }
            }

            Interlocked.Exchange( ref isRunning, 0 );
        }
    }
}
