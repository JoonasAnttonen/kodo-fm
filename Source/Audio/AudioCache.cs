﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;

namespace KodoFM.Audio
{
    public static class AudioCache
    {
        public static string AlbumArtCache;
        public const string AlbumArtFileExtension = ".jpg";

        /// <summary>
        /// The file extensions of the supported album art image types.
        /// </summary>
        public static readonly string[] SupportedImageTypes = { ".jpg", ".jpeg", ".png" };

        public static void CacheAlbumArt( string albumName, string artistName, Bitmap art )
        {
            if ( !Directory.Exists( AlbumArtCache ) )
                Directory.CreateDirectory( AlbumArtCache );

            var albumArtFilename = GetAlbumArtPath( albumName, artistName );

            if ( art != null )
            {
                if ( File.Exists( albumArtFilename ) )
                {
                    File.Delete( albumArtFilename );
                }

                SaveAlbumArt( albumArtFilename, art );
            }
        }

        public static bool CacheAlbumArt( string albumName, string artistName, string filename )
        {
            if ( !Directory.Exists( AlbumArtCache ) )
                Directory.CreateDirectory( AlbumArtCache );

            var albumArtFilename = GetAlbumArtPath( albumName, artistName );

            if ( string.IsNullOrEmpty( albumArtFilename ) )
                return false;

            if ( File.Exists( albumArtFilename ) )
                return true;

            try
            {
                using ( var tagFile = TagLib.File.Create( filename ) )
                {
                    var albumArt = CollectAlbumArt( Path.GetDirectoryName( filename ), SupportedImageTypes );

                    if ( albumArt == null )
                    {
                        if ( tagFile.Tag.Pictures.Length > 0 && tagFile.Tag.Pictures[ 0 ].Data.Data.Length > 0 )
                        {
                            using ( var memoryStream = new MemoryStream( tagFile.Tag.Pictures[ 0 ].Data.Data ) )
                            {
                                SaveAlbumArt( albumArtFilename, Image.FromStream( memoryStream ) );
                            }
                        }
                    }
                    else
                    {
                        SaveAlbumArt( albumArtFilename, albumArt );
                    }
                }
            }
            // ReSharper disable once EmptyGeneralCatchClause
            catch { }

            return File.Exists( albumArtFilename );
        }

        static void SaveAlbumArt( string albumArtFilename, Image albumArt )
        {
            if ( albumArt.Height > 500 && albumArt.Width > 500 )
            {
                var albumArtThumbnail = albumArt.GetThumbnailImage( 500, 500, () => true, IntPtr.Zero );
                albumArtThumbnail.Save( albumArtFilename, ImageFormat.Jpeg );
            }
            else
            {
                albumArt.Save( albumArtFilename, ImageFormat.Jpeg );
            }
        }

        static string GetAlbumArtPath( string albumName, string artistName )
        {
            try
            {
                var illegalChars = Path.GetInvalidFileNameChars();
                var filename = $"{artistName}-{albumName}{AlbumArtFileExtension}";
                filename = illegalChars.Aggregate( filename, ( current, c ) => current.Replace( c, '$' ) );

                return $"{AlbumArtCache}\\{filename}";
            }
            catch
            {
                return string.Empty;
            }
        }

        public static Bitmap GetCachedAlbumArt( string albumName, string artistName )
        {
            var albumArtFilename = GetAlbumArtPath( albumName, artistName );
            return !File.Exists( albumArtFilename ) ? null : new Bitmap( albumArtFilename );
        }

        public static Bitmap GetAlbumArt( string albumName, string artistName, string filename )
        {
            var albumArtFilename = GetAlbumArtPath( albumName, artistName );
            return CacheAlbumArt( albumName, artistName, filename ) ? new Bitmap( albumArtFilename ) : null;
        }

        static Image CollectAlbumArt( string path, string[] allowedExtensions )
        {
            try
            {
                if ( Directory.Exists( path ) )
                {
                    var files = Directory.GetFileSystemEntries( path );

                    foreach ( var file in files.Where( file => allowedExtensions.Any( t => file.EndsWith( t, StringComparison.OrdinalIgnoreCase ) ) ) )
                    {
                        return Image.FromFile( file );
                    }
                }
            }
            // ReSharper disable once EmptyGeneralCatchClause
            catch { }

            return null;
        }
    }
}
