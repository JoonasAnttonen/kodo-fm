﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.IO;

using Kodo.JSON;

namespace Kodo.Lastfm
{
    internal class LastfmRequest
    {
        const string ServiceURL = "http://ws.audioscrobbler.com/2.0/";
        const string ServiceURLHTTPS = "https://ws.audioscrobbler.com/2.0/";
        static DateTime TimeOfLastRequest = DateTime.MinValue;

        /// <summary>
        /// The parameters used in the request.
        /// </summary>
        public SortedDictionary<string, string> Parameters
        {
            get;
            set;
        }

        /// <summary>
        /// Enable or disable HTTPS connection.
        /// </summary>
        public bool Https
        {
            get;
            set;
        }

        /// <summary>
        /// Get or set the timeout in milliseconds.
        /// </summary>
        public int Timeout
        {
            get;
            set;
        }

        /// <summary>
        /// Create a new instance of <see cref="LastfmRequest"/>.
        /// </summary>
        /// <param name="method">The method of the request.</param>
        public LastfmRequest( string method ) :
            this( method, String.Empty )
        {
        }

        /// <summary>
        /// Create a new instance of <see cref="LastfmRequest"/>.
        /// </summary>
        /// <param name="method">The method of the request.</param>
        /// <param name="sessionKey">The session key for the request.</param>
        public LastfmRequest( string method, string sessionKey )
        {
            // Assert required parameters.
            Debug.Assert( !string.IsNullOrEmpty( method ) );

            Timeout = 10000;

            // Create the initial parameters.
            Parameters = new SortedDictionary<string, string>();
            Parameters[ "method" ] = method;
            Parameters[ "api_key" ] = LastfmGlobal.AppKey;

            // Optional parameters.
            if ( !string.IsNullOrEmpty( sessionKey ) )
                Parameters[ "sk" ] = sessionKey;
        }

        /// <summary>
        /// Execute the request.
        /// </summary>
        public JSONDocument Execute()
        {
            WaitForTimeslot();

            // Get the query as a byte array.
            var queryData = BuildQuery();

            // Create the http request.
            var request = WebRequest.Create( Https == false ? ServiceURL : ServiceURLHTTPS ) as HttpWebRequest;
            request.ServicePoint.Expect100Continue = false;
            request.Timeout = Timeout;
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = queryData.Length;
            request.UserAgent = LastfmGlobal.AppName;
            request.Method = "POST";
            request.Headers[ "Accept-Charset" ] = "utf-8";

            // Write our request data.
            using ( var writeStream = request.GetRequestStream() )
            {
                writeStream.Write( queryData, 0, queryData.Length );
                writeStream.Flush();
            }

            // Get the response.
            HttpWebResponse webresponse;

            try
            {
                webresponse = (HttpWebResponse)request.GetResponse();
            }
            catch ( WebException e )
            {
                webresponse = (HttpWebResponse)e.Response;
            }

            using ( var responseStream = webresponse.GetResponseStream() )
            {
                return new JSONDocument( responseStream );
            }
        }

        void WaitForTimeslot()
        {
            var timeDifference = DateTime.Now - TimeOfLastRequest;

            if ( timeDifference < LastfmGlobal.TimeBetweenRequests )
            {
                System.Threading.Thread.Sleep( LastfmGlobal.TimeBetweenRequests - timeDifference );
            }

            TimeOfLastRequest = DateTime.Now;
        }

        public static string MD5Hash( string text )
        {
            var buffer = Encoding.UTF8.GetBytes( text );

            using ( var c = new MD5CryptoServiceProvider() )
                buffer = c.ComputeHash( buffer );

            var builder = new StringBuilder();

            foreach ( var b in buffer )
                builder.Append( b.ToString( "x2" ).ToLower() );

            return builder.ToString();
        }

        byte[] BuildQuery()
        {
            var builder = new StringBuilder();

            // Build the signature string.
            foreach ( var parameter in Parameters )
                builder.Append( parameter.Key + parameter.Value );

            builder.Append( LastfmGlobal.AppSecret );

            // Add the signature and format request to the parameters.
            Parameters[ "api_sig" ] = MD5Hash( builder.ToString() );
            Parameters[ "format" ] = "json";

            builder.Clear();

            // Build the query url string.
            foreach ( var parameter in Parameters )
                builder.AppendFormat( "{0}={1}&", Uri.EscapeDataString( parameter.Key ), Uri.EscapeDataString( parameter.Value ) );

            // Remove the trailing "&".
            builder.Length -= 1;

            // Convert the query to a byte array.
            return Encoding.UTF8.GetBytes( builder.ToString() );
        }
    }
}
