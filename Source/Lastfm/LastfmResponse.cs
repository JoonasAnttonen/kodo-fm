﻿using System;
using System.IO;

using Kodo.JSON;

namespace Kodo.Lastfm
{
    /// <summary> Last.fm error codes. </summary>
    public enum LastfmCode
    {
        /// <summary> OK. </summary>
        OK = 0,
        /// <summary> Invalid service - This service does not exist. </summary>
        InvalidService = 2,
        /// <summary> Invalid Method - No method with that name in this package. </summary>
        InvalidMethod = 3,
        /// <summary> Authentication Failed - You do not have permissions to access the service. </summary>
        AuthenticationFailed = 4,
        /// <summary> Invalid format - This service doesn't exist in that format. </summary>
        InvalidFormat = 5,
        /// <summary> Invalid parameters - Your request is missing a required parameter. </summary>
        InvalidParameters = 6,
        /// <summary> Invalid resource specified. </summary>
        InvalidResource = 7,
        /// <summary> Operation failed - Most likely the backend service failed. Please try again. </summary>
        TokenError = 8,
        /// <summary> Invalid session key. </summary>
        InvalidSessionKey = 9,
        /// <summary> Invalid API key - You must be granted a valid key by last.fm. </summary>
        InvalidAPIKey = 10,
        /// <summary> Service Offline - This service is temporarily offline. Try again later. </summary>
        ServiceOffline = 11,
        /// <summary> The service is temporarily unavailable, please try again. </summary>
        ServiceUnavailable = 16,
        /// <summary> Subscribers Only - This station is only available to paid last.fm subscribers. </summary>
        SubscribersOnly = 12,
        /// <summary> Invalid method signature supplied. </summary>
        InvalidSignature = 13,
        /// <summary> Unauthorized Token - This token has not been authorized. </summary>
        UnauthorizedToken = 14,
        /// <summary> This item is not available for streaming. </summary>
        ExpiredToken = 15,
        /// <summary> Login: User requires to be logged in. </summary>
        NotLoggedIn = 17,
        /// <summary> Trial Expired - This user has no free radio plays left. Subscription required. </summary>
        TrialExpired = 18,
        /// <summary> API Key Suspended - This application is not allowed to make requests to the web services. </summary>
        APIKeySuspended = 26,
        /// <summary> This error does not exist. </summary>
        DoesNotExists = 19,
        /// <summary> Not Enough Content - There is not enough content to play this station. </summary>
        NotEnoughContent = 20,
        /// <summary> Not Enough Members - This group does not have enough members for radio. </summary>
        NotEnoughMembers = 21,
        /// <summary> Not Enough Fans - This artist does not have enough fans for for radio. </summary>
        NotEnoughFans = 22,
        /// <summary> Not Enough Neighbours - There are not enough neighbours for radio. </summary>
        NotEnoughNeighbours = 23,
        /// <summary> No Peak Radio - This user is not allowed to listen to radio during peak usage. </summary>
        NoPeakRadio = 24,
        /// <summary> Radio Not Found - Radio station not found. </summary>
        RadioNotFound = 25,
        /// <summary> Deprecated - This type of request is no longer supported. </summary> 
        Deprecated = 27
    }

    /// <summary> Represents a base response from last.fm. </summary>
    public class LastfmResponse
    {
        /// <summary> Contains the unmodified response JSON. </summary>
        public JSONDocument JSON { get; private set; }

        /// <summary> The last.fm code. </summary>
        public LastfmCode Code { get; private set; }

        /// <summary> Contains the error message if the request was not accepted. </summary>
        public string Message { get; private set; }

        /// <summary> Indicates whether or not the request succeeded. </summary>
        public bool IsOK { get; private set; }

        /// <summary> Create a new instance of <see cref="LastfmResponse"/>. </summary>
        /// <param name="json"><see cref="JSONDocument"/> containing the response.</param>
        public LastfmResponse( JSONDocument json )
        {
            JSON = json;

            if ( JSON.Has( "error" ) )
            {
                IsOK = false;
                Code = (LastfmCode)int.Parse( JSON[ "error" ].Value, System.Globalization.NumberStyles.Integer, System.Globalization.CultureInfo.InvariantCulture );
                Message = JSON[ "message" ].Value;
            }
            else
            {
                IsOK = true;
                Code = LastfmCode.OK;
            }
        }
    }
}