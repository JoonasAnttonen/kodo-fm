﻿using System;

namespace Kodo.Lastfm
{
    /// <summary> 
    /// Last.fm exception. 
    /// </summary>
    public class LastfmException : Exception
    {
        /// <summary> 
        /// Create a new instance of <see cref="LastfmException"/>. 
        /// </summary>
        /// <param name="message">The message.</param>
        public LastfmException( string message ) 
            : base( message )
        {
        }
    }

    /// <summary>
    /// Global last.fm service class. 
    /// </summary>
    public static class LastfmGlobal
    {
        /// <summary> 
        /// Get or set the user agent string.
        /// </summary>
        public static string AppName
        {
            get;
            set;
        }

        /// <summary> 
        /// Get or set the API key string. 
        /// </summary>
        public static string AppKey
        {
            get;
            set;
        }

        /// <summary> 
        /// Get or set the API secret key string. 
        /// </summary>
        public static string AppSecret
        {
            get;
            set;
        }

        /// <summary> 
        /// Get or set the minimum time between requests. 
        /// </summary>
        public static TimeSpan TimeBetweenRequests { get; set; } = TimeSpan.FromSeconds( 5 );

        public static DateTime TimestampToDateTime( long timestamp )
        {
            return new DateTime( 1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc ).AddSeconds( timestamp ).ToLocalTime();
        }

        public static long DateTimeToUTCTimestamp( DateTime dateTime )
        {
            var baseDate = new DateTime( 1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc );
            var span = dateTime.ToUniversalTime() - baseDate;

            return (long)span.TotalSeconds;
        }
    }
}