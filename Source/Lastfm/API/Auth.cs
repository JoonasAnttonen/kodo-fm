﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kodo.JSON;

namespace Kodo.Lastfm.API
{
    /// <summary>
    /// Authentication response.
    /// </summary>
    public class LastfmAuthenticationResponse : LastfmResponse
    {
        /// <summary>
        /// Contains the username that was used in the request.
        /// </summary>
        public string Username
        {
            get;
            private set;
        }

        /// <summary>
        /// Contains the session key if the request was accepted.
        /// </summary>
        public string Session
        {
            get;
            private set;
        }

        /// <summary>
        /// Create a new istance of LastfmAuthenticationResponse with the specified xml.
        /// </summary>
        /// <param name="responseJson">XmlDocument containing the last.fm response.</param>
        public LastfmAuthenticationResponse( JSONDocument responseJson )
            : base( responseJson )
        {
            if ( IsOK )
            {
                var sessionNode = JSON[ "session" ];
                Username = sessionNode[ "name" ].Value;
                Session = sessionNode[ "key" ].Value;
            }
            else
            {
                Username = string.Empty;
                Session = string.Empty;
            }
        }
    }

    /// <summary>
    /// Last.fm API authentication methods.
    /// </summary>
    public static class Auth
    {
        /// <summary>
        /// Create a web service session for a user.
        /// </summary>
        /// <param name="profileName">last.fm profile name.</param>
        /// <param name="profilePassword">last.fm profile password.</param>
        public static LastfmAuthenticationResponse GetMobileSession( string profileName, string profilePassword )
        {
            // Assert required parameters.
            Debug.Assert( !string.IsNullOrEmpty( profileName ) );
            Debug.Assert( !string.IsNullOrEmpty( profilePassword ) );

            // Create the new request.
            var request = new LastfmRequest( "auth.getMobileSession" );
            request.Https = true;
            request.Parameters[ "username" ] = profileName;
            request.Parameters[ "password" ] = profilePassword;

            // Make the request.
            return new LastfmAuthenticationResponse( request.Execute() );
        }

        /// <summary>
        /// Create a web service session for a user.
        /// </summary>
        /// <param name="profileName">last.fm profile name.</param>
        /// <param name="profilePassword">last.fm profile password.</param>
        public static async Task<LastfmAuthenticationResponse> GetMobileSessionAsync( string profileName, string profilePassword )
        {
            return await Task.Run( () => { return GetMobileSession( profileName, profilePassword ); } );
        }
    }
}
