﻿using System;

using Kodo.JSON;

namespace Kodo.Lastfm.API
{
    /// <summary>
    /// Last.fm ignore codes.
    /// </summary>
    public enum LastfmIgnoreCode
    {
        /// <summary> The request passed all filters. </summary>
        None = 0,
        /// <summary> Artist ignored. </summary>
        Artist = 1,
        /// <summary> Track ignored. </summary>
        Track = 2,
        /// <summary> Timestamp was too old. </summary>
        TimestampOld = 3,
        /// <summary> Timestamp was too new. </summary>
        TImestampNew = 4,
        /// <summary> Daily scrobble limit exceeded. </summary>
        MaxScrobbles = 5
    }

    public class LastfmCorrections
    {
        /// <summary> 
        /// Empty corrections data. 
        /// </summary>
        public static readonly LastfmCorrections Empty = new LastfmCorrections();

        /// <summary> Indicates whether or not the track field was corrected. </summary>
        public bool TrackCorrected { get { return !string.IsNullOrEmpty( Track ); } }
        /// <summary> Indicates whether or not the artist field was corrected. </summary>
        public bool ArtistCorrected { get { return !string.IsNullOrEmpty( Artist ); } }
        /// <summary> Indicates whether or not the album field was corrected. </summary>
        public bool AlbumCorrected { get { return !string.IsNullOrEmpty( Album ); } }
        /// <summary> Indicated whether or not the album artist field was corrected. </summary>
        public bool AlbumArtistCorrected { get { return !string.IsNullOrEmpty( AlbumArtist ); } }

        /// <summary> Contains the corrected track field. </summary>
        public string Track { get; }
        /// <summary> Contains the corrected artist field. </summary>
        public string Artist { get; }
        /// <summary> Contains the corrected album field. </summary>
        public string Album { get; }
        /// <summary> Contains the corrected album artist field. </summary>
        public string AlbumArtist { get; }

        /// <summary> 
        /// Create a new <see cref="LastfmCorrections"/>. 
        /// </summary>
        public LastfmCorrections() { }

        /// <summary> 
        /// Create a new <see cref="LastfmCorrections"/> from the specified <see cref="JSONNode"/>. 
        /// </summary>
        /// <param name="json"><see cref="JSONNode"/> containing the correction data.</param>
        public LastfmCorrections( JSONNode json )
        {
            var node = json[ "track" ];

            if ( node[ "corrected" ].Value == "1" )
                Track = node[ "#text" ].Value;

            node = json[ "artist" ];

            if ( node[ "corrected" ].Value == "1" )
                Artist = node[ "#text" ].Value;

            node = json[ "album" ];

            if ( node[ "corrected" ].Value == "1" )
                Album = node[ "#text" ].Value;

            node = json[ "albumArtist" ];

            if ( node[ "corrected" ].Value == "1" )
                AlbumArtist = node[ "#text" ].Value;
        }
    }

    /// <summary>
    /// Track update/scrobble response.
    /// </summary>
    public class LastfmTrackResponse : LastfmResponse
    {
        /// <summary> 
        /// Contains the track information with corrections, if any. 
        /// </summary>
        public LastfmCorrections Corrections { get; }

        /// <summary> 
        /// Indicated whether or not the track was ignored. 
        /// </summary>
        public bool Ignored { get; private set; }
        /// <summary> 
        /// Indicates the ignore code if track was not accepted. 
        /// </summary>
        public LastfmIgnoreCode IgnoreCode { get; }
        /// <summary> 
        /// Contains the ignore message if track was not accepted. 
        /// </summary>
        public string IgnoreMessage { get; }

        /// <summary> 
        /// Create a new <see cref="LastfmTrackResponse"/> from the specified <see cref="JSONDocument"/>. 
        /// </summary>
        /// <param name="json"><see cref="JSONDocument"/> containing the last.fm response.</param>
        public LastfmTrackResponse( JSONDocument json )
            : base( json )
        {
            if ( !IsOK )
                return;

            var correctionNode = json[ "track" ];

            if ( correctionNode != null )
                Corrections = new LastfmCorrections( json );
            else
                Corrections = LastfmCorrections.Empty;

            var ignoreNode = json[ "ignoredMessage" ];

            if ( ignoreNode != null )
            {
                IgnoreCode = (LastfmIgnoreCode)int.Parse( ignoreNode[ "code" ].Value );
                Ignored = IgnoreCode != LastfmIgnoreCode.None;
                IgnoreMessage = ignoreNode[ "#text" ].Value;
            }
        }
    }

    /// <summary>
    /// Last.fm API track methods.
    /// </summary>
    public static class Track
    {
        /// <summary> 
        /// Notify Last.fm that a user has started listening to a track.
        ///  </summary>
        /// <param name="session">Session key generated by authenticating a user via the authentication protocol.</param>
        /// <param name="track">Track name</param>
        /// <param name="artist">Artist name.</param>
        /// <param name="album">Album name.</param>
        /// <param name="duration">Length of the track in seconds.</param>
        public static LastfmTrackResponse UpdateNowPlaying( string session, string track, string artist, string album = null, string duration = null )
        {
            // Parameter checks.
            if ( string.IsNullOrEmpty( session ) )
                throw new ArgumentException( "Session key can't be null/empty." );
            if ( string.IsNullOrEmpty( track ) )
                throw new ArgumentException( "Track can't be null/empty." );
            if ( string.IsNullOrEmpty( artist ) )
                throw new ArgumentException( "Artist can't be null/empty." );

            // Create the new request.
            var request = new LastfmRequest( "track.updateNowPlaying", session );
            request.Parameters[ "track" ] = track;
            request.Parameters[ "artist" ] = artist;

            // Optional parameters.
            if ( !string.IsNullOrEmpty( album ) )
                request.Parameters[ "album" ] = album;
            if ( !string.IsNullOrEmpty( duration ) )
                request.Parameters[ "duration" ] = duration;

            // Make the request.
            return new LastfmTrackResponse( request.Execute() );
        }

        /// <summary> 
        /// Add a track-play to a user's profile. 
        /// </summary>
        /// <param name="session">A session key generated by authenticating a user via the authentication protocol.</param>
        /// <param name="timestamp">The time the track started playing, in UNIX timestamp format (integer number of seconds since 00:00:00, January 1st 1970 UTC). This must be in the UTC time zone.</param>
        /// <param name="track">Track name.</param>
        /// <param name="artist">Artist name.</param>
        /// <param name="album">Album name.</param>
        public static LastfmTrackResponse Scrobble( string session, string timestamp, string track, string artist, string album = null )
        {
            // Parameter checks.
            if ( string.IsNullOrEmpty( session ) )
                throw new ArgumentException( "Session key can't be null/empty." );
            if ( string.IsNullOrEmpty( timestamp ) )
                throw new ArgumentException( "Timestamp can't be null/empty." );
            if ( string.IsNullOrEmpty( track ) )
                throw new ArgumentException( "Track can't be null/empty." );
            if ( string.IsNullOrEmpty( artist ) )
                throw new ArgumentException( "Artist can't be null/empty." );

            // Create the new request.
            var request = new LastfmRequest( "track.scrobble", session );
            request.Parameters[ "timestamp" ] = timestamp;
            request.Parameters[ "track" ] = track;
            request.Parameters[ "artist" ] = artist;

            // Optional parameters.
            if ( !string.IsNullOrEmpty( album ) )
                request.Parameters[ "album" ] = album;

            // Make the request.
            return new LastfmTrackResponse( request.Execute() );
        }

        /// <summary>
        /// Love a track.
        /// </summary>
        /// <param name="session">A session key generated by authenticating a user via the authentication protocol.</param>
        /// <param name="track">Track name.</param>
        /// <param name="artist">Artist name.</param>
        public static LastfmResponse Love( string session, string track, string artist )
        {
            // Parameter checks.
            if ( string.IsNullOrEmpty( session ) )
                throw new ArgumentException( "Session key can't be null/empty." );
            if ( string.IsNullOrEmpty( track ) )
                throw new ArgumentException( "Track can't be null/empty." );
            if ( string.IsNullOrEmpty( artist ) )
                throw new ArgumentException( "Artist can't be null/empty." );

            // Create the request.
            var request = new LastfmRequest( "track.love", session );
            request.Parameters[ "track" ] = track;
            request.Parameters[ "artist" ] = artist;

            // Make the request.
            return new LastfmResponse( request.Execute() );
        }

        /// <summary>
        /// Unlove a track.
        /// </summary>
        /// <param name="session">A session key generated by authenticating a user via the authentication protocol.</param>
        /// <param name="track">Track name.</param>
        /// <param name="artist">Artist name.</param>
        public static LastfmResponse Unlove( string session, string track, string artist )
        {
            // Parameter checks.
            if ( string.IsNullOrEmpty( session ) )
                throw new ArgumentException( "Session key can't be null/empty." );
            if ( string.IsNullOrEmpty( track ) )
                throw new ArgumentException( "Track can't be null/empty." );
            if ( string.IsNullOrEmpty( artist ) )
                throw new ArgumentException( "Artist can't be null/empty." );

            // Create the request.
            var request = new LastfmRequest( "track.unlove", session );
            request.Parameters[ "track" ] = track;
            request.Parameters[ "artist" ] = artist;

            // Make the request.
            return new LastfmResponse( request.Execute() );
        }
    }
}
