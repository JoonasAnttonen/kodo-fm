﻿using System;
using System.Collections.Generic;

using Kodo.JSON;

namespace Kodo.Lastfm.API
{
    public class LastfmAlbumTrack
    {
        /// <summary> 
        /// The name of this track. 
        /// </summary>
        public string Name { get; private set; }
        /// <summary> 
        /// The artist of this track. 
        /// </summary>
        public string Artist { get; private set; }
        /// <summary> 
        /// The number of this track. 
        /// </summary>
        public int Rank { get; private set; }

        /// <summary> 
        /// Create a new <see cref="LastfmAlbumTrack"/>.
        ///  </summary>
        /// <param name="name">The name of the track.</param>
        /// <param name="artist">The artist of the track.</param>
        /// <param name="rank">The number of the track.</param>
        public LastfmAlbumTrack( string name, string artist, int rank )
        {
            Name = name;
            Artist = artist;
            Rank = rank;
        }
    }

    public class LastfmAlbum : LastfmResponse
    {
        /// <summary> 
        /// The name of this album. 
        /// </summary>
        public string Name { get; }
        /// <summary> 
        /// The artist of this album. 
        /// </summary>
        public string Artist { get; }
        /// <summary> 
        /// The release date of this album. 
        /// </summary>
        public DateTime Release { get; }
        /// <summary> 
        /// The list of image URLs for this album. 
        /// </summary>
        public Dictionary<string, string> Images { get; } = new Dictionary<string, string>();
        /// <summary> 
        /// The list of tracks in this album. 
        /// </summary>
        public List<LastfmAlbumTrack> Tracks { get; } = new List<LastfmAlbumTrack>();

        /// <summary> 
        /// Create a new <see cref="LastfmAlbum"/> from the specified <see cref="JSONDocument"/>. 
        /// </summary>
        /// <param name="json"><see cref="JSONDocument"/> containing the last.fm response.</param>
        public LastfmAlbum( JSONDocument json )
            : base( json )
        {
            if ( !IsOK )
                return;

            var albumNode = json[ "album" ];

            Name = albumNode[ "name" ].Value;
            Artist = albumNode[ "artist" ].Value;
            Release = new DateTime();
            //Release = DateTime.Parse( albumNode[ "releasedate" ].Value );

            var imageNode = albumNode[ "image" ];

            foreach ( var image in imageNode.Members )
                Images.Add( image[ "size" ].Value, image[ "#text" ].Value );

            var trackNode = albumNode[ "tracks" ][ "track" ];

            foreach ( var track in trackNode.Members )
                Tracks.Add( new LastfmAlbumTrack( track[ "name" ].Value, track[ "artist" ][ "name" ].Value, int.Parse( track[ "@attr" ][ "rank" ].Value ) ) );
        }
    }

    /// <summary>
    /// Last.fm API album methods
    /// </summary>
    public static class Album
    {
        /// <summary> 
        /// Get the metadata of an album on Last.fm using the album name. 
        /// </summary>
        /// <param name="artist">The artist name.</param>
        /// <param name="album">The album name.</param>
        public static LastfmAlbum GetInfo( string artist, string album )
        {
            // Parameter checks.
            if ( string.IsNullOrEmpty( artist ) )
                throw new ArgumentException( "Artist can't be null/empty." );
            if ( string.IsNullOrEmpty( album ) )
                throw new ArgumentException( "Album can't be null/empty." );

            // Create the request.
            var request = new LastfmRequest( "album.getInfo" );
            request.Parameters[ "album" ] = album;
            request.Parameters[ "artist" ] = artist;

            // Make the request.
            return new LastfmAlbum( request.Execute() );
        }
    }
}
