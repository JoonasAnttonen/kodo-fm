﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using KodoFM.Audio;

using Kodo.Lastfm;
using Kodo.Lastfm.API;

namespace KodoFM
{
    /// <summary> 
    /// Contains the last.fm result and tag data corrections, if any. 
    /// </summary>
    public struct ScrobbleResult
    {
        /// <summary> 
        /// The result. 
        /// </summary>
        public readonly LastfmResult Result;

        /// <summary> 
        /// The corrections. 
        /// </summary>
        public readonly LastfmCorrections Corrections;

        /// <summary> 
        /// Indicates whether or not there are any corrections. 
        /// </summary>
        public bool HasCorrections
        {
            get { return Corrections.TrackCorrected || Corrections.ArtistCorrected || Corrections.AlbumCorrected; }
        }

        /// <summary> 
        /// Indicates whether or not the operation was a success. 
        /// </summary>
        public bool IsOK
        {
            get { return Result == LastfmResult.OK; }
        }

        public ScrobbleResult( LastfmResult result ) : this( result, LastfmCorrections.Empty ) { }
        public ScrobbleResult( LastfmResult result, LastfmCorrections corrections )
        {
            Result = result;
            Corrections = corrections;
        }
    }

    public struct Result<T>
    {
        /// <summary> 
        /// The result. 
        /// </summary>
        public readonly LastfmResult Code;

        /// <summary> 
        /// The data. 
        /// </summary>
        public readonly T Data;

        /// <summary> 
        /// Indicates whether or not the operation was a success. 
        /// </summary>
        public bool IsOK
        {
            get { return Code == LastfmResult.OK; }
        }

        public Result( LastfmResult result ) : this( result, default( T ) ) { }
        public Result( LastfmResult result, T data )
        {
            Code = result;
            Data = data;
        }
    }

    /// <summary> 
    /// Contains the username and sessions key that identify a last.fm session. 
    /// </summary>
    public struct LastfmSession
    {
        public static readonly LastfmSession Invalid = new LastfmSession();

        /// <summary> 
        /// The username. 
        /// </summary>
        public string User { get; private set; } // Setter for serialization purposes!

        /// <summary> 
        /// The session key. 
        /// </summary>
        public string Key { get; private set; } // Setter for serialization purposes!

        /// <summary> 
        /// Indicates whether or not the session appears to be valid. 
        /// </summary>
        public bool Valid
        {
            get { return !string.IsNullOrEmpty( User ) && !string.IsNullOrEmpty( Key ); }
        }

        public LastfmSession( string user, string key )
        {
            User = user;
            Key = key;
        }
    }

    /// <summary> 
    /// LastfmInterface result codes. 
    /// </summary>
    public enum LastfmResult
    {
        /// <summary> 
        /// OK. 
        /// </summary>
        OK,
        /// <summary> 
        /// Something went wrong and you should try again later. 
        /// </summary>
        TryAgainLater,
        /// <summary> 
        /// No profile has been logged in. 
        /// </summary>
        NotLoggedIn,
        /// <summary> 
        /// The stream has not been played long enough. 
        /// </summary>
        NotPlayedEnough,
        /// <summary> 
        /// Supplied account credentials were invalid. 
        /// </summary>
        InvalidCredentials,
        /// <summary> 
        /// The stream was invalid. 
        /// </summary>
        InvalidStream,
        /// <summary> 
        /// There was an internal error. 
        /// </summary>
        InternalError,
    }

    /// <summary>
    /// Interface between kodo-fm and last.fm.
    /// </summary>
    public class LastfmInterface
    {
        HashSet<DateTime> scrobbleSet = new HashSet<DateTime>();
        Queue<AudioStream> scrobbleQueue = new Queue<AudioStream>();

        LastfmSession session;
        float limitOfNowPlaying = 0.01f;
        float limitOfScrobble = 0.5f;

        /// <summary> 
        /// Indicates whether or not a user is logged in. 
        /// </summary>
        public bool LoggedIn
        {
            get
            {
                bool result;

                lock ( scrobbleQueue )
                {
                    result = session.Valid;
                }

                return result;
            }
        }

        /// <summary> 
        /// Get the last.fm session data. 
        /// </summary>
        public LastfmSession Session
        {
            get { return session; }
        }

        /// <summary> 
        /// Get or set the percentage played required to send the now playing to last.fm. 
        /// Value will be clamped between 0.01 and 0.5. 
        /// </summary>
        public float LimitOfNowPlaying
        {
            get { return limitOfNowPlaying; }
            set { limitOfNowPlaying = KodoFMUtil.Clamp( value, 0.01f, 0.5f ); }
        }

        /// <summary> 
        /// Get or set the percentage played required to send the scrobble to last.fm. 
        /// Value will be clamped between 0.5 and 1.0. 
        /// </summary>
        public float LimitOfScrobble
        {
            get { return limitOfScrobble; }
            set { limitOfScrobble = KodoFMUtil.Clamp( value, 0.5f, 1.0f ); }
        }

        /// <summary> 
        /// Create a new <see cref="LastfmInterface"/>. 
        /// </summary>
        public LastfmInterface()
        {
        }

        /// <summary> 
        /// Create a new <see cref="LastfmInterface"/> from the specified session. 
        /// </summary>
        public LastfmInterface( LastfmSession session )
        {
            this.session = session;
        }

        /// <summary> 
        /// Get the number of enqueued scrobbles. 
        /// </summary>
        public int GetQueueLength()
        {
            return scrobbleQueue.Count;
        }

        /// <summary> 
        /// Enqueue a stream to the scrobble queue. 
        /// </summary>
        public bool Enqueue( AudioStream stream )
        {
            if ( !scrobbleSet.Contains( stream.Timestamp ) )
            {
                scrobbleQueue.Enqueue( stream );
                scrobbleSet.Add( stream.Timestamp );
                return true;
            }

            return false;
        }

        /// <summary> 
        /// Dequeue a stream from the scrobble queue. 
        /// </summary>
        public bool Dequeue( out AudioStream stream )
        {
            if ( scrobbleQueue.Count > 0 )
            {
                stream = scrobbleQueue.Dequeue();
                scrobbleSet.Remove( stream.Timestamp );
                return true;
            }

            stream = AudioStream.Empty;
            return false;
        }

        /// <summary> 
        /// Indicates whether or not the specified stream is valid for last.fm scrobbling. 
        /// </summary>
        /// <param name="stream">The stream to test.</param>
        public bool IsStreamValidForLastfm( AudioStream stream )
        {
            if ( string.IsNullOrEmpty( stream.Info.Title ) )
                return false;
            if ( string.IsNullOrEmpty( stream.Info.Artist ) || string.Equals( stream.Info.Artist, AudioInfo.UnknownArtistString ) )
                return false;
            if ( stream.Info.Length <= 30000 )
                return false;

            return true;
        }

        /// <summary> Login the specified last.fm profile with the specified password. Calls Task.Run so you dont have to. </summary>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// <returns>The result.</returns>
        public async Task<LastfmResult> LoginAsync( string username, string password )
        {
            if ( string.IsNullOrEmpty( username ) || string.IsNullOrEmpty( password ) )
                return LastfmResult.InvalidCredentials;

            return await Task.Run( () =>
            {
                return GetResult( () =>
                {
                    lock ( scrobbleQueue )
                    {
                        var response = Auth.GetMobileSession( username, password );

                        if ( response.IsOK )
                        {
                            session = new LastfmSession( username, response.Session );
                        }

                        return response;
                    }
                } );
            } );
        }

        /// <summary> 
        /// Logout the current last.fm profile. 
        /// </summary>
        public void Logout()
        {
            lock ( scrobbleQueue )
            {
                session = new LastfmSession( string.Empty, string.Empty );
            }
        }

        /// <summary> 
        /// Set the specified stream as nowplaying in the users last.fm profile.
        /// </summary>
        /// <param name="stream">The stream.</param>
        /// <returns>The result.</returns>
        public async Task<ScrobbleResult> NowPlayingAsync( AudioStream stream )
        {
            if ( !IsStreamValidForLastfm( stream ) )
                return new ScrobbleResult( LastfmResult.InvalidStream );

            if ( !LoggedIn )
                return new ScrobbleResult( LastfmResult.NotLoggedIn );

            if ( CalculatePlayedPercentage( stream ) < limitOfNowPlaying )
                return new ScrobbleResult( LastfmResult.NotPlayedEnough );

            return await Task.Run( () =>
            {
                LastfmCorrections corrections = LastfmCorrections.Empty;
                LastfmResult result = GetResult( () =>
                {
                    LastfmSession localSession;

                    lock ( scrobbleQueue )
                    {
                        localSession = session;
                    }

                    var response = Track.UpdateNowPlaying(
                       localSession.Key,
                       stream.Info.Title,
                       stream.Info.Artist,
                       stream.Info.Album,
                       ((int)TimeSpan.FromMilliseconds( stream.Info.Length ).TotalSeconds).ToString() );

                    if ( response.IsOK )
                    {
                        corrections = response.Corrections;
                    }

                    return response;

                } );

                return new ScrobbleResult( result, corrections );
            } );
        }

        /// <summary> 
        /// Add the specified stream as listened to the users last.fm profile.
        /// </summary>
        /// <param name="stream">The stream.</param>
        /// <returns>The result.</returns>
        public async Task<ScrobbleResult> ScrobbleAsync( AudioStream stream )
        {
            if ( !IsStreamValidForLastfm( stream ) )
                return new ScrobbleResult( LastfmResult.InvalidStream );

            if ( !LoggedIn )
                return new ScrobbleResult( LastfmResult.NotLoggedIn );

            if ( CalculatePlayedPercentage( stream ) < limitOfScrobble )
                return new ScrobbleResult( LastfmResult.NotPlayedEnough );

            return await Task.Run( () =>
            {
                LastfmCorrections corrections = LastfmCorrections.Empty;
                LastfmResult result = GetResult( () =>
                {
                    LastfmSession localSession;

                    lock ( scrobbleQueue )
                    {
                        localSession = session;
                    }

                    var response = Track.Scrobble(
                        localSession.Key,
                        LastfmGlobal.DateTimeToUTCTimestamp( stream.Timestamp ).ToString(),
                        stream.Info.Title,
                        stream.Info.Artist,
                        stream.Info.Album );

                    if ( response.IsOK )
                    {
                        corrections = response.Corrections;
                    }

                    return response;

                } );

                return new ScrobbleResult( result, corrections );
            } );
        }

        /// <summary>
        /// Get album info from last.fm.
        /// </summary>
        /// <param name="artist">The artist.</param>
        /// <param name="album">The album.</param>
        /// <returns>The result.</returns>
        public async Task<Result<LastfmAlbum>> GetAlbumInfoAsync( string artist, string album )
        {
            return await Task.Run( () =>
            {
                LastfmAlbum info = null;
                LastfmResult result = GetResult( () =>
                {
                    var response = Album.GetInfo( artist, album );

                    if ( response.IsOK )
                    {
                        info = response;
                    }

                    return response;

                } );

                return new Result<LastfmAlbum>( result, info );
            } );
        }

        static float CalculatePlayedPercentage( AudioStream stream )
        {
            // Calculate percentage played -- Over 4 minutes (240000 ms) of playtime can be scrobbled regardless, according to last.fm
            if ( stream.Played < 240000 )
                return (float)stream.Played / stream.Length;
            return 1f;
        }

        static LastfmResult GetResult( Func<LastfmResponse> request )
        {
            LastfmResult result;

            try
            {
                var response = request();

                switch ( response.Code )
                {
                    case LastfmCode.OK:
                        result = LastfmResult.OK;
                        break;
                    case LastfmCode.NotLoggedIn:
                    case LastfmCode.InvalidSessionKey:
                        result = LastfmResult.NotLoggedIn;
                        break;
                    case LastfmCode.TokenError:
                    case LastfmCode.ServiceOffline:
                    case LastfmCode.ServiceUnavailable:
                        result = LastfmResult.TryAgainLater;
                        break;
                    default:
                        result = LastfmResult.InternalError;
                        break;
                }
            }
            catch ( System.Net.WebException )
            {
                result = LastfmResult.TryAgainLater;
            }
            catch ( Kodo.JSON.JSONException )
            {
                result = LastfmResult.TryAgainLater;
            }
            catch ( Exception )
            {
                result = LastfmResult.InternalError;
            }

            return result;
        }
    }
}
