﻿using System;
using System.Collections.Generic;

using KodoFM.Audio;
using Kodo.Graphics;
using Kodo.Graphics.Style;

namespace KodoFM
{
    public class PlaylistView : ListView<AudioItem>
    {
        AudioPlaylist playlist = AudioPlaylist.Empty;

        /// <summary> 
        /// Get or set the <see cref="AudioPlaylist"/> to be viewed. 
        /// </summary>
        public AudioPlaylist Playlist
        {
            get { return playlist; }
            set
            {
                if ( playlist != value )
                {
                    // Disconnect from the old playlist.	
                    if ( playlist != null )
                    {
                        playlist.OnModified -= OnPlaylistModified;
                        playlist.OnActivated -= OnPlaylistActivated;
                    }

                    playlist = value;

                    // Connect to the new playlist.
                    if ( playlist != null )
                    {
                        playlist.OnModified += OnPlaylistModified;
                        playlist.OnActivated += OnPlaylistActivated;
                    }
                    else
                    {
                        playlist = AudioPlaylist.Empty;
                    }
                }

                OnPlaylistModified();
            }
        }

        /// <summary> 
        /// Create a new <see cref="PlaylistView"/>.
        /// </summary>
        /// <param name="window">The containing <see cref="Window"/>.</param>
        public PlaylistView( Window window )
            : base( window )
        {
        }

        /// <summary> 
        /// Get the currectly selected items as <see cref="AudioID"/>s. 
        /// </summary>
        public List<AudioID> GetSelectedIDs()
        {
            var result = new List<AudioID>();

            for ( var i = 0; i < playlist.Count; i++ )
            {
                if ( IsSelected( i ) )
                {
                    result.Add( playlist.Get( i ).ID );
                }
            }

            return result;
        }

        // ________________ AudioPlaylist event handlers ___________________________________________________________

        void OnPlaylistModified()
        {
            Reload();
        }

        void OnPlaylistActivated( AudioID item )
        {
            CenterToActiveLazy();
        }

        // ________________ ListView implementation ___________________________________________________________

        protected override AudioItem Get( int index )
        {
            return playlist.Get( index );
        }

        protected override int GetCount()
        {
            return playlist.Count;
        }

        public override int GetActiveIndex()
        {
            return playlist.GetActiveIndex();
        }

        protected override void SetActiveIndex( int index )
        {
            playlist.SetActiveIndex( index );
        }

        protected override void Remove( List<AudioItem> items )
        {
            playlist.RemoveRange( items );
        }

        protected override void Swap( int x, int y )
        {
            playlist.SwapByIndex( x, y );
        }

        protected override bool Match( int index, string search )
        {
            var item = playlist.Get( index );
            return item.Path.IndexOf( search, StringComparison.OrdinalIgnoreCase ) != -1 ||
                   item.Title.IndexOf( search, StringComparison.OrdinalIgnoreCase ) != -1 ||
                   item.Artist.IndexOf( search, StringComparison.OrdinalIgnoreCase ) != -1 ||
                   item.Album.IndexOf( search, StringComparison.OrdinalIgnoreCase ) != -1;
        }

        protected override void OnDrawItem( Context context, AudioItem item, Rectangle area, bool selected, bool active )
        {
            SharedBrush.Color = active ? AccentColor : Color.GhostWhite;

            const float itemWidthExtra = 50;
            var itemHeightHalf = ActualItemHeight / 2;
            var itemWidth = area.Width - 4 - itemWidthExtra;

            var titleRect = Rectangle.FromXYWH( area.Left + 2, area.Top, itemWidth, itemHeightHalf );
            var artistRect = Rectangle.FromXYWH( area.Left + 2, area.Top + itemHeightHalf, 200, itemHeightHalf );
            var albumRect = Rectangle.FromXYWH( artistRect.Right, area.Top + itemHeightHalf, itemWidth - 200, itemHeightHalf );
            var lengthRect = Rectangle.FromXYWH( area.Left + 2 + itemWidth, area.Top, itemWidthExtra, ActualItemHeight );

            TextStyle.Alignment = TextStyleAlignment.LeftTop;
            context.DrawText( item.Title, TextStyle, titleRect, SharedBrush );

            TextStyle.Alignment = TextStyleAlignment.RightCenter;
            context.DrawText( item.Length, TextStyle, lengthRect, SharedBrush );

            SharedBrush.Color = Color.SlateGray;
            TextStyle.Alignment = TextStyleAlignment.LeftBottom;
            context.DrawText( item.Album, TextStyle, artistRect, SharedBrush );
            context.DrawText( item.Artist, TextStyle, albumRect, SharedBrush );
        }
    }
}