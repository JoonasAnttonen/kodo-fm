﻿using System;

using KodoFM.Audio;
using Kodo.Graphics;
using Kodo.Graphics.Style;

namespace KodoFM
{
    public class InfoView : Control
    {
        Timer renderTimer;

        bool fadeIn;

        readonly Size SizeOfArtFrame = new Size( 100, 100 );
        readonly Size SizeOfArt = new Size( 90, 90 );

        BitmapAsset art;
        Bitmap artBitmap;
        Bitmap threeDotsBitmap;

        Rectangle threeDotsArea;
        Rectangle artPlaceholderArea;
        Rectangle artArea;
        Rectangle textArea;

        TextLayout streamTextLayout;
        TextLayout updateTextLayout;

        const float OpacityChangePerTick = 0.05f;
        const float OpacityMin = 0;
        const float OpacityMax = 1;
        const int OpacityTickRate = 10;
        float opacity = 1;

        AudioInfo info;
        AudioInfo infoStore;

        string streamTitle;
        string streamArtist;
        string streamAlbum;
        string streamPlaylist;

        const string UpdateAvailable = "A new version is available";

        bool artPlaceholder;
        bool updateAvailable;
        bool mouseIsHoveringThreeDots;
        bool mouseIsHoveringPlaceholder;
        bool mouseIsHoveringOnUpdate;
        bool mouseIsDown;

        public event DefaultEventHandler OnArtClick;
        public event DefaultEventHandler OnUpdateClick;

        public InfoView( Window window )
            : base( window )
        {
            renderTimer = new Timer( OnFrame );
            renderTimer.Interval = OpacityTickRate;
        }

        protected override void OnLoad( Context context )
        {
            base.OnLoad( context );

            threeDotsBitmap = new Bitmap( context, Global.Icons.ThreeDots.Source );

            TextStyle.Alignment = TextStyleAlignment.RightBottom;
            updateTextLayout = new TextLayout( UpdateAvailable, TextStyle, float.MaxValue, float.MaxValue );
        }

        protected override void OnUpdate( Context context )
        {
            var clip = new Rectangle( Area.Dimensions );
            var clipInner = new Rectangle( Area.Dimensions );
            clipInner = clipInner.Adjust( -20, -10 ).Move( 10, 5 );

            threeDotsArea = Rectangle.FromXYWH( clip.Right - 36, clip.Top, 32, 32 );
            textArea = clipInner;
            artArea = Rectangle.FromXYWH( clipInner.Left, clipInner.Top + (clipInner.Height - SizeOfArtFrame.Height) / 2, SizeOfArtFrame.Width, SizeOfArtFrame.Height );

            if ( art != null )
            {
                artPlaceholderArea = new Rectangle( art.Size ).CenterTo( artArea );

                textArea = new Rectangle( artArea.Right + 10, artArea.Top, clipInner.Right, artArea.Bottom );
                artBitmap = artBitmap ?? new Bitmap( context, art.Source );
            }

            if ( info == null )
            {
                TextStyle.Alignment = TextStyleAlignment.CenterCenter;

                streamTextLayout = new TextLayout( Global.ApplicationName, TextStyle, textArea.Width, textArea.Height );
            }
            else
            {
                TextStyle.Alignment = TextStyleAlignment.LeftCenter;

                var highlightedTextRange = new TextRange( 0, streamTitle.Length );

                streamTextLayout = new TextLayout( $"{ streamTitle }\n{ streamArtist }\n{ streamAlbum }", TextStyle, textArea.Width, textArea.Height );
                streamTextLayout.SetFontSize( TextStyle.Format.FontSize + 3, highlightedTextRange );
                streamTextLayout.SetFontWeight( FontWeight.Bold, highlightedTextRange );
            }

            updateTextLayout.SetMaximumSize( textArea.Dimensions );
        }

        protected override void OnDraw( Context context )
        {
            var clip = new Rectangle( Area.Dimensions );

            context.AntialiasMode = AntialiasMode.PerPrimitive;
            SharedBrush.Opacity = opacity;

            SharedStyle.Align( clip );
            context.FillRectangle( clip, SharedStyle.Background );
            context.FillRectangle( clip, SharedStyle.Foreground );

            SharedBrush.Color = Color.GhostWhite;
            context.DrawTextLayout( streamTextLayout, textArea.Location, SharedBrush, DrawTextOptions.NoSnap );

            if ( updateAvailable )
            {
                SharedBrush.Color = AccentColor;
                context.DrawTextLayout( updateTextLayout, textArea.Location, SharedBrush, DrawTextOptions.NoSnap );
            }

            if ( info != null && artBitmap != null )
            {
                SharedBrush.Color = mouseIsHoveringThreeDots ? AccentColor : Color.FromAColor( 0.5f, Color.GhostWhite );

                context.AntialiasMode = AntialiasMode.Aliased;
                context.FillOpacityMask( threeDotsBitmap, SharedBrush, threeDotsArea );
                context.AntialiasMode = AntialiasMode.PerPrimitive;

                if ( artPlaceholder )
                {
                    SharedBrush.Color = Color.GhostWhite;

                    context.AntialiasMode = AntialiasMode.Aliased;
                    context.FillOpacityMask( artBitmap, SharedBrush, artPlaceholderArea );
                    context.AntialiasMode = AntialiasMode.PerPrimitive;
                }
                else
                {
                    SharedBrush.Color = Color.Black;
                    context.DrawRectangle( artArea, SharedBrush, 4 );
                    context.DrawBitmap( artBitmap, artArea, opacity, InterpolationMode.Linear );
                }
            }

            SharedBrush.Opacity = 1;
            SharedBrush.Color = Color.Black;
            context.DrawRectangle( clip, SharedBrush, 2 );
        }

        public void SetUpdateAvailable( bool available )
        {
            if ( updateAvailable != available )
            {
                updateAvailable = available;
                Refresh();
            }
        }

        public void SetView( AudioStream value, string playlist )
        {
            if ( value == AudioStream.Empty )
                return;

            SetView( value.Info, playlist );
        }

        public void SetView( AudioInfo setInfo, string playlist )
        {
            infoStore = setInfo;
            streamPlaylist = playlist;

            BeginFlip();
        }

        public void Clear()
        {
            infoStore = null;

            BeginFlip();
        }

        void BeginFlip()
        {
            fadeIn = false;
            renderTimer.Start();
        }

        void Flip()
        {
            info = infoStore;
            infoStore = null;

            art = null;
            artBitmap?.Dispose();
            artBitmap = null;

            artPlaceholder = true;

            if ( info != null )
            {
                streamTitle = info.MakeTitleString();
                streamArtist = info.MakeFieldString( info.Artist, AudioInfo.UnknownArtistString );
                streamAlbum = info.MakeFieldString( info.Album, AudioInfo.UnknownAlbumString );

                using ( var albumArt = AudioCache.GetAlbumArt( streamAlbum, streamArtist, info.Path ) )
                {
                    if ( albumArt != null )
                    {
                        artPlaceholder = false;
                        art = new BitmapAsset( albumArt, SizeOfArt );
                    }
                    else
                    {
                        art = Global.Icons.Now;
                    }
                }
            }

            Update();
        }

        void OnFrame()
        {
            if ( fadeIn )
            {
                if ( opacity < OpacityMax )
                {
                    opacity = Math.Min( opacity + OpacityChangePerTick, OpacityMax );
                }
                else
                {
                    renderTimer.Stop();
                }
            }
            else
            {
                if ( opacity > OpacityMin )
                {
                    opacity = Math.Max( opacity - OpacityChangePerTick, OpacityMin );
                }
                else
                {
                    fadeIn = true;
                    Flip();
                }
            }

            Refresh();
        }

        protected override void OnMouseMove( Mouse mouse )
        {
            base.OnMouseMove( mouse );

            if ( info != null )
            {
                var mouseOnThreeDots = threeDotsArea.Contains( mouse.Position );
                if ( mouseIsHoveringThreeDots != mouseOnThreeDots )
                {
                    mouseIsHoveringThreeDots = mouseOnThreeDots;
                    Refresh();
                    return;
                }
            }

            if ( artPlaceholder )
            {
                /*var mouseOnPlaceholder = artPlaceholderArea.Contains( mouse.Position );
                if ( mouseIsHoveringPlaceholder != mouseOnPlaceholder )
                {
                    mouseIsHoveringPlaceholder = mouseOnPlaceholder;
                    Refresh();
                    return;
                }*/
            }

            if ( !updateAvailable )
                return;

            var testRange = new TextRange( 0, UpdateAvailable.Length );
            var testPosition = new Point( mouse.Position.X - textArea.Left, mouse.Position.Y - textArea.Top );

            bool isInside;
            bool isTrailing;
            updateTextLayout.HitTestPoint( testPosition.X, testPosition.Y, out isTrailing, out isInside );

            if ( isInside && !mouseIsHoveringOnUpdate )
            {
                updateTextLayout.SetUnderline( true, testRange );
                //updateTextLayout.SetFontWeight( FontWeight.Bold, testRange );

                mouseIsHoveringOnUpdate = true;
                Refresh();
            }
            else if ( !isInside && mouseIsHoveringOnUpdate )
            {
                updateTextLayout.SetUnderline( false, testRange );
                //updateTextLayout.SetFontWeight( FontWeight.Normal, testRange );

                mouseIsHoveringOnUpdate = false;
                Refresh();
            }
        }

        protected override void OnMouseLeave( Mouse mouse )
        {
            base.OnMouseLeave( mouse );
            mouseIsHoveringThreeDots = false;
            mouseIsHoveringOnUpdate = false;
            mouseIsHoveringPlaceholder = false;
            Refresh();
        }

        protected override void OnMouseUp( Mouse mouse )
        {
            base.OnMouseUp( mouse );

            if ( mouseIsDown )
            {
                Refresh();
            }
        }

        protected override void OnMouseDoubleClick( Mouse mouse )
        {
            base.OnMouseDoubleClick( mouse );

            if ( mouse.IsPressed( MouseButton.Left ) )
            {
                mouseIsDown = true;
                Refresh();
            }
        }

        protected override void OnMouseDown( Mouse mouse )
        {
            base.OnMouseDown( mouse );

            if ( mouse.IsPressed( MouseButton.Left ) )
            {
                mouseIsDown = true;

                if ( mouseIsHoveringThreeDots )
                {
                    Menu?.Show( FromControlToWindow( mouse.Position ) );
                }
                else if ( mouseIsHoveringPlaceholder )
                {
                    OnArtClick?.Invoke();
                }
                else if ( mouseIsHoveringOnUpdate )
                {
                    OnUpdateClick?.Invoke();
                }

                Refresh();
            }
        }
    }
}
