﻿using System;
using System.Collections.Generic;

using Kodo.Graphics;
using Kodo.Graphics.Style;

using KodoFM.Audio;

namespace KodoFM
{
    public class AudioAlbumHeader
    {
        public string Name { get; }
        public string Artist { get; }
        public string Year { get; }
        public string Length { get; }

        public AudioAlbumHeader( string name, string artist, string year, string length )
        {
            Name = name;
            Artist = artist;
            Year = year;
            Length = length;
        }
    }

    public class AudioListingView : ListingView<AudioAlbumHeader, AudioItem>
    {
        class Art
        {
            public Bitmap Bitmap { get; }
            public BitmapAsset Asset { get; }

            public Art( Bitmap bitmap, BitmapAsset asset )
            {
                Bitmap = bitmap;
                Asset = asset;
            }
        }

        class Text
        {
            public TextLayout Title { get; }
            public TextLayout Length { get; }

            public Text( TextLayout title, TextLayout length )
            {
                Title = title;
                Length = length;
            }
        }

        public string Name { get; private set; }

        public delegate void UserLoadDelegate( string album, string artist );

        public event UserLoadDelegate OnUserCloudLoad;
        public event UserLoadDelegate OnUserLocalLoad;

        Bitmap artPlaceholderBitmap;
        BitmapAsset artPlaceholderAsset;

        const string LoadTrackInfo = "Load album art from last.fm";
        const string ChooseAlbumArt = "Load album art from filesystem";

        Dictionary<string, Art> headerArtCache = new Dictionary<string, Art>();
        Dictionary<string, TextLayout> headerTextCache = new Dictionary<string, TextLayout>();
        Dictionary<AudioItem, Text> itemTextCache = new Dictionary<AudioItem, Text>();

        public AudioListingView( Window window )
            : base( window )
        {
            var headerMenu = new ContextMenu( window );
            headerMenu.TextStyle = Global.TextStyles.Large;
            headerMenu.Add( LoadTrackInfo );
            headerMenu.Add( ChooseAlbumArt );
            headerMenu.OnContextMenu += OnInfoContext;

            Menu = headerMenu;
        }

        protected override void OnLoad( Context context )
        {
            base.OnLoad( context );

            artPlaceholderAsset = Global.Icons.Now;
            artPlaceholderBitmap = new Bitmap( context, artPlaceholderAsset.Source );
        }

        void OnInfoContext( ContextMenuItem item )
        {
            var header = GetHoveredHeader();

            if ( header == null )
                return;

            switch ( item.Text )
            {
                case LoadTrackInfo:
                    OnUserCloudLoad?.Invoke( header.Name, header.Artist );
                    break;
                case ChooseAlbumArt:
                    OnUserLocalLoad?.Invoke( header.Name, header.Artist );
                    break;
            }
        }

        protected override void OnClear()
        {
            /*foreach ( var art in headerArtCache.Values )
            {
                art.Asset.Dispose();
                art.Bitmap.Dispose();
            }

            foreach ( var text in headerTextCache.Values )
            {
                text.Dispose();
            }

            foreach ( var text in itemTextCache.Values )
            {
                text.Title.Dispose();
                text.Length.Dispose();
            }*/

            headerTextCache.Clear();
            headerArtCache.Clear();
            itemTextCache.Clear();
        }

        public void SetAlbumListing( AudioAlbumInfo data )
        {
            Clear();

            Name = $"{AudioDatabase.AlbumPrefix}{data.Name}";

            AddHeader( new AudioAlbumHeader( data.Name, data.Artist, "", "" ) );

            var albumArtCached = false;

            foreach ( var track in data )
            {
                if ( !albumArtCached )
                {
                    AudioCache.CacheAlbumArt( track.Album, track.Artist, track.Path );
                    albumArtCached = true;
                }

                AddItem( track );
            }

            Load();
        }

        public void SetArtistListing( AudioArtistInfo data )
        {
            Clear();

            Name = $"{AudioDatabase.ArtistPrefix}{data.Name}";

            foreach ( var album in data.Albums )
            {
                var albumArtCached = false;

                AddHeader( new AudioAlbumHeader( album.Key, data.Name, "", "" ) );

                foreach ( var track in album.Value )
                {
                    if ( !albumArtCached )
                    {
                        AudioCache.CacheAlbumArt( track.Album, track.Artist, track.Path );
                        albumArtCached = true;
                    }

                    AddItem( track );
                }
            }

            Load();
        }

        protected override void OnLoadHeader( AudioAlbumHeader item, Context context )
        {
            try
            {
                using ( var albumArt = AudioCache.GetCachedAlbumArt( item.Name, item.Artist ) )
                {
                    if ( albumArt != null )
                    {
                        var artAsset = new BitmapAsset( albumArt, new Size( HeightOfHeader, HeightOfHeader ) );
                        var artBitmap = new Bitmap( context, artAsset.Source );

                        headerArtCache.Remove( item.Name );
                        headerArtCache.Add( item.Name, new Art( artBitmap, artAsset ) );
                    }
                }
            }
            // ReSharper disable once EmptyGeneralCatchClause
            catch { }
        }

        protected override void OnDrawHeader( AudioAlbumHeader item, Context context, Rectangle area )
        {
            var artArea = new Rectangle( area.Left, area.Top, area.Left + area.Height, area.Bottom );
            var textArea = new Rectangle( artArea.Right + 10, artArea.Top, area.Right, artArea.Bottom );

            context.AntialiasMode = AntialiasMode.PerPrimitive;

            Art art;

            if ( headerArtCache.TryGetValue( item.Name, out art ) )
            {
                context.DrawBitmap( art.Bitmap, artArea, 1, InterpolationMode.Linear );
            }
            else
            {
                var artPlaceholderArea = new Rectangle( artPlaceholderAsset.Size ).CenterTo( artArea );
                SharedBrush.Color = Color.GhostWhite;

                context.AntialiasMode = AntialiasMode.Aliased;
                context.FillOpacityMask( artPlaceholderBitmap, SharedBrush, artPlaceholderArea );
                context.AntialiasMode = AntialiasMode.PerPrimitive;
            }

            TextLayout layout;

            if ( !headerTextCache.TryGetValue( item.Name, out layout ) )
            {
                TextStyle.Alignment = TextStyleAlignment.LeftCenter;
                var layoutStr = $"{item.Name}\n{item.Artist}";
                layout = new TextLayout( layoutStr, TextStyle, textArea.Dimensions );
                layout.SetFontSize( TextStyle.Format.FontSize + 6, layoutStr.RangeOf( item.Name ) );
            }
            else
            {
                layout.SetMaximumSize( textArea.Dimensions );
            }

            SharedBrush.Color = Color.GhostWhite;
            context.DrawTextLayout( layout, textArea.Location, SharedBrush );
        }

        protected override void OnDrawItem( AudioItem item, Context context, Rectangle area, bool active )
        {
            Text textLayouts;

            var titleArea = new Rectangle( area.Left + 2, area.Top, area.Right - 50, area.Bottom );
            var lengthArea = new Rectangle( titleArea.Right, area.Top, area.Right - 2, area.Bottom );

            if ( !itemTextCache.TryGetValue( item, out textLayouts ) )
            {
                TextStyle.Alignment = TextStyleAlignment.LeftCenter;
                var titleLayout = new TextLayout( item.Title, TextStyle, titleArea.Dimensions );
                TextStyle.Alignment = TextStyleAlignment.RightCenter;
                var lengthLayout = new TextLayout( item.Length, TextStyle, lengthArea.Dimensions );

                textLayouts = new Text( titleLayout, lengthLayout );
                itemTextCache.Add( item, textLayouts );
            }
            else
            {
                textLayouts.Title.SetMaximumSize( titleArea.Dimensions );
                textLayouts.Length.SetMaximumSize( lengthArea.Dimensions );
            }

            SharedBrush.Color = active ? AccentColor : Color.GhostWhite;

            context.DrawTextLayout( textLayouts.Title, titleArea.Location, SharedBrush );
            context.DrawTextLayout( textLayouts.Length, lengthArea.Location, SharedBrush );
        }
    }

    [Flags]
    public enum ListingViewMode
    {
        None = 0,
        AllowUserSelect = 1,
        AllowUserRearrange = 2,
        AllowUserActivation = 4,
    }

    public abstract class ListingView<THeader, TItem> : Control
    {
        class ListingViewItem
        {
            public bool IsHeader { get; }

            protected ListingViewItem( bool isHeader )
            {
                IsHeader = isHeader;
            }
        }

        class ListingViewItemHeader<T> : ListingViewItem
        {
            public bool IsFirst { get; }
            public T Value { get; }

            public ListingViewItemHeader( T value, bool isFirst = false )
                : base( true )
            {
                Value = value;
                IsFirst = isFirst;
            }
        }

        class ListingViewItemNormal<T> : ListingViewItem
        {
            public T Value { get; }

            public ListingViewItemNormal( T value )
                : base( false )
            {
                Value = value;
            }
        }

        public delegate void UserActivatedItemDelegate( TItem item );

        public event UserActivatedItemDelegate OnUserActivatedItem;

        Dictionary<TItem, int> lookup = new Dictionary<TItem, int>();
        SortedSet<int> selectedItems = new SortedSet<int>();
        List<ListingViewItem> items = new List<ListingViewItem>();

        enum ScrollMode
        {
            CenterTo,
            CenterToLazy,
            BringToView,
            Scrollbar,
            Forward,
            Backward
        }

        Trackbar trackbar;

        Bitmap threeDotsBitmap;

        const int InvalidIndex = -1;

        protected const float HeightOfItem = 36;
        protected const float HeightOfHeader = 36 * 3;

        int viewRange;
        int viewBegin;
        int viewEnd;

        bool allowActivation = true;

        ListingViewItemHeader<THeader> mouseIsHovering;
        Point mousePosition;
        bool mouseIsHoveringHeader;
        bool mouseIsHoveringThreeDots;

        int activeItem = InvalidIndex;
        int selectedTether = InvalidIndex;

        int mouseHover = InvalidIndex;
        //int keyboardHover = InvalidIndex;

        protected THeader GetHoveredHeader()
        {
            return mouseIsHovering.Value;
        }

        ListViewSelectMode selectMode = ListViewSelectMode.Multi;

        /// <summary>
        /// Get or set the <see cref="ListViewSelectMode"/> of the list.
        /// </summary>
        public ListViewSelectMode SelectionMode
        {
            get { return selectMode; }
            set { selectMode = value; }
        }

        /// <summary> 
        /// Get or set the <see cref="Trackbar"/> for scrolling the list. 
        /// </summary>
        public Trackbar Trackbar
        {
            get { return trackbar; }
            set
            {
                if ( trackbar != value )
                {
                    // Disconnect from the old scrollbar.
                    if ( trackbar != null )
                        trackbar.Scroll -= OnTrack;

                    trackbar = value;

                    // Connect to the new scrollbar.
                    if ( trackbar != null )
                    {
                        trackbar.Scroll += OnTrack;
                        trackbar.Set( viewBegin, 0, items.Count - viewRange );
                    }
                }
            }
        }

        protected void AddHeader( THeader value )
        {
            items.Add( new ListingViewItemHeader<THeader>( value, true ) );
            items.Add( new ListingViewItemHeader<THeader>( value ) );
            items.Add( new ListingViewItemHeader<THeader>( value ) );
        }

        protected void AddItem( TItem value )
        {
            lookup.Add( value, /*Must be the count of items because headers are 3 items!*/ items.Count );
            items.Add( new ListingViewItemNormal<TItem>( value ) );
        }

        protected virtual void OnClear() { }

        public void Clear()
        {
            activeItem = InvalidIndex;
            SelectClearInternal();
            OnClear();
            items.Clear();
            lookup.Clear();
        }

        protected ListingView( Window window )
            : base( window )
        {
        }

        static int Clamp( int value, int min, int max )
        {
            return Math.Max( Math.Min( value, max ), min );
        }

        protected override void OnLoad( Context context )
        {
            base.OnLoad( context );

            threeDotsBitmap = new Bitmap( context, Global.Icons.ThreeDots.Source );

            foreach ( var item in items )
            {
                if ( item.IsHeader )
                {
                    var header = (ListingViewItemHeader<THeader>)item;

                    OnLoadHeader( header.Value, context );
                }
            }
        }

        protected override void OnUpdate( Context context )
        {
            base.OnUpdate( context );

            var size = Area.Dimensions;

            viewRange = Clamp( (int)Math.Floor( size.Height / HeightOfItem ), 0, items.Count );
            viewBegin = Clamp( viewBegin, 0, items.Count - viewRange );
            viewEnd = viewBegin + viewRange;

            TrackMode( viewRange != items.Count );
        }

        protected override void OnDraw( Context context )
        {
            var clip = new Rectangle( Area.Dimensions );

            context.AntialiasMode = AntialiasMode.PerPrimitive;
            SharedBrush.Opacity = 1;

            SharedStyle.Align( clip );
            context.FillRectangle( clip, SharedStyle.Background );

            mouseIsHovering = null;
            mouseIsHoveringThreeDots = false;

            var itemArea = new Rectangle( 0, 0, clip.Right, HeightOfItem );

            for ( var i = viewBegin; i < viewEnd; i++ )
            {
                var item = items[ i ];

                if ( item.IsHeader )
                {
                    var header = (ListingViewItemHeader<THeader>)item;

                    if ( header.IsFirst && (clip.Bottom - (itemArea.Top + HeightOfHeader) >= 0) )
                    {
                        var headerArea = new Rectangle( itemArea.Left, itemArea.Top, itemArea.Right, itemArea.Top + HeightOfHeader );
                        OnDrawHeader( header.Value, context, headerArea );

                        SharedStyle.Align( headerArea );
                        context.FillRectangle( headerArea, SharedStyle.Foreground );

                        SharedBrush.Color = Color.Black;
                        context.DrawRectangle( headerArea, SharedBrush, 2 );

                        if ( mouseIsHoveringHeader && mouseIsHoveringThreeDots == false && headerArea.Contains( mousePosition ) )
                        {
                            mouseIsHovering = header;

                            var threeDotsArea = Rectangle.FromXYWH( headerArea.Right - 36, headerArea.Top, 32, 32 );
                            mouseIsHoveringThreeDots = false;

                            if ( mouseIsHoveringHeader )
                            {
                                mouseIsHoveringThreeDots = threeDotsArea.Contains( mousePosition );
                            }

                            SharedBrush.Color = mouseIsHoveringThreeDots
                                ? AccentColor
                                : Color.FromAColor( 0.5f, Color.GhostWhite );

                            context.AntialiasMode = AntialiasMode.Aliased;
                            context.FillOpacityMask( threeDotsBitmap, SharedBrush, threeDotsArea );
                            context.AntialiasMode = AntialiasMode.PerPrimitive;
                        }
                    }
                    else
                    {
                        itemArea = itemArea.Move( 0, HeightOfItem );
                        continue;
                    }
                }
                else
                {
                    OnDrawItem( ((ListingViewItemNormal<TItem>)item).Value, context,
                        new Rectangle( itemArea.Left, itemArea.Top, itemArea.Right, itemArea.Top + HeightOfItem ), i == activeItem );

                    SharedStyle.Align( itemArea );
                    context.FillRectangle( itemArea, SharedStyle.Foreground );

                    SharedBrush.Color = Color.Black;
                    context.DrawRectangle( itemArea, SharedBrush, 2 );
                }

                if ( i == mouseHover )
                {
                    SharedBrush.Color = new Color( 0.2f, AccentColor );
                    context.FillRectangle( itemArea, SharedBrush );
                }
                else if ( selectedItems.Contains( i ) )
                {
                    SharedBrush.Color = new Color( 0.5f, AccentColor );
                    context.FillRectangle( itemArea, SharedBrush );
                }

                itemArea = itemArea.Move( 0, HeightOfItem );
            }
        }

        protected abstract void OnLoadHeader( THeader item, Context context );
        protected abstract void OnDrawHeader( THeader item, Context context, Rectangle area );
        protected abstract void OnDrawItem( TItem item, Context context, Rectangle area, bool active );

        /// <summary>
        /// Called when the list receives a key down event.
        /// </summary>
        /// <param name="key">The key.</param>
        protected override void OnKeyDown( Key key )
        {
            switch ( key )
            {
                case Key.Space:
                    Scroll( ScrollMode.CenterTo, activeItem );
                    Refresh();
                    break;

                case Key.A:
                    if ( Keyboard.IsDown( Key.Ctrl ) )
                        SelectAll();
                    break;

                case Key.Escape:
                    SelectClear();
                    break;

                case Key.Enter:
                    ActivateCurrentItem();
                    break;

                case Key.ArrowUp:
                    KeyboardCursorMove( -1 );
                    break;

                case Key.ArrowDown:
                    KeyboardCursorMove( 1 );
                    break;
            }
        }

        public void Reload()
        {
            Load();
        }

        public void ClearActiveItem()
        {
            activeItem = InvalidIndex;
            Refresh();
        }

        public void ActivateItem( TItem item )
        {
            if ( allowActivation )
            {
                int itemIndex;

                if ( lookup.TryGetValue( item, out itemIndex ) )
                {
                    activeItem = itemIndex;
                    Scroll( ScrollMode.CenterToLazy, activeItem );
                    Refresh();
                }
            }
        }

        void ActivateCurrentItem()
        {
            if ( !allowActivation )
                return;

            if ( selectedTether == InvalidIndex )
                return;

            activeItem = selectedTether;
            SelectClearInternal();
            OnUserActivatedItem?.Invoke( ((ListingViewItemNormal<TItem>)items[ activeItem ]).Value );
            Refresh();
        }

        void KeyboardCursorMove( int direction )
        {
            if ( selectedTether == InvalidIndex )
            {
                //var index = GetActiveIndex();
                var index = InvalidIndex;
                Select( index == InvalidIndex ? 0 : index );
                mouseHover = selectedTether;
            }
            else
            {
                if ( Keyboard.IsDown( Key.ShiftLeft ) )
                {
                    if ( IsItemWithinBounds( mouseHover + direction ) )
                    {
                        Select( mouseHover + direction, Keyboard.IsDown( Key.CtrlLeft ), true );
                        mouseHover += direction;
                    }
                }
                else
                {
                    if ( IsItemWithinBounds( selectedTether + direction ) )
                    {
                        Select( selectedTether + direction );
                        mouseHover = selectedTether;
                    }
                }
            }

            Scroll( ScrollMode.BringToView, mouseHover );
            Refresh();
        }

        protected override void OnMouseMove( Mouse mouse )
        {
            CaptureKeyboard();

            base.OnMouseMove( mouse );

            mouseHover = CoordinateToIndex( mouse.Position );

            Refresh();
        }

        protected override void OnMouseDown( Mouse mouse )
        {
            if ( mouse.IsPressed( MouseButton.Left ) ||
                (mouse.IsPressed( MouseButton.Right ) && mouseHover != InvalidIndex && !selectedItems.Contains( mouseHover )) )
            {
                Select( mouseHover, Keyboard.IsDown( Key.Ctrl ), Keyboard.IsDown( Key.Shift ) );
                Refresh();
            }

            if ( mouseIsHoveringThreeDots )
            {
                Menu?.Show( FromControlToWindow( mouse.Position ) );
            }
        }

        protected override void OnMouseWheel( Mouse mouse )
        {
            Scroll( mouse.WheelDelta < 0 ? ScrollMode.Forward : ScrollMode.Backward );

            mouseHover = CoordinateToIndex( mouse.Position );
            Refresh();
        }

        protected override void OnMouseLeave( Mouse mouse )
        {
            mouseHover = InvalidIndex;
            Refresh();
        }

        protected override void OnMouseDoubleClick( Mouse mouse )
        {
            if ( !mouse.IsPressed( MouseButton.Left ) )
                return;

            ActivateCurrentItem();
        }

        int CoordinateToIndex( Point coordinate )
        {
            var height = 0f;

            var result = InvalidIndex;

            mouseIsHoveringHeader = false;

            for ( var i = viewBegin; i < viewEnd; i++ )
            {
                var item = items[ i ];

                height += HeightOfItem;

                if ( height > coordinate.Y )
                {
                    if ( item.IsHeader == false )
                    {
                        result = i;
                    }
                    else
                    {
                        mousePosition = coordinate;
                        mouseIsHoveringHeader = true;
                    }
                    break;
                }
            }

            return result;
        }

        /// <summary>
        /// Clear all selections.
        /// </summary>
        public void SelectClear()
        {
            SelectClearInternal();
            Refresh();
        }

        void SelectClearInternal()
        {
            selectedTether = InvalidIndex;
            selectedItems.Clear();
        }

        void SelectRangeInternal( int start, int end )
        {
            if ( selectMode != ListViewSelectMode.Multi )
                throw new InvalidOperationException( nameof( selectMode ) );

            for ( var i = start; i <= end; ++i )
            {
                if ( items[ i ].IsHeader == false )
                {
                    selectedItems.Add( i );
                }
            }
        }

        /// <summary>
        /// Select all items in the list.
        /// </summary>
        public void SelectAll()
        {
            if ( selectMode != ListViewSelectMode.Multi )
                throw new InvalidOperationException( nameof( selectMode ) );

            SelectAllInternal();
            Refresh();
        }

        void SelectAllInternal()
        {
            for ( var i = 0; i < items.Count; i++ )
            {
                if ( items[ i ].IsHeader == false )
                {
                    selectedItems.Add( i );
                }
            }
        }

        void Select( int target, bool ctrlMode = false, bool shiftMode = false )
        {
            if ( IsItemWithinBounds( target ) == false ||
                 selectMode == ListViewSelectMode.None )
            {
                SelectClearInternal();
            }
            else if ( selectMode == ListViewSelectMode.Single ||
                      selectMode == ListViewSelectMode.SingleActivation ||
                     (ctrlMode == false && shiftMode == false) )
            {
                SelectClearInternal();

                if ( selectMode == ListViewSelectMode.SingleActivation )
                {
                    return;
                }

                selectedTether = target;
                selectedItems.Add( mouseHover );
            }
            else if ( shiftMode )
            {
                int start;
                int end;

                if ( selectedTether == InvalidIndex )
                    selectedTether = 0;

                if ( !ctrlMode )
                {
                    start = selectedTether;
                    SelectClearInternal();
                    selectedTether = start;
                }

                // Set the selection range.
                if ( target > selectedTether )
                {
                    start = selectedTether;
                    end = target;
                }
                else
                {
                    start = target;
                    end = selectedTether;
                }

                SelectRangeInternal( start, end );
            }
            else if ( ctrlMode )
            {
                if ( selectedItems.Contains( target ) )
                {
                    selectedItems.Remove( target );
                }
                else
                {
                    selectedTether = target;
                    selectedItems.Add( target );
                }
            }
        }

        bool IsItemWithinBounds( int item )
        {
            return item >= 0 && item < items.Count;
        }

        bool IsItemWithinView( int item )
        {
            return item >= viewBegin && item <= viewEnd;
        }

        void OnTrack( int value )
        {
            Scroll( ScrollMode.Scrollbar, value );
            Refresh();
        }

        void TrackMode( bool value )
        {
            if ( trackbar == null )
                return;

            if ( value )
            {
                trackbar.ThumbVisible = true;
                trackbar.Set( viewBegin, 0, items.Count - viewRange );
            }
            else
            {
                trackbar.ThumbVisible = false;
                trackbar.Set( 0, 0, 0 );
            }
        }

        void Scroll( ScrollMode mode, int parameter = 1 )
        {
            if ( parameter == InvalidIndex )
                return;

            switch ( mode )
            {
                case ScrollMode.CenterTo:

                    viewBegin = Clamp( parameter - viewRange / 2, 0, items.Count - viewRange );
                    break;

                case ScrollMode.CenterToLazy:

                    if ( parameter <= viewBegin + 2 || parameter >= viewEnd - 2 )
                    {
                        viewBegin = Clamp( parameter - viewRange / 2, 0, items.Count - viewRange );
                    }
                    break;

                case ScrollMode.Scrollbar:

                    if ( viewBegin != parameter )
                    {
                        viewBegin = Clamp( parameter, 0, items.Count - viewRange );
                    }
                    break;

                case ScrollMode.BringToView:

                    if ( parameter < viewBegin )
                        viewBegin = parameter;
                    else if ( parameter >= viewEnd )
                        viewBegin = Clamp( viewBegin + 1, 0, items.Count - viewRange );
                    break;

                case ScrollMode.Backward:

                    if ( viewBegin > 0 )
                        viewBegin -= parameter;
                    break;

                case ScrollMode.Forward:

                    if ( viewBegin < items.Count - viewRange )
                        viewBegin += parameter;
                    break;

                default:
                    throw new ArgumentOutOfRangeException( nameof( mode ), mode, null );
            }

            viewEnd = viewBegin + viewRange;

            trackbar?.Set( viewBegin, 0, items.Count - viewRange );
        }
    }

    public class PlaylistsView : ListView<AudioPlaylist>
    {
        List<AudioPlaylist> playlists = new List<AudioPlaylist>();

        public PlaylistsView( Window window )
            : base( window )
        {
        }

        public void SetPlaylists( List<AudioPlaylist> playlistList )
        {
            playlists = playlistList;
            Reload();
        }

        protected override float ActualItemHeight
        {
            get { return 36; }
        }

        public override ListViewSelectMode SelectionMode
        {
            get { return ListViewSelectMode.Single; }
        }

        protected override AudioPlaylist Get( int index )
        {
            return playlists[ index ];
        }

        protected override int GetCount()
        {
            return playlists.Count;
        }

        protected override bool Match( int index, string search )
        {
            return playlists[ index ].Name.IndexOf( search, StringComparison.OrdinalIgnoreCase ) != -1;
        }

        protected override void OnDrawItem( Context context, AudioPlaylist item, Rectangle area, bool selected, bool active )
        {
            var itemHeightHalf = ActualItemHeight / 2;
            var itemWidth = area.Width - 4;
            var nameRect = Rectangle.FromXYWH( area.Left + 2, area.Top, itemWidth, itemHeightHalf );
            var trackRect = Rectangle.FromXYWH( area.Left + 2, area.Top + itemHeightHalf, 200, itemHeightHalf );

            SharedBrush.Color = Color.GhostWhite;
            TextStyle.Alignment = TextStyleAlignment.LeftTop;
            context.DrawText( item.Name, TextStyle, nameRect, SharedBrush );

            SharedBrush.Color = Color.SlateGray;
            TextStyle.Alignment = TextStyleAlignment.LeftBottom;
            context.DrawText( $"Tracks: {item.Count}", TextStyle, trackRect, SharedBrush );
        }
    }

    public class ArtistsView : ListView<AudioListingArtist>
    {
        List<AudioListingArtist> artists = new List<AudioListingArtist>();

        public ArtistsView( Window window )
            : base( window )
        {
        }

        public void SetArtists( List<AudioListingArtist> artistList )
        {
            artists = artistList;
            Reload();
        }

        protected override float ActualItemHeight
        {
            get { return 36; }
        }

        public override ListViewSelectMode SelectionMode
        {
            get { return ListViewSelectMode.Single; }
        }

        protected override AudioListingArtist Get( int index )
        {
            return artists[ index ];
        }

        protected override int GetCount()
        {
            return artists.Count;
        }

        protected override bool Match( int index, string search )
        {
            return artists[ index ].Name.IndexOf( search, StringComparison.OrdinalIgnoreCase ) != -1;
        }

        protected override void OnDrawItem( Context context, AudioListingArtist item, Rectangle area, bool selected, bool active )
        {
            var itemHeightHalf = ActualItemHeight / 2;
            var itemWidth = area.Width - 4;
            var nameRect = Rectangle.FromXYWH( area.Left + 2, area.Top, itemWidth, itemHeightHalf );
            var trackRect = Rectangle.FromXYWH( area.Left + 2, area.Top + itemHeightHalf, 200, itemHeightHalf );
            var albumRect = Rectangle.FromXYWH( trackRect.Right, area.Top + itemHeightHalf, itemWidth - 200, itemHeightHalf );

            SharedBrush.Color = Color.GhostWhite;
            TextStyle.Alignment = TextStyleAlignment.LeftTop;
            context.DrawText( item.Name, TextStyle, nameRect, SharedBrush );

            SharedBrush.Color = Color.SlateGray;
            TextStyle.Alignment = TextStyleAlignment.LeftBottom;
            context.DrawText( $"Tracks: {item.Tracks}", TextStyle, trackRect, SharedBrush );
            context.DrawText( $"Albums: {item.Albums}", TextStyle, albumRect, SharedBrush );
        }
    }

    public class AlbumsView : ListView<AudioListingAlbum>
    {
        List<AudioListingAlbum> albums = new List<AudioListingAlbum>();

        public AlbumsView( Window window )
            : base( window )
        {
        }

        public void SetAlbums( List<AudioListingAlbum> albumList )
        {
            albums = albumList;
            Reload();
        }

        protected override float ActualItemHeight
        {
            get { return 36; }
        }

        public override ListViewSelectMode SelectionMode
        {
            get { return ListViewSelectMode.Single; }
        }

        protected override AudioListingAlbum Get( int index )
        {
            return albums[ index ];
        }

        protected override int GetCount()
        {
            return albums.Count;
        }

        protected override bool Match( int index, string search )
        {
            return albums[ index ].Name.IndexOf( search, StringComparison.OrdinalIgnoreCase ) != -1;
        }

        protected override void OnDrawItem( Context context, AudioListingAlbum item, Rectangle area, bool selected, bool active )
        {
            var itemHeightHalf = ActualItemHeight / 2;
            var itemWidth = area.Width - 4;
            var nameRect = Rectangle.FromXYWH( area.Left + 2, area.Top, itemWidth, itemHeightHalf );
            var trackRect = Rectangle.FromXYWH( area.Left + 2, area.Top + itemHeightHalf, 200, itemHeightHalf );
            var artistRect = Rectangle.FromXYWH( trackRect.Right, area.Top + itemHeightHalf, itemWidth - 200, itemHeightHalf );

            SharedBrush.Color = Color.GhostWhite;
            TextStyle.Alignment = TextStyleAlignment.LeftTop;
            context.DrawText( item.Name, TextStyle, nameRect, SharedBrush );

            SharedBrush.Color = Color.SlateGray;
            TextStyle.Alignment = TextStyleAlignment.LeftBottom;
            context.DrawText( $"Tracks: {item.Tracks}", TextStyle, trackRect, SharedBrush );
            context.DrawText( item.Artist, TextStyle, artistRect, SharedBrush );
        }
    }
}
