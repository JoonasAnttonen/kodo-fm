﻿using System;

using KodoFM.Audio;
using Kodo.Graphics;
using Kodo.Graphics.Style;

namespace KodoFM
{
    public class AudioView : Control
    {
        const int SpectrumSize = 512;
        const int SpectrumStart = 2;
        const int SpectrumEnd = 128;
        const int SpectrumLength = SpectrumEnd - SpectrumStart;
        const float BarMinimum = 2;
        const float BarSpacer = 1;
        const float Prescale = 12;
        const float PeakDrop = 0.5f;

        readonly float[] audioData = new float[ SpectrumSize ];
        float[] barPeaks = new float[ 0 ];
        float barWidth = 1;
        int barCount;
        int dataPerBar;

        Timer renderTimer;
        Bitmap spectrumBitmap;
        BitmapBrush spectrumBrush;

        Rectangle quadrant;

        public AudioPlayer Player
        {
            get;
            set;
        }

        public AudioView( Window window )
            : base( window )
        {
            renderTimer = new Timer( OnFrame );
            renderTimer.Interval = 16;
            renderTimer.Start();
        }

        void OnFrame()
        {
            Refresh();
        }

        protected override void OnUpdate( Context context )
        {
            quadrant = new Rectangle( 0, 0, (float)Math.Ceiling( Area.Width / 2 ), Area.Height / 2 );

            spectrumBitmap = new Bitmap( context, new Size( quadrant.Width, quadrant.Height ), BitmapProperties.Target( context ) );
            spectrumBrush = new BitmapBrush( context, spectrumBitmap );
            spectrumBrush.ExtendModeX = ExtendMode.Mirror;
            spectrumBrush.ExtendModeY = ExtendMode.Mirror;

            barCount = Math.Min( (int)Math.Floor( quadrant.Width / (barWidth + BarSpacer) ), SpectrumLength );
            dataPerBar = (int)Math.Floor( (float)SpectrumLength / barCount );

            if ( barCount == SpectrumLength )
            {
                barWidth = (quadrant.Width - (barCount * BarSpacer)) / barCount;
            }
            else
            {
                barWidth = 2;
            }

            if ( barCount != barPeaks.Length )
            {
                barPeaks = new float[ barCount ];

                for ( var i = 0; i < barPeaks.Length; i++ )
                    barPeaks[ i ] = quadrant.Height;
            }
        }

        protected override void OnDraw( Context context )
        {
            var oldTarget = context.GetTarget();
            context.SetTarget( spectrumBitmap );
            context.SetTransform( Matrix3x2.Identity );

            context.FillRectangle( quadrant, SharedStyle.Background );

            context.AntialiasMode = AntialiasMode.PerPrimitive;

            SharedBrush.Opacity = 1;
            SharedBrush.Color = AccentColor;

            if ( Player != null && Player.Stream.Playing )
            {
                Player.ReadSpectrum( audioData );
            }
            else
            {
                for ( var i = 0; i < SpectrumSize; i++ )
                    audioData[ i ] = 0;
            }

            var barAreaHeight = quadrant.Height;
            var barArea = new Rectangle( quadrant.Right + (BarSpacer / 2), quadrant.Top, quadrant.Right - barWidth, barAreaHeight );
            var lastBarHeight = BarMinimum;

            for ( var i = 0; i < barCount; i++ )
            {
                float barHeight = 0;

                for ( var j = 0; j < dataPerBar; j++ )
                    barHeight += audioData[ SpectrumStart + (i * dataPerBar) + j ];

                barHeight /= dataPerBar;
                barHeight *= Prescale;
                barHeight = Math.Min( BarMinimum + (barHeight * barAreaHeight), barAreaHeight );
                barHeight = lastBarHeight = (barHeight + lastBarHeight) / 2;
                barHeight = (float)Math.Ceiling( barHeight );

                barArea = Rectangle.FromXYWH( barArea.Left, (barAreaHeight - barHeight), barWidth, barHeight );

                var peakX = barArea.Left;
                var peakX2 = barArea.Right;
                var peakY = barPeaks[ i ] = Math.Min( barPeaks[ i ] + PeakDrop, barArea.Top );

                context.DrawLine( new Point( peakX, peakY ), new Point( peakX2, peakY ), SharedBrush, 2 );
                context.FillRectangle( barArea, SharedBrush );

                barArea = barArea.Move( -(barWidth + BarSpacer), 0 );
            }

            var clip = new Rectangle( Area.Dimensions );

            context.SetTarget( oldTarget );
            context.SetTransform( Transform );
            context.FillRectangle( clip, spectrumBrush );

            SharedStyle.Align( quadrant );
            context.FillRectangle( clip, SharedStyle.Foreground );

            SharedBrush.Color = Color.Black;
            context.DrawRectangle( clip, SharedBrush, 2 );
        }
    }
}
