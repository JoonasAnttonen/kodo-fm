﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Kodo.Graphics;
using Kodo.Graphics.Style;

using System.Net;

using KodoFM.Assets;
using KodoFM.Audio;

namespace KodoFM
{
    public sealed class KodoFMMainWindow : Window
    {
        public enum UIState
        {
            Now,
            All,
            Queue,
            Artists,
            ArtistListing,
            Albums,
            AlbumListing,
            Playlists,
            PlaylistsActivated,
        }

        public struct UIStateMemory
        {
            public UIState State { get; private set; } // Setter for serialization purposes!
            public string Data { get; private set; } // Setter for serialization purposes!

            public UIStateMemory( UIState state, string data = null )
            {
                State = state;
                Data = data;
            }
        }

        UIState stateOfUI = UIState.Now;
        Stack<UIStateMemory> stateStack = new Stack<UIStateMemory>();

        public UIStateMemory StateOfUI
        {
            get { return stateStack.Peek(); }
            set
            {
                stateOfUI = value.State;
                stateStack.Push( value );
            }
        }

        ContextMenu contextMenu;

        Timer updateTimer;

        AudioListingView listingView;

        Button buttonPlay;
        Button buttonFrw;
        Button buttonBrw;
        Button buttonShuffle;
        Button buttonRepeat;
        Button buttonMute;
        Button buttonNavi;
        Button buttonSettings;
        Button buttonBack;
        Button buttonNow;
        Button buttonTracks;
        Button buttonArtists;
        Button buttonAlbums;
        Button buttonPlaylists;
        Button buttonTime;

        Trackbar barSeek;
        Trackbar barVolume;
        Trackbar barPlaylist;

        AudioView audioView;
        InfoView infoView;
        PlaylistView playlistView;

        ArtistsView artistsView;
        AlbumsView albumsView;
        PlaylistsView playlistsView;

        AudioDatabase audioDatabase;
        AudioPlayer audioPlayer;
        LastfmInterface lastfm;

        GlobalHotkeys hotkeys;
        KodoFMLogWindow logWindow;
        KodoFMEditWindow editWindow;
        KodoFMSettingsWindow settingsWindow;
        PromtWindow promtWindow;

        AudioSorter pathSorter = new AudioSorter( AudioSorting.Path, AudioSortingOrder.Ascending );
        AudioSorter artistSorter = new AudioSorter( AudioSorting.Artist, AudioSortingOrder.Ascending );
        AudioSorter albumSorter = new AudioSorter( AudioSorting.Album, AudioSortingOrder.Ascending );
        //AudioSorter masterPlaylistSorting;

        static readonly string AlbumArtTypeFilter = new StringBuilder()
                        .Append( "All Image Files\0*.bmp;*.dib;*.wdp;*.mdp;*.hdp;*.gif;*.png;*.jpg;*.jpeg;*.tif;*.ico\0" )
                        .Append( "Windows Bitmap\0*.bmp;*.dib\0" )
                        .Append( "High Definition Photo\0*.wdp;*.mdp;*.hdp\0" )
                        .Append( "Graphics Interchange Format\0*.gif\0" )
                        .Append( "Portable Network Graphics\0*.png\0" )
                        .Append( "JPEG File Interchange Format\0*.jpg;*.jpeg\0" )
                        .Append( "Tiff File\0*.tif\0" )
                        .Append( "Icon\0*.ico\0" )
                        .Append( "All Files\0*.*\0" )
                        .Append( "\0" ).ToString();

        static readonly Size ButtonSize = new Size( 42, 40 );

        StringBuilder streamTimeBuilder = new StringBuilder( 13 );
        string streamLastTimeString = "00:00 | 00:00";
        uint streamLastTime;
        uint streamLastLength;

        bool updateAvailable;
        bool streamSetAsNowPlaying;

        const string LoadAlbumArtFromLastfm = "Load album art from last.fm";
        const string LoadAlbumArtFromLocal = "Load album art from filesystem";
        const string AddToPlaylist = "Add to playlist ...";
        const string CreatePlaylist = "Create ...";

        string MakeTimeString( AudioStream value )
        {
            if ( value.Position == streamLastTime && value.Length == streamLastLength )
            {
                streamLastTime = value.Position;
                streamLastLength = value.Length;
                return streamLastTimeString;
            }

            streamLastTime = value.Position;
            streamLastLength = value.Length;
            return streamLastTimeString = streamTimeBuilder.Clear().Append( AudioInfo.MillisecondsToString( value.Position ) ).Append( " | " ).Append( AudioInfo.MillisecondsToString( value.Info.Length ) ).ToString();
        }

        void OnUpdateClick()
        {
            if ( !updateAvailable )
                return;

            promtWindow.Area = new Rectangle( promtWindow.Area.Dimensions ).CenterTo( Area );
            Locked = true;
            promtWindow.ShowQuestion( OnUpdateClickConfirm, "Updating will restart kodo-fm, continue?" );
        }

        void OnUpdateClickConfirm( PromtWindowResult result )
        {
            Locked = false;
            if ( result == PromtWindowResult.Ok )
            {
                updateAvailable = false;
                infoView.SetUpdateAvailable( updateAvailable );

                if ( Global.RunUpdate() )
                {
                    Close();
                }
            }
        }

        async void OnUpdate()
        {
            updateAvailable = await Global.FetchUpdate();
            infoView.SetUpdateAvailable( updateAvailable );
        }

        public KodoFMMainWindow( WindowManager manager, WindowSettings settings, AudioDatabase database, AudioPlayer player, LastfmInterface lfmInterface, GlobalHotkeys binds ) : base( manager, settings )
        {
            audioDatabase = database;
            audioPlayer = player;
            lastfm = lfmInterface;
            hotkeys = binds;

            updateTimer = new Timer( OnUpdate )
            {
                Interval = (uint)TimeSpan.FromMinutes( 15 ).TotalMilliseconds
            };
            updateTimer.Start();

            contextMenu = new ContextMenu( this ) { TextStyle = Global.TextStyles.Large };
            contextMenu.OnContextMenu += OnContext;
            contextMenu.Add( "Play" );
            contextMenu.Add( "Edit" );
            var deleteMenu = contextMenu.AddSub( "Delete ..." );
            deleteMenu.TextStyle = Global.TextStyles.Large;
            deleteMenu.Add( "Do it!" );
            contextMenu.AddSeparator();
            contextMenu.Add( "Add to queue" );
            var playlistMenu = contextMenu.AddSub( AddToPlaylist );
            playlistMenu.TextStyle = Global.TextStyles.Large;
            playlistMenu.OnContextMenu += OnContextPlaylist;
            playlistMenu.Add( CreatePlaylist );
            playlistMenu.AddSeparator();
            contextMenu.AddSeparator();
            var sortingMenu = contextMenu.AddSub( "Sort ..." );
            sortingMenu.TextStyle = Global.TextStyles.Large;
            sortingMenu.Add( "Path (Decending)" );
            sortingMenu.Add( "Path (Ascending)" );
            sortingMenu.AddSeparator();
            sortingMenu.Add( "Artist (Decending)" );
            sortingMenu.Add( "Artist (Ascending)" );
            sortingMenu.AddSeparator();
            sortingMenu.Add( "Album (Decending)" );
            sortingMenu.Add( "Album (Ascending)" );

            foreach ( var pls in audioDatabase.GetPlaylists() )
            {
                playlistMenu.Add( pls.Name );
            }

            buttonTime = new Button( this )
            {
                TextStyle = Global.TextStyles.Small,
                Text = "",
                Locked = true
            };
            buttonBack = new Button( this )
            {
                Icon = Global.Icons.Back,
                Tip = "Navigate back"
            };
            buttonBack.OnClick += OnBack;
            buttonNow = new Button( this )
            {
                Icon = Global.Icons.Now,
                Tip = "Music playing right now"
            };
            buttonNow.OnClick += OnNow;
            buttonTracks = new Button( this )
            {
                Icon = Global.Icons.Tracks,
                Tip = "All music"
            };
            buttonTracks.OnClick += OnTracks;
            buttonArtists = new Button( this )
            {
                Icon = Global.Icons.Artists,
                Tip = "All music grouped by artist"
            };
            buttonArtists.OnClick += OnArtists;
            buttonAlbums = new Button( this )
            {
                Icon = Global.Icons.Albums,
                Tip = "All music grouped by album"
            };
            buttonAlbums.OnClick += OnAlbums;
            buttonPlaylists = new Button( this )
            {
                Icon = Global.Icons.Playlists,
                Tip = "User created playlists"
            };
            buttonPlaylists.OnClick += OnPlaylists;
            buttonSettings = new Button( this )
            {
                Locked = true,
                Tip = "Settings",
                Icon = Global.Icons.Settings
            };
            buttonSettings.OnClick += OnSettings;
            buttonNavi = new Button( this )
            {
                Locked = true,
                TextStyle = Global.TextStyles.BigAndBold,
                TextAlignment = TextStyleAlignment.LeftCenter
            };
            buttonPlay = new Button( this )
            {
                Tip = "Play",
                Icon = Global.Icons.Play
            };
            buttonPlay.OnClick += OnPlay;
            buttonBrw = new Button( this )
            {
                Tip = "Previous",
                Icon = Global.Icons.Backward
            };
            buttonBrw.OnClick += OnBrw;
            buttonFrw = new Button( this )
            {
                Tip = "Next",
                Icon = Global.Icons.Forward
            };
            buttonFrw.OnClick += OnFrw;
            buttonShuffle = new Button( this )
            {
                Tip = "Shuffle",
                ToggleOnClick = true,
                Toggled = player.Shuffle,
                Icon = Global.Icons.Shuffle
            };
            buttonShuffle.OnClick += OnShuffle;
            buttonRepeat = new Button( this )
            {
                Tip = "Repeat",
                ToggleOnClick = true,
                Toggled = player.Repeat,
                Icon = Global.Icons.Repeat
            };
            buttonRepeat.OnClick += OnRepeat;

            buttonMute = new Button( this )
            {
                Icon = Global.Icons.VolumeMute,
                ToggleOnClick = true,
                Toggled = player.Mute,
                Tip = "Mute",
            };
            buttonMute.OnClick += OnMute;

            barSeek = new Trackbar( this )
            {
                Style = TrackbarStyle.TrackRegular | TrackbarStyle.ThumbAutoHide
            };
            barSeek.Set( (int)(player.Stream.Position / 1000u), 0, (int)(player.Stream.Length / 1000u) );
            barSeek.Scroll += OnSeekSlide;
            barVolume = new Trackbar( this )
            {
                Style = TrackbarStyle.TrackRegular | TrackbarStyle.ThumbAutoHide
            };
            barVolume.Set( (int)(player.Volume * 100), 0, 100 );
            barVolume.Scroll += OnVolumeSlide;
            barPlaylist = new Trackbar( this )
            {
                Style = TrackbarStyle.TrackFull,
                Orientation = TrackbarOrientation.Vertical
            };

            var listViewLateralWidth = Global.Config.Appearance.Slimming ? Global.Config.Appearance.SlimmingWidth : 0;

            artistsView = new ArtistsView( this )
            {
                Visible = false,
                MaxItemWidth = listViewLateralWidth,
                AllowActivation = true,
                TextStyle = Global.TextStyles.Large
            };
            artistsView.OnItemActivated += ArtistsView_OnItemActivated;
            albumsView = new AlbumsView( this )
            {
                Visible = false,
                MaxItemWidth = listViewLateralWidth,
                AllowActivation = true,
                TextStyle = Global.TextStyles.Large
            };
            albumsView.OnItemActivated += AlbumsView_OnItemActivated;
            playlistsView = new PlaylistsView( this )
            {
                Visible = false,
                MaxItemWidth = listViewLateralWidth,
                AllowActivation = true,
                TextStyle = Global.TextStyles.Large
            };
            playlistsView.OnItemActivated += playlistsView_OnPlaylistActivated;

            var infoViewMenu = new ContextMenu( this );
            infoViewMenu.TextStyle = Global.TextStyles.Large;
            infoViewMenu.Add( LoadAlbumArtFromLastfm );
            infoViewMenu.Add( LoadAlbumArtFromLocal );
            infoViewMenu.OnContextMenu += OnInfoContext;

            infoView = new InfoView( this )
            {
                TextStyle = Global.TextStyles.Large,
                Menu = infoViewMenu
            };
            infoView.OnUpdateClick += OnUpdateClick;

            audioView = new AudioView( this )
            {
                Player = player,
                TextStyle = Global.TextStyles.Large
            };
            playlistView = new PlaylistView( this )
            {
                MaxItemWidth = listViewLateralWidth,
                TextStyle = Global.TextStyles.Large,
                AllowActivation = true,
                AllowMoving = true,
                Menu = contextMenu,
            };
            playlistView.OnItemActivated += playlistView_OnItemActivated;

            //var listingViewMenu = new ContextMenu( this );
            //listingViewMenu.TextStyle = Global.TextStyles.Large;
            //listingViewMenu.

            listingView = new AudioListingView( this );
            listingView.TextStyle = Global.TextStyles.Large;
            listingView.OnUserActivatedItem += OnUserActivatedListing;
            listingView.OnUserCloudLoad += OnListingCloudLoad;
            listingView.OnUserLocalLoad += OnUserListingLocalLoad;
        }

        async void OnUserListingLocalLoad( string album, string artist )
        {
            await CacheAlbumArtFromLocal( album, artist );
            listingView.Reload();

            if ( audioPlayer.Stream.Info.Album == album )
            {
                infoView.SetView( audioPlayer.Stream, audioPlayer.Playlist.Name );
            }
        }

        async void OnListingCloudLoad( string album, string artist )
        {
            if ( audioPlayer.Stream.Info.Album == album )
            {
                // If this is the same album as the currently playing, we can update the current stream too.
                await CacheAlbumArtFromLastfm( album, artist, audioPlayer.Stream.Info );

                infoView.SetView( audioPlayer.Stream, audioPlayer.Playlist.Name );
            }
            else
            {
                await CacheAlbumArtFromLastfm( album, artist );
            }

            listingView.Reload();
        }

        void OnUserActivated( AudioID item )
        {
            switch ( stateOfUI )
            {
                case UIState.ArtistListing:
                case UIState.AlbumListing:
                    if ( listingView.Name == audioPlayer.Playlist.Name )
                    {
                        listingView.ActivateItem( audioDatabase.Get( item ) );
                    }
                    break;
            }
        }

        async Task CacheAlbumArtFromLastfm( string album, string artist, AudioInfo audioInfo = null )
        {
            var albumInfo = await lastfm.GetAlbumInfoAsync( artist, album );

            if ( albumInfo.IsOK )
            {
                var albumImagesUris = albumInfo.Data.Images;

                if ( albumImagesUris.ContainsKey( "mega" ) )
                {
                    var imageUri = albumImagesUris[ "mega" ];
                    var request = WebRequest.CreateHttp( imageUri );

                    try
                    {
                        var response = await request.GetResponseAsync();
                        var responseBitmap = Task.Run( () => new System.Drawing.Bitmap( response.GetResponseStream() ?? System.IO.Stream.Null ) );

                        using ( var albumArt = await responseBitmap )
                        {
                            AudioCache.CacheAlbumArt( album, artist, albumArt );

                            if ( audioInfo != null )
                            {
                                await audioPlayer.WriteStreamInfoAsync( audioInfo, albumArt );
                            }
                        }
                    }
                    catch ( Exception e )
                    {
                        Global.Log.CallerError( e.Message, e.ToString() );
                    }
                }
            }
        }

        async Task CacheAlbumArtFromLocal( string album, string artist, AudioInfo audioInfo = null )
        {
            using ( var dlg = new OpenFileDialog() )
            {
                var filename = dlg.Open( $"Choose album artwork for {artist} | {album}", AlbumArtTypeFilter, ".jpg" );

                try
                {
                    var localBitmap = Task.Run( () => new System.Drawing.Bitmap( filename ) );

                    using ( var albumArt = await localBitmap )
                    {
                        AudioCache.CacheAlbumArt( album, artist, albumArt );

                        if ( audioInfo != null )
                        {
                            await audioPlayer.WriteStreamInfoAsync( audioInfo, albumArt );
                        }
                    }
                }
                catch ( Exception e )
                {
                    Global.Log.CallerError( e.Message, e.ToString() );
                }
            }
        }

        void SetCurrentPlaylist( AudioPlaylist playlist )
        {
            audioPlayer.Playlist.OnActivated -= OnUserActivated;
            audioPlayer.Playlist = playlist;
            audioPlayer.Playlist.OnActivated += OnUserActivated;
        }

        void OnUserActivatedListing( AudioItem item )
        {
            var plsName = listingView.Name;

            if ( plsName.StartsWith( AudioDatabase.ArtistPrefix ) )
            {
                var artistPlaylist = audioDatabase.CreatePlaylist( plsName );

                if ( audioPlayer.Playlist.Name != plsName )
                {
                    artistPlaylist.RemoveAll();
                    artistPlaylist.AddRange( audioDatabase.GetTracksFromArtist( item.Artist ) );

                    SetCurrentPlaylist( artistPlaylist );
                }

                artistPlaylist.SetActiveIndex( artistPlaylist.IndexOf( item ) );
            }
            else if ( plsName.StartsWith( AudioDatabase.AlbumPrefix ) )
            {
                var albumPlaylist = audioDatabase.CreatePlaylist( plsName );

                if ( audioPlayer.Playlist.Name != plsName )
                {
                    albumPlaylist.RemoveAll();
                    albumPlaylist.AddRange( audioDatabase.GetTracksFromAlbum( item.Album ) );

                    SetCurrentPlaylist( albumPlaylist );
                }

                albumPlaylist.SetActiveIndex( albumPlaylist.IndexOf( item ) );
            }
        }

        async void OnInfoContext( ContextMenuItem item )
        {
            if ( item == null )
                return;

            var currentStream = audioPlayer.Stream;

            if ( item.Text == LoadAlbumArtFromLastfm )
            {
                await CacheAlbumArtFromLastfm( currentStream.Info.Album, currentStream.Info.Artist, currentStream.Info );
            }
            else if ( item.Text == LoadAlbumArtFromLocal )
            {
                await CacheAlbumArtFromLocal( currentStream.Info.Album, currentStream.Info.Artist, currentStream.Info );
            }

            if ( audioPlayer.Stream.ID == currentStream.ID )
            {
                infoView.SetView( currentStream, audioPlayer.Playlist.Name );
            }

            if ( stateOfUI == UIState.ArtistListing || stateOfUI == UIState.AlbumListing )
            {
                listingView.Reload();
            }
        }

        // OnCreated
        //
        // Sets up all non-UI components and their event handlers and asychronously refreshes the database.
        //______________________________________________________________________________________

        protected override async void OnCreated()
        {
            base.OnCreated();

            var settingsWindowSettings = new WindowSettings
            {
                Margings = new WindowMargings( 3 ),
                NoTitle = true,
                ToolWindow = true
            };
            settingsWindowSettings.MinimumSize = new Size( 100, 100 );
            settingsWindowSettings.Area = new Rectangle( new Size( 570, 420 ) ).CenterTo( Area );
            settingsWindow = new KodoFMSettingsWindow( Manager, settingsWindowSettings );
            settingsWindow.TextStyle = Global.TextStyles.Large;
            settingsWindow.Visible = false;
            settingsWindow.OnClosed += SettingsWindow_OnClosed;
            settingsWindow.Create();

            var promtWindowSettings = new WindowSettings
            {
                Margings = new WindowMargings( 3 ),
                NoTitle = true,
                ToolWindow = true
            };
            promtWindowSettings.MinimumSize = new Size( 100, 100 );
            promtWindowSettings.Area = new Rectangle( new Size( 370, 120 ) ).CenterTo( Area );
            promtWindow = new PromtWindow( Manager, promtWindowSettings );
            promtWindow.TextStyle = Global.TextStyles.Large;
            promtWindow.Visible = false;
            promtWindow.Create();

            var editWindowSettings = new WindowSettings
            {
                Margings = new WindowMargings( 3 ),
                NoTitle = true,
                ToolWindow = true
            };
            editWindowSettings.MinimumSize = new Size( 370, 220 );
            editWindowSettings.Area = new Rectangle( new Size( 370, 220 ) ).CenterTo( Area );
            editWindow = new KodoFMEditWindow( Manager, editWindowSettings, audioPlayer, audioDatabase );
            editWindow.TextStyle = Global.TextStyles.Large;
            editWindow.Visible = false;
            editWindow.OnClosed += OnEditWindowClosed;
            editWindow.Create();

            var logWindowSettings = new WindowSettings
            {
                Margings = new WindowMargings( 3 ),
                NoTitle = true,
                ToolWindow = true
            };
            logWindowSettings.MinimumSize = new Size( 371, 220 );
            logWindowSettings.Area = new Rectangle( new Size( 500, 540 ) ).CenterTo( Area );
            logWindow = new KodoFMLogWindow( Manager, logWindowSettings );
            logWindow.TextStyle = Global.TextStyles.Large;
            logWindow.Visible = false;
            logWindow.OnClosed += OnLogWindowClosed;
            logWindow.Create();

            SetIcon( Resources.kodofm );

            hotkeys.OnKey += OnGlobalKey;

            audioDatabase.OnItemModified += OnDatabaseItemChanged;

            audioPlayer.OnStreamBegin += OnStreamBegin;
            audioPlayer.OnHeartbeat += OnStreamHeartbeat;
            audioPlayer.OnStreamEnd += OnStreamEnd;

            SetCurrentPlaylist( audioPlayer.Playlist );

            infoView.SetView( audioPlayer.Stream, audioPlayer.Playlist != AudioPlaylist.Empty ? audioPlayer.Playlist.Name : string.Empty );
            playlistView.Playlist = audioPlayer.Playlist;

            if ( stateStack.Count > 0 )
            {
                var uiState = stateStack.Pop();
                SwitchUIState( uiState.State, uiState.Data );
            }
            else
            {
                SwitchUIState( UIState.Now );
            }

            OnUpdate();

            await audioDatabase.RefreshAsync();
        }

        protected override void OnUpdate( Context context )
        {
            base.OnUpdate( context );

            var sidebarWidth = 50;
            var client = Client;
            var buttonFrame = new Rectangle( client.Left, client.Bottom - ButtonSize.Height, client.Left + ButtonSize.Width, client.Bottom );

            infoView.Area = new Rectangle( client.Left, buttonFrame.Top - 120, client.Left + (client.Width / 2), buttonFrame.Top );
            audioView.Area = new Rectangle( infoView.Area.Right, infoView.Area.Top, client.Right, infoView.Area.Bottom );

            buttonBack.Area = new Rectangle( client.Left, client.Top, client.Left + sidebarWidth, client.Top + 34 );

            buttonNavi.Area = new Rectangle( buttonBack.Area.Right + 1, client.Top, client.Right - 35, client.Top + 34 );
            buttonSettings.Area = new Rectangle( client.Right - 34, client.Top, client.Right, client.Top + 34 );

            playlistView.Area = new Rectangle( client.Left + sidebarWidth, buttonNavi.Area.Bottom, client.Right - 10, infoView.Area.Top );
            listingView.Area = new Rectangle( client.Left + sidebarWidth, buttonNavi.Area.Bottom, client.Right - 10, infoView.Area.Top );

            var homeViewArea = new Rectangle( client.Left, client.Top + 34, playlistView.Area.Left, infoView.Area.Top );
            var homeViewAreaSlice = new Rectangle( homeViewArea.Left, homeViewArea.Top, homeViewArea.Right, homeViewArea.Top + (homeViewArea.Height / 4) );

            buttonTracks.Area = homeViewAreaSlice;
            homeViewAreaSlice = homeViewAreaSlice.Move( 0, homeViewAreaSlice.Height );
            buttonArtists.Area = homeViewAreaSlice;
            homeViewAreaSlice = homeViewAreaSlice.Move( 0, homeViewAreaSlice.Height );
            buttonAlbums.Area = homeViewAreaSlice;
            homeViewAreaSlice = homeViewAreaSlice.Move( 0, homeViewAreaSlice.Height );
            buttonPlaylists.Area = homeViewAreaSlice;

            artistsView.Area = playlistView.Area;
            albumsView.Area = playlistView.Area;
            playlistsView.Area = playlistView.Area;

            barPlaylist.Area = new Rectangle( playlistView.Area.Right, playlistView.Area.Top, playlistView.Area.Right + 10, playlistView.Area.Bottom );

            buttonPlay.Area = buttonFrame;
            buttonFrame = buttonFrame.Move( buttonFrame.Width, 0 );
            buttonBrw.Area = buttonFrame;
            buttonFrame = buttonFrame.Move( buttonFrame.Width, 0 );
            buttonFrw.Area = buttonFrame;
            buttonFrame = buttonFrame.Move( buttonFrame.Width, 0 );
            buttonShuffle.Area = buttonFrame;
            buttonFrame = buttonFrame.Move( buttonFrame.Width, 0 );
            buttonRepeat.Area = buttonFrame;

            barVolume.Area = new Rectangle( client.Right - 100, buttonFrame.Top, client.Right, buttonFrame.Bottom );
            buttonMute.Area = new Rectangle( barVolume.Area.Left - ButtonSize.Width, buttonFrame.Top, barVolume.Area.Left, buttonFrame.Bottom );
            buttonNow.Area = new Rectangle( buttonMute.Area.Left - ButtonSize.Width, buttonFrame.Top, buttonMute.Area.Left, buttonFrame.Bottom );
            buttonTime.Area = new Rectangle( buttonNow.Area.Left - 80, buttonFrame.Top, buttonNow.Area.Left, buttonFrame.Bottom );
            barSeek.Area = new Rectangle( buttonFrame.Right, buttonFrame.Top, buttonTime.Area.Left, buttonFrame.Bottom );
        }

        void OnBack()
        {
            NavigateBack();
        }

        void OnContext( ContextMenuItem item )
        {
        }

        void OnContextPlaylist( ContextMenuItem item )
        {
            if ( item.Text == CreatePlaylist )
            {
                Locked = true;
                promtWindow.ShowText( OnPromtWindowClosed, "Enter the name of the new playlist" );
            }
            else
            {
                var playlistName = item.Text;
                var playlist = audioDatabase.GetPlaylist( playlistName );

                if ( playlist != AudioPlaylist.Empty )
                {
                    playlist.AddRange( playlistView.GetSelectedItems() );
                }
            }
        }

        void OnNow()
        {
            SwitchUIState( UIState.Now );
        }

        void OnTracks()
        {
            SwitchUIState( UIState.All );
        }

        void OnArtists()
        {
            SwitchUIState( UIState.Artists );
        }

        void OnAlbums()
        {
            SwitchUIState( UIState.Albums );
        }

        void OnPlaylists()
        {
            SwitchUIState( UIState.Playlists );
        }

        void OnSettings()
        {
            settingsWindow.Visible = true;
            Locked = true;
        }

        void playlistView_OnItemActivated( AudioItem item )
        {
            if ( audioPlayer.Playlist != playlistView.Playlist )
            {
                audioPlayer.Playlist = playlistView.Playlist;
                audioPlayer.Replay();
            }
        }

        void ArtistsView_OnItemActivated( AudioListingArtist item )
        {
            SwitchUIState( UIState.ArtistListing, item.Name );
        }

        void AlbumsView_OnItemActivated( AudioListingAlbum item )
        {
            SwitchUIState( UIState.AlbumListing, item.Name );
        }

        void playlistsView_OnPlaylistActivated( AudioPlaylist pls )
        {
            SwitchUIState( UIState.PlaylistsActivated, pls.Name );
        }

        void NavigateClear()
        {
            stateStack.Clear();
        }

        void NavigateBack()
        {
            if ( stateStack.Count > 1 )
            {
                stateStack.Pop();
                var uiState = stateStack.Pop();
                SwitchUIState( uiState.State, uiState.Data );
            }
        }

        void SwitchUIState( UIState state, string data = null )
        {
            stateOfUI = state;
            stateStack.Push( new UIStateMemory( state, data ) );

            buttonTracks.Toggled = false;
            buttonArtists.Toggled = false;
            buttonAlbums.Toggled = false;
            buttonPlaylists.Toggled = false;

            buttonNavi.Visible = false;
            listingView.Trackbar = null;
            listingView.Visible = false;
            playlistView.Trackbar = null;
            playlistView.Visible = false;
            playlistsView.Visible = false;
            playlistsView.Trackbar = null;
            artistsView.Visible = false;
            artistsView.Trackbar = null;
            albumsView.Visible = false;
            albumsView.Trackbar = null;

            switch ( stateOfUI )
            {
                case UIState.Now:
                    buttonNavi.Visible = true;

                    if ( audioPlayer.Playlist.Name.StartsWith( AudioDatabase.ArtistPrefix ) )
                    {
                        var artistName = audioPlayer.Playlist.Name.Substring( AudioDatabase.ArtistPrefix.Length );

                        buttonNavi.Text = $"Home / Artists / {artistName}";

                        listingView.Visible = true;
                        listingView.Trackbar = barPlaylist;
                        listingView.SetArtistListing( audioDatabase.GetArtistListing( artistName ) );
                        listingView.ActivateItem( audioPlayer.Playlist.GetActiveItem() );

                        stateOfUI = UIState.ArtistListing;
                        stateStack.Pop();
                        stateStack.Push( new UIStateMemory( stateOfUI, artistName ) );
                    }
                    else if ( audioPlayer.Playlist.Name.StartsWith( AudioDatabase.AlbumPrefix ) )
                    {
                        buttonNavi.Text = $"Home / Albums / {audioPlayer.Playlist.Name.Substring( AudioDatabase.AlbumPrefix.Length )}";

                        var albumName = audioPlayer.Playlist.Name.Substring( AudioDatabase.ArtistPrefix.Length );

                        buttonNavi.Text = $"Home / Albums / {albumName}";

                        listingView.Visible = true;
                        listingView.Trackbar = barPlaylist;
                        listingView.SetAlbumListing( audioDatabase.GetAlbumListing( albumName ) );
                        listingView.ActivateItem( audioPlayer.Playlist.GetActiveItem() );

                        stateOfUI = UIState.AlbumListing;
                        stateStack.Pop();
                        stateStack.Push( new UIStateMemory( stateOfUI, albumName ) );
                    }
                    else if ( audioPlayer.Playlist.Name == AudioDatabase.MasterPlaylistName )
                    {
                        buttonNavi.Text = "Home / All";
                        playlistView.Visible = true;
                        playlistView.Trackbar = barPlaylist;
                        playlistView.Playlist = audioPlayer.Playlist;

                        stateOfUI = UIState.All;
                        stateStack.Pop();
                        stateStack.Push( new UIStateMemory( stateOfUI ) );
                    }
                    else
                    {
                        buttonNavi.Text = $"Home / Playlists / {audioPlayer.Playlist.Name}";
                        playlistView.Visible = true;
                        playlistView.Trackbar = barPlaylist;
                        playlistView.Playlist = audioPlayer.Playlist;

                        stateOfUI = UIState.PlaylistsActivated;
                        stateStack.Pop();
                        stateStack.Push( new UIStateMemory( stateOfUI, audioPlayer.Playlist.Name ) );
                    }
                    break;
                case UIState.All:
                    buttonTracks.Toggled = true;
                    buttonNavi.Visible = true;
                    buttonNavi.Text = "Home / All";

                    playlistsView.AllowMoving = false;
                    playlistView.Visible = true;
                    playlistView.Trackbar = barPlaylist;
                    playlistView.Playlist = audioDatabase.GetPlaylistMaster();
                    break;
                case UIState.Queue:
                    buttonNavi.Visible = true;
                    buttonNavi.Text = "Home / Queue";

                    playlistView.Visible = true;
                    playlistView.Trackbar = barPlaylist;
                    playlistView.Playlist = audioDatabase.GetPlaylistMaster();
                    break;
                case UIState.Artists:
                    buttonArtists.Toggled = true;
                    buttonNavi.Visible = true;
                    buttonNavi.Text = "Home / Artists";

                    var artists = audioDatabase.GetArtists();

                    artistsView.Visible = true;
                    artistsView.Trackbar = barPlaylist;
                    artistsView.SetArtists( artists );
                    break;
                case UIState.ArtistListing:
                    {
                        buttonArtists.Toggled = true;

                        var artistName = data;

                        buttonNavi.Visible = true;
                        buttonNavi.Text = $"Home / Artists / {artistName}";

                        var artistPlaylistName = $"artist:{artistName}";
                        var artistPlaylist = audioDatabase.CreatePlaylist( artistPlaylistName );

                        if ( audioPlayer.Playlist.Name != artistPlaylistName )
                        {
                            artistPlaylist.RemoveAll();
                            artistPlaylist.AddRange( audioDatabase.GetTracksFromArtist( artistName ) );
                        }

                        listingView.Visible = true;
                        listingView.Trackbar = barPlaylist;
                        listingView.SetArtistListing( audioDatabase.GetArtistListing( artistName ) );
                        listingView.ActivateItem( artistPlaylist.GetActiveItem() );
                        break;
                    }
                case UIState.Albums:
                    buttonAlbums.Toggled = true;
                    buttonNavi.Visible = true;
                    buttonNavi.Text = "Home / Albums";

                    var albums = audioDatabase.GetAlbums();

                    albumsView.Visible = true;
                    albumsView.Trackbar = barPlaylist;
                    albumsView.SetAlbums( albums );
                    break;
                case UIState.AlbumListing:
                    {
                        buttonAlbums.Toggled = true;
                        var albumName = data;

                        buttonNavi.Visible = true;
                        buttonNavi.Text = $"Home / Albums / {albumName}";

                        var albumPlaylist = audioDatabase.CreatePlaylist( $"album:{albumName}" );
                        albumPlaylist.RemoveAll();
                        albumPlaylist.AddRange( audioDatabase.GetTracksFromAlbum( albumName ) );

                        listingView.Visible = true;
                        listingView.Trackbar = barPlaylist;
                        listingView.SetAlbumListing( audioDatabase.GetAlbumListing( albumName ) );
                        listingView.ActivateItem( albumPlaylist.GetActiveItem() );
                    }
                    break;
                case UIState.Playlists:
                    buttonPlaylists.Toggled = true;
                    buttonNavi.Visible = true;
                    buttonNavi.Text = "Home / Playlists";

                    var playlists = audioDatabase.GetPlaylists();

                    playlistsView.Visible = true;
                    playlistsView.Trackbar = barPlaylist;
                    playlistsView.SetPlaylists( playlists );
                    break;
                case UIState.PlaylistsActivated:
                    buttonPlaylists.Toggled = true;

                    var plsName = data;

                    buttonNavi.Visible = true;
                    buttonNavi.Text = $"Home / Playlists / {plsName}";

                    playlistView.Visible = true;
                    playlistView.Trackbar = barPlaylist;
                    playlistView.Playlist = audioDatabase.GetPlaylist( plsName );
                    break;
            }

            playlistView.SelectClear();
            playlistsView.SelectClear();
            artistsView.SelectClear();
            albumsView.SelectClear();

            Update();
        }

        protected override bool OnKeyDown( Key key )
        {
            if ( key == Key.O )
            {
                if ( Keyboard.IsDown( Key.CtrlLeft ) )
                {
                    artistSorter.Order = artistSorter.Order == AudioSortingOrder.Descending ? AudioSortingOrder.Ascending : AudioSortingOrder.Descending;
                    //playlist.Sort( artistSorter );
                }
                else if ( Keyboard.IsDown( Key.ShiftLeft ) )
                {
                    albumSorter.Order = albumSorter.Order == AudioSortingOrder.Descending ? AudioSortingOrder.Ascending : AudioSortingOrder.Descending;
                    //playlist.Sort( albumSorter );
                }
                else if ( Keyboard.IsDown( Key.AltLeft ) )
                {
                    pathSorter.Order = pathSorter.Order == AudioSortingOrder.Descending ? AudioSortingOrder.Ascending : AudioSortingOrder.Descending;
                    //playlist.Sort( pathSorter );
                }

                return true;
            }
            else if ( key == Key.E && Keyboard.IsDown( Key.Ctrl ) && editWindow.Visible == false )
            {
                editWindow.Area = new Rectangle( editWindow.Area.Dimensions ).CenterTo( Area );
                //editWindow.Area = editWindow.Area.CenterTo( Area );
                OpenEditWindow();
                return true;
            }
            else if ( key == Key.L && Keyboard.IsDown( Key.Ctrl ) && logWindow.Visible == false )
            {
                logWindow.Area = new Rectangle( logWindow.Area.Dimensions ).CenterTo( Area );
                //logWindow.Area = logWindow.Area.CenterTo( Area );
                OpenLogWindow();
                return true;
            }
            else
            {
                return base.OnKeyDown( key );
            }
        }

        void OpenEditWindow()
        {
            if ( editWindow.Visible || logWindow.Visible )
                return;

            var selected = playlistView.GetSelectedIDs();

            if ( selected.Count > 0 )
            {
                editWindow.LoadItems( selected );
                editWindow.Visible = true;
                Locked = true;
            }
        }

        void OpenLogWindow()
        {
            if ( !logWindow.Visible && !editWindow.Visible )
            {
                logWindow.Visible = true;
                Locked = true;
            }
        }

        void OnEditWindowClosed()
        {
            Locked = false;

            if ( stateOfUI == UIState.ArtistListing )
            {
                if ( stateStack.Count > 0 )
                {
                    var uiState = stateStack.Peek();
                    var artistName = uiState.Data;

                    if ( !audioDatabase.HasArtist( artistName ) )
                    {
                        stateStack.Pop();
                        NavigateBack();
                    }
                }
            }
            else if ( stateOfUI == UIState.AlbumListing )
            {
                if ( stateStack.Count > 0 )
                {
                    var uiState = stateStack.Peek();
                    var albumName = uiState.Data;

                    if ( !audioDatabase.HasAlbum( albumName ) )
                    {
                        stateStack.Pop();
                        NavigateBack();
                    }
                }
            }
        }

        void SettingsWindow_OnClosed()
        {
            Locked = false;
        }

        void OnLogWindowClosed()
        {
            Locked = false;
        }

        void OnPromtWindowClosed( PromtWindowResult result )
        {
            Locked = false;

            if ( result == PromtWindowResult.Ok )
            {
                var playlistName = promtWindow.Text;

                if ( !string.IsNullOrWhiteSpace( playlistName ) )
                {
                    var playlist = audioDatabase.CreatePlaylist( playlistName );
                    playlist.AddRange( playlistView.GetSelectedItems() );

                    //playlistEditView.Playlist = playlist;
                    //playlistEditing = true;
                    //SwitchUIState( UIState.Playlists );
                }
            }
        }

        void OnPlay()
        {
            if ( audioPlayer.TogglePlayback() )
            {
                buttonPlay.Tip = "Pause";
                buttonPlay.Icon = Global.Icons.Pause;
            }
            else
            {
                buttonPlay.Tip = "Play";
                buttonPlay.Icon = Global.Icons.Play;
            }
        }

        void OnFrw()
        {
            audioPlayer.Next();
        }

        void OnBrw()
        {
            audioPlayer.Previous();
        }

        void OnShuffle()
        {
            audioPlayer.Shuffle = buttonShuffle.Toggled;
        }

        void OnRepeat()
        {
            audioPlayer.Repeat = buttonRepeat.Toggled;
        }

        void OnMute()
        {
            audioPlayer.Mute = buttonMute.Toggled;
        }

        void OnVolumeSlide( int value )
        {
            audioPlayer.Volume = value / 100f;
        }

        void OnSeekSlide( int value )
        {
            if ( !barSeek.ThumbGripped )
            {
                audioPlayer.Seek( (uint)value * 1000u );
            }
        }

        // ________________ Bindings event handler ___________________________________________________________

        void OnGlobalKey( Hotkey key )
        {
            switch ( key.Action )
            {
                case HotkeyName.Play:
                case HotkeyName.Pause:
                    OnPlay();
                    break;
                case HotkeyName.Stop:
                    break;
                case HotkeyName.Forward:
                    OnFrw();
                    break;
                case HotkeyName.Backward:
                    OnBrw();
                    break;
                case HotkeyName.VolumeUp:
                    barVolume.Value = barVolume.Value + 1;
                    OnVolumeSlide( barVolume.Value );
                    break;
                case HotkeyName.VolumeDown:
                    barVolume.Value = barVolume.Value - 1;
                    OnVolumeSlide( barVolume.Value );
                    break;
                case HotkeyName.VolumeMute:
                    buttonMute.Toggled = !buttonMute.Toggled;
                    OnMute();
                    break;
                case HotkeyName.Shuffle:
                    buttonShuffle.Toggled = !buttonShuffle.Toggled;
                    OnShuffle();
                    break;
                case HotkeyName.Repeat:
                    buttonRepeat.Toggled = !buttonRepeat.Toggled;
                    OnRepeat();
                    break;
                case HotkeyName.Back:
                case HotkeyName.Account:
                case HotkeyName.LoveEmpty:
                case HotkeyName.LoveFull:
                case HotkeyName.Home:
                case HotkeyName.Add:
                case HotkeyName.Edit:
                case HotkeyName.Delete:
                case HotkeyName.Right:
                case HotkeyName.Check:
                case HotkeyName.Search:
                case HotkeyName.Settings:
                case HotkeyName.Tracks:
                case HotkeyName.Artists:
                case HotkeyName.Albums:
                case HotkeyName.Playlists:
                case HotkeyName.Now:
                case HotkeyName.Update:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        // ________________ AudioPlayer event handlers  ___________________________________________________________

        void OnStreamBegin( AudioStream stream )
        {
            streamSetAsNowPlaying = false;

            UpdateDatabaseEntryAsync( stream.Info, false );

            if ( stream.Playing )
            {
                buttonPlay.Tip = "Pause";
                buttonPlay.Icon = Global.Icons.Pause;
            }
            else
            {
                buttonPlay.Tip = "Play";
                buttonPlay.Icon = Global.Icons.Play;
            }

            infoView.SetView( stream, audioPlayer.Playlist.Name );

            barSeek.Set( (int)(stream.Position / 1000u), 0, (int)(stream.Info.Length / 1000u) );
        }

        async void OnStreamHeartbeat( AudioStream stream )
        {
            //
            // Update UI.
            //

            buttonTime.Text = MakeTimeString( stream );

            if ( !barSeek.ThumbGripped )
            {
                barSeek.Value = (int)(stream.Position / 1000u);
            }

            //
            // Update last.fm
            //

            if ( !lastfm.LoggedIn || streamSetAsNowPlaying )
                return;

            // Make sure we dont re-enter until done.
            streamSetAsNowPlaying = true;

            // Do the update.
            var result = await lastfm.NowPlayingAsync( stream );

            switch ( result.Result )
            {
                // If everything is OK, update the database entry.
                case LastfmResult.OK:
                    Global.Log.Information( stream.Name, "Set as nowplaying." );

                    if ( result.HasCorrections )
                    {
                        ProcessStreamCorrections( stream, result.Corrections );
                    }
                    break;
                // If we got TryAgainLater or NotPlayedEnough, allow re-entry to try again.
                case LastfmResult.TryAgainLater:
                case LastfmResult.NotPlayedEnough:
                    streamSetAsNowPlaying = false;
                    break;
                case LastfmResult.InvalidStream:
                    Global.Log.Warning( stream.Name, "Failed to set as nowplaying because title/artist/duration is invalid." );
                    break;
                case LastfmResult.NotLoggedIn:
                    Global.Log.Warning( stream.Name, "Failed to set as nowplaying because not logged in." );
                    break;
                case LastfmResult.InvalidCredentials:
                case LastfmResult.InternalError:
                    break;
                default:
                    Global.Log.Error( stream.Name, "Failed to set as nowplaying because of an unknown reason." );
                    break;
            }
        }

        async void OnStreamEnd( AudioStream stream )
        {
            // Update UI.
            infoView.Clear();
            barSeek.Set( 0, 0, 0 );
            buttonPlay.Icon = Global.Icons.Play;

            if ( !lastfm.LoggedIn )
            {
                return;
            }

            // Put this scrobble in to the queue.
            var streamEnqueued = lastfm.Enqueue( stream );

            if ( !streamEnqueued )
            {
                Global.Log.Warning( stream.Name, "Attempted to double enqueue a stream." );
            }

            do
            {
                // Process the scrobble queue.
                AudioStream streamToScrobble;
                var gotStreamToSrobble = lastfm.Dequeue( out streamToScrobble );

                // Successfully get the first scrobble in the queue or do nothing.
                if ( !gotStreamToSrobble || streamToScrobble == AudioStream.Empty )
                {
                    break;
                }

                // Do the scrobble.
                var result = await lastfm.ScrobbleAsync( streamToScrobble );

                switch ( result.Result )
                {
                    case LastfmResult.OK:
                        Global.Log.Information( streamToScrobble.Name, "Scrobbled." );

                        if ( result.HasCorrections )
                        {
                            ProcessStreamCorrections( streamToScrobble, result.Corrections );
                        }
                        break;

                    case LastfmResult.NotPlayedEnough:
                        break;

                    case LastfmResult.TryAgainLater:
                        streamEnqueued = lastfm.Enqueue( streamToScrobble );

                        if ( !streamEnqueued )
                        {
                            Global.Log.Warning( stream.Name, "Attempted to double enqueue a stream." );
                        }
                        else
                        {
                            Global.Log.Warning( streamToScrobble.Name, "Saved for a later scrobble." );
                        }
                        return;

                    case LastfmResult.InvalidStream:
                        Global.Log.Warning( streamToScrobble.Name, "Failed to scrobble because title/artist/duration is invalid." );
                        break;

                    case LastfmResult.NotLoggedIn:
                        Global.Log.Warning( streamToScrobble.Name, "Failed to scrobble because not logged in." );
                        return;

                    case LastfmResult.InvalidCredentials:
                    case LastfmResult.InternalError:
                        break;

                    default:
                        Global.Log.Warning( streamToScrobble.Name, "failed to scrobble because of an unknown reason" );
                        break;
                }
            } while ( lastfm.GetQueueLength() > 0 );
        }

        /// <summary> 
        /// Correct the specified stream's information from the last.fm corrections. 
        /// </summary>
        /// <param name="stream">The stream.</param>
        /// <param name="corrections">The corrections.</param>
        void ProcessStreamCorrections( AudioStream stream, Kodo.Lastfm.API.LastfmCorrections corrections )
        {
            var info = new AudioInfo( stream.Info );
            var logBuilder = new StringBuilder();

            if ( corrections.TrackCorrected )
            {
                logBuilder.Append( $"title corrected from [{info.Title}] to [{corrections.Track}]\n" );
                info.Title = corrections.Track;
            }
            if ( corrections.ArtistCorrected )
            {
                logBuilder.Append( $"artist corrected from [{info.Title}] to [{corrections.Track}]\n" );
                info.Artist = corrections.Artist;
            }
            if ( corrections.AlbumCorrected )
            {
                logBuilder.Append( $"album corrected from [{info.Title}] to [{corrections.Track}]\n" );
                info.Album = corrections.Album;
            }

            // Check if we are actually allowed to use this information.
            if ( Global.Config.Lastfm.Corrections )
            {
                UpdateDatabaseEntryAsync( info, true );
                Global.Log.Information( $"{info.Title} | {info.Artist}", "corrected", logBuilder.ToString() );
            }
            else
            {
                Global.Log.Information( stream.Name, "corrections were available but are disabled", logBuilder.ToString() );
            }
        }

        // ________________ Private methods ___________________________________________________________

        async void UpdateDatabaseEntryAsync( AudioInfo info, bool write )
        {
            // Update the database entry for this item.
            audioDatabase.Set( info.ToAudioItem() );

            if ( write )
            {
                // Write the update to file.
                await audioPlayer.WriteStreamInfoAsync( info );
            }
        }

        void OnDatabaseItemChanged( AudioID item )
        {
            //
            // Update the artist / album tabs.
            //
            switch ( stateOfUI )
            {
                case UIState.Artists:
                    artistsView.SetArtists( audioDatabase.GetArtists() );
                    break;
                case UIState.ArtistListing:
                    //var artistName = playlistView.Playlist.Name.Substring( AudioDatabase.ArtistPrefix.Length );
                    //playlistView.Playlist.AddRange( audioDatabase.GetTracksFromArtist( artistName ) );
                    break;
                case UIState.Albums:
                    albumsView.SetAlbums( audioDatabase.GetAlbums() );
                    break;
                case UIState.AlbumListing:
                    //var albumName = playlistView.Playlist.Name.Substring( AudioDatabase.AlbumPrefix.Length );
                    //playlistView.Playlist.AddRange( audioDatabase.GetTracksFromAlbum( albumName ) );
                    break;
                case UIState.Now:
                case UIState.All:
                case UIState.Queue:
                case UIState.Playlists:
                case UIState.PlaylistsActivated:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            //
            // Update the info view if this is the current stream.
            //
            if ( audioPlayer.Stream.ID == item )
            {
                infoView.SetView( audioPlayer.Stream, audioPlayer.Playlist.Name );
            }
        }

        /// <summary> 
        /// Process the dropped files.
        /// </summary>
        protected override async void OnDragDropped( List<string> filenames )
        {
            audioDatabase.AddRange( await CollectItemsAsync( filenames ) );
            SwitchUIState( UIState.All );
            await audioDatabase.RefreshAsync();
        }

        /// <summary> 
        /// Asyncronously creates <see cref="AudioItem"/>s from a list of paths that can be files and/or folders. Folders are recursively searched all the way down. 
        /// Calls Task.Run so you dont have to. 
        /// </summary>
        /// <param name="filenames">The list of filenames.</param>
        static Task<List<AudioItem>> CollectItemsAsync( IEnumerable<string> filenames )
        {
            return Task.Run( () =>
            {
                var items = new List<AudioItem>();
                CollectItems( filenames, AudioPlayer.SupportedTypes, items );
                return items;
            } );
        }

        /// <summary> 
        /// Creates <see cref="AudioItem"/>s from a list of paths that can be files and/or folders. Folders are recursively searched all the way down. 
        /// </summary>
        /// <param name="paths">The input list.</param>
        /// <param name="allowedExtensions">Allowed extensions.</param>
        /// <param name="result">The resulting list.</param>
        static void CollectItems( IEnumerable<string> paths, string[] allowedExtensions, ICollection<AudioItem> result )
        {
            foreach ( var path in paths )
            {
                if ( System.IO.Directory.Exists( path ) )
                {
                    CollectItems( System.IO.Directory.GetFileSystemEntries( path ), allowedExtensions, result );
                }
                else
                {
                    if ( allowedExtensions.Any( ext => path.EndsWith( ext, StringComparison.OrdinalIgnoreCase ) ) )
                    {
                        result.Add( new AudioItem( path ) );
                    }
                }
            }
        }
    }
}
