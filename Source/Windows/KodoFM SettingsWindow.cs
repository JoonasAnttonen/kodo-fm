﻿using System.Collections.Generic;

using Kodo.Graphics;
using Kodo.Graphics.Style;

namespace KodoFM
{
    class TabsViewTab
    {
        public string Name
        {
            get;
        }

        public List<Control> Controls
        {
            get;
        }

        public TabsViewTab( string name )
        {
            Name = name;
            Controls = new List<Control>();
        }

        public void Add( Control control )
        {
            Controls.Add( control );
        }

        public void Show()
        {
            foreach ( var control in Controls )
                control.Visible = true;
        }

        public void Hide()
        {
            foreach ( var control in Controls )
                control.Visible = false;
        }
    }

    class TabsView : ListView<TabsViewTab>
    {
        TextStyle hugeText;

        List<TabsViewTab> items = new List<TabsViewTab>();
        float itemHeight;

        public TabsViewTab ActiveTab
        {
            get
            {
                var activeTabIndex = GetActiveIndex();

                if ( activeTabIndex != InvalidIndex )
                {
                    return items[ activeTabIndex ];
                }

                return null;
            }
        }

        public TabsView( Window window )
            : base( window )
        {
            AllowSearch = false;
            AllowMoving = false;
            AllowActivation = true;
            SelectionMode = ListViewSelectMode.SingleActivation;

            //items.Add( "General" );
            //items.Add( "Hotkeys" );
            //items.Add( "last.fm" );
            //UpdateIndices();
        }

        public void Add( TabsViewTab tab )
        {
            items.Add( tab );
            Reload();
        }

        protected override float ActualItemHeight
        {
            get { return itemHeight; }
        }

        protected override TabsViewTab Get( int index )
        {
            return items[ index ];
        }

        protected override int GetCount()
        {
            return items.Count;
        }

        protected override void SetActiveIndex( int index )
        {
            var activeTabIndex = GetActiveIndex();

            if ( activeTabIndex != InvalidIndex )
            {
                items[ activeTabIndex ].Hide();
            }

            var activatedTab = items[ index ];
            activatedTab.Show();

            base.SetActiveIndex( index );
        }

        protected override void OnLoad( Context context )
        {
            hugeText = new TextStyle( "Montserrat", 12 );

            base.OnLoad( context );
        }

        protected override void OnUpdate( Context context )
        {
            itemHeight = Area.Dimensions.Height / items.Count;

            base.OnUpdate( context );
        }

        protected override void OnDrawItem( Context context, TabsViewTab item, Rectangle area, bool selected, bool active )
        {
            hugeText.Alignment = TextStyleAlignment.CenterCenter;

            SharedBrush.Color = active ? Color.LightSkyBlue : Color.GhostWhite;
            context.DrawText( item.Name, hugeText, area, SharedBrush );

            SharedStyle.Align( area );
            context.FillRectangle( area, SharedStyle.Foreground );

            SharedBrush.Color = Color.Black;
            context.DrawRectangle( area, SharedBrush, 2 );
        }
    }

    public class KodoFMSettingsWindow : Window
    {
        TabsView tabs;

        Button buttonUser;
        Button buttonPass;

        Button buttonAccept;
        Button buttonCancel;

        Textbox boxUser;
        Textbox boxPass;

        Textbox boxHotPlay;
        Textbox boxHotFrw;
        Textbox boxHotBrw;
        Textbox boxHotShuffle;
        Textbox boxHotRepeat;
        Textbox boxHotMute;
        Textbox boxHotVolumeUp;
        Textbox boxHotVolumeDown;

        string activeTab = string.Empty;

        public event DefaultEventHandler OnClosed;

        public KodoFMSettingsWindow( WindowManager manager, WindowSettings settings )
            : base( manager, settings )
        {
            buttonUser = new Button( this ) { Visible = false };
            buttonPass = new Button( this ) { Visible = false };

            buttonAccept = new Button( this ) { Text = "Accept" };
            buttonCancel = new Button( this ) { Text = "Cancel" };

            boxUser = new Textbox( this ) { Visible = false };
            boxPass = new Textbox( this ) { Visible = false };

            boxHotPlay = new Textbox( this ) { Visible = false };
            boxHotFrw = new Textbox( this ) { Visible = false };
            boxHotBrw = new Textbox( this ) { Visible = false };
            boxHotShuffle = new Textbox( this ) { Visible = false };
            boxHotRepeat = new Textbox( this ) { Visible = false };
            boxHotMute = new Textbox( this ) { Visible = false };
            boxHotVolumeUp = new Textbox( this ) { Visible = false };
            boxHotVolumeDown = new Textbox( this ) { Visible = false };

            tabs = new TabsView( this );
            tabs.OnItemActivated += OnTabActivated;

            var generalTab = new TabsViewTab( "General" );
            tabs.Add( generalTab );
            var hotkeysTab = new TabsViewTab( "Hotkeys" );
            hotkeysTab.Add( boxHotPlay );
            hotkeysTab.Add( boxHotFrw );
            hotkeysTab.Add( boxHotBrw );
            hotkeysTab.Add( boxHotShuffle );
            hotkeysTab.Add( boxHotRepeat );
            hotkeysTab.Add( boxHotMute );
            hotkeysTab.Add( boxHotVolumeUp );
            hotkeysTab.Add( boxHotVolumeDown );
            tabs.Add( hotkeysTab );
            var lastfmTab = new TabsViewTab( "last.fm" );
            lastfmTab.Add( buttonUser );
            lastfmTab.Add( buttonPass );
            lastfmTab.Add( boxUser );
            lastfmTab.Add( boxPass );
            tabs.Add( lastfmTab );
        }

        void OnTabActivated( TabsViewTab item )
        {
            //activeTab = item;
            Update();
        }

        protected override bool OnKeyDown( Key key )
        {
            if ( key == Key.Escape )
            {
                OnClosed?.Invoke();
                Visible = false;
            }

            return base.OnKeyDown( key );
        }

        protected override void OnUpdate( Context context )
        {
            base.OnUpdate( context );

            var client = Client;

            tabs.Area = new Rectangle( client.Left, client.Top, client.Left + 100, client.Bottom );
            buttonAccept.Area = new Rectangle( tabs.Area.Right, client.Bottom - 30, tabs.Area.Right + (client.Right - tabs.Area.Right) / 2, client.Bottom );
            buttonCancel.Area = new Rectangle( buttonAccept.Area.Right, buttonAccept.Area.Top, client.Right, client.Bottom );

            var area = new Rectangle( tabs.Area.Right, client.Top, client.Right, client.Bottom - 30 );
            var boxArea = new Rectangle( new Size( 200, 30 ) ).CenterTo( area );
            boxArea = boxArea.Move( 0, -15 );
            boxUser.Area = boxArea;
            boxArea = boxArea.Move( 0, 30 );
            boxPass.Area = boxArea;

            switch ( tabs.ActiveTab?.Name )
            {
                case "General":
                    break;
                case "Hotkeys":
                    break;
                case "last.fm":
                    break;
                default:
                    break;
            }
        }

        protected override void OnDraw( Context context )
        {
            base.OnDraw( context );

            var client = Client;

            SharedStyle.Align( client );
            context.FillRectangle( client, SharedStyle.Background );

            SharedBrush.Color = Color.Black;
            context.DrawRectangle( client, SharedBrush, 2 );
        }
    }
}
