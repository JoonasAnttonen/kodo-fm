﻿using System;

using Kodo.Graphics;
using Kodo.Graphics.Style;

namespace KodoFM
{
    public enum PromtWindowResult
    {
        Ok,
        Cancel
    }

    public delegate void PromtWindowDelegate( PromtWindowResult result );

    public class PromtWindow : Window
    {
        Button textButton;
        Button okButton;
        Button cancelButton;
        Textbox textBox;

        PromtWindowDelegate resultHandler;

        public string Text
        {
            get { return textBox.Text; }
        }

        public PromtWindow( WindowManager manager, WindowSettings settings )
            : base( manager, settings )
        {
            textButton = new Button( this )
            {
                TextStyle = Global.TextStyles.LargeAndBold,
                Locked = true
            };

            okButton = new Button( this )
            {
                TextStyle = Global.TextStyles.LargeAndBold,
                Text = "OK"
            };
            okButton.OnClick += OnOk;

            cancelButton = new Button( this )
            {
                TextStyle = Global.TextStyles.LargeAndBold,
                Text = "Cancel"
            };
            cancelButton.OnClick += OnCancel;

            textBox = new Textbox( this )
            {
                TextStyle = Global.TextStyles.LargeAndBold
            };
        }

        public void ShowText( PromtWindowDelegate handler, string text, string prefill = "" )
        {
            if ( Visible )
                throw new InvalidOperationException();

            resultHandler = handler;
            textBox.Text = prefill;
            textBox.Visible = true;
            textButton.Text = text;
            okButton.Text = "OK";
            cancelButton.Text = "Cancel";
            Visible = true;
        }

        public void ShowQuestion( PromtWindowDelegate handler, string text, string okText = "Yes", string cancelText = "No" )
        {
            if ( Visible )
                throw new InvalidOperationException();

            okButton.Text = "Yes";
            cancelButton.Text = "No";
            resultHandler = handler;
            textBox.Visible = false;
            textButton.Text = text;
            Visible = true;
        }

        void OnCancel()
        {
            resultHandler?.Invoke( PromtWindowResult.Cancel );
            Visible = false;
        }

        void OnOk()
        {
            resultHandler?.Invoke( PromtWindowResult.Ok );
            Visible = false;
        }

        protected override bool OnKeyDown( Key key )
        {
            if ( key == Key.Escape )
            {
                OnCancel();
            }
            else if ( key == Key.Enter )
            {
                OnOk();
            }

            return base.OnKeyDown( key );
        }

        protected override void OnUpdate( Context context )
        {
            textBox.CaptureKeyboard();

            base.OnUpdate( context );

            var clip = Client;

            if ( textBox.Visible )
            {
                textButton.Area = new Rectangle( clip.Left, clip.Top, clip.Right, clip.Bottom - 30 - 30 );
                textBox.Area = new Rectangle( clip.Left, clip.Bottom - 30 - 30, clip.Right, clip.Bottom - 30 );
            }
            else
            {
                textButton.Area = new Rectangle( clip.Left, clip.Top, clip.Right, clip.Bottom - 30 );
            }

            okButton.Area = new Rectangle( clip.Left, clip.Bottom - 30, clip.Left + clip.Width / 2, clip.Bottom );
            cancelButton.Area = new Rectangle( okButton.Area.Right, okButton.Area.Top, clip.Right, clip.Bottom );
        }
    }
}
