﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using KodoFM.Assets;
using Kodo.Graphics;
using Kodo.Graphics.Style;
using KodoFM.Audio;

namespace KodoFM
{
    public class KodoFMEditWindow : Window
    {
        Button buttonSave;
        Button buttonCancel;
        Button buttonExplorer;

        Textbox boxTitle;
        Textbox boxArtist;
        Textbox boxAlbum;
        Textbox boxGenre;
        Textbox boxYear;
        Textbox boxTrack;

        Button buttonTitle;
        Button buttonArtist;
        Button buttonAlbum;
        Button buttonGenre;
        Button buttonYear;
        Button buttonTrack;

        AudioPlayer player;
        AudioDatabase database;

        bool singleEdit;
        AudioID id;
        List<AudioID> ids;

        string errorMessage;

        Timer workingTimer;
        BitmapAsset workingIcon;
        Bitmap workingBitmap;
        float workingRotation;

        EditState editState = EditState.Normal;

        enum EditState
        {
            Normal,
            Error,
            Working
        }

        public event DefaultEventHandler OnClosed;

        public void LoadItem( AudioID id )
        {
            this.id = id;
            this.ids = null;

            singleEdit = true;

            buttonExplorer.Text = "Show In Folder";
            buttonExplorer.Locked = false;

            var info = AudioDatabase.ReadStreamInfo( id.Data );

            boxTitle.Text = info.Title;
            boxArtist.Text = info.Artist;
            boxAlbum.Text = info.Album;
            boxYear.Text = info.Year;
            boxTrack.Text = info.Track;
            boxGenre.Text = info.Genre;

            buttonTitle.ToggleOnClick = false;
            buttonTitle.Locked = true;
            buttonArtist.ToggleOnClick = false;
            buttonArtist.Locked = true;
            buttonAlbum.ToggleOnClick = false;
            buttonAlbum.Locked = true;
            buttonGenre.ToggleOnClick = false;
            buttonGenre.Locked = true;
            buttonYear.ToggleOnClick = false;
            buttonYear.Locked = true;
            buttonTrack.ToggleOnClick = false;
            buttonTrack.Locked = true;
        }

        public void LoadItems( List<AudioID> ids )
        {
            if ( ids.Count == 0 )
                throw new InvalidOperationException();

            if ( ids.Count == 1 )
            {
                LoadItem( ids[ 0 ] );
                return;
            }

            this.id = AudioID.Empty;
            this.ids = ids;

            singleEdit = false;

            buttonExplorer.Text = "";
            buttonExplorer.Locked = true;

            buttonTitle.ToggleOnClick = true;
            buttonTitle.Locked = false;
            buttonArtist.ToggleOnClick = true;
            buttonArtist.Locked = false;
            buttonAlbum.ToggleOnClick = true;
            buttonAlbum.Locked = false;
            buttonGenre.ToggleOnClick = true;
            buttonGenre.Locked = false;
            buttonYear.ToggleOnClick = true;
            buttonYear.Locked = false;
            buttonTrack.ToggleOnClick = true;
            buttonTrack.Locked = false;

            var info = AudioDatabase.ReadStreamInfo( ids[ 0 ].Data );

            boxTitle.Text = info.Title;
            boxArtist.Text = info.Artist;
            boxAlbum.Text = info.Album;
            boxYear.Text = info.Year;
            boxTrack.Text = info.Track;
            boxGenre.Text = info.Genre;

            for ( var i = 1; i < ids.Count; i++ )
            {
                info = AudioDatabase.ReadStreamInfo( ids[ i ].Data );

                if ( boxTitle.Text != info.Title )
                    boxTitle.Text = string.Empty;
                if ( boxArtist.Text != info.Artist )
                    boxArtist.Text = string.Empty;
                if ( boxAlbum.Text != info.Album )
                    boxAlbum.Text = string.Empty;
                if ( boxYear.Text != info.Year )
                    boxYear.Text = string.Empty;
                if ( boxTrack.Text != info.Track )
                    boxTrack.Text = string.Empty;
                if ( boxGenre.Text != info.Genre )
                    boxGenre.Text = string.Empty;
            }

            if ( boxTitle.Text != string.Empty )
                buttonTitle.Toggled = true;
            if ( boxArtist.Text != string.Empty )
                buttonArtist.Toggled = true;
            if ( boxAlbum.Text != string.Empty )
                buttonAlbum.Toggled = true;
            if ( boxYear.Text != string.Empty )
                buttonYear.Toggled = true;
            if ( boxTrack.Text != string.Empty )
                buttonTrack.Toggled = true;
            if ( boxGenre.Text != string.Empty )
                buttonGenre.Toggled = true;
        }

        public KodoFMEditWindow( WindowManager manager, WindowSettings settings, AudioPlayer player, AudioDatabase database )
            : base( manager, settings )
        {
            this.player = player;
            this.database = database;

            workingTimer = new Timer( workingTimer_OnTick );
            workingTimer.Interval = 10;

            workingIcon = new BitmapAsset( Resources.refresh55, new Size( 64, 64 ) );

            buttonCancel = new Button( this );
            buttonCancel.Text = "Cancel";
            buttonCancel.OnClick += OnCancel;

            buttonSave = new Button( this );
            buttonSave.Text = "Save";
            buttonSave.OnClick += OnSave;

            buttonExplorer = new Button( this );
            buttonExplorer.Text = "Show In Folder";
            buttonExplorer.OnClick += OnExplorerClick;

            boxTitle = new Textbox( this );
            boxTitle.Subtle = true;
            boxArtist = new Textbox( this );
            boxArtist.Subtle = true;
            boxAlbum = new Textbox( this );
            boxAlbum.Subtle = true;
            boxGenre = new Textbox( this );
            boxGenre.Subtle = true;
            boxYear = new Textbox( this );
            boxYear.Subtle = true;
            boxTrack = new Textbox( this );
            boxTrack.Subtle = true;

            buttonTitle = new Button( this );
            buttonTitle.Text = "Title";
            buttonArtist = new Button( this );
            buttonArtist.Text = "Artist";
            buttonAlbum = new Button( this );
            buttonAlbum.Text = "Album";
            buttonGenre = new Button( this );
            buttonGenre.Text = "Genre";
            buttonYear = new Button( this );
            buttonYear.Text = "Year";
            buttonTrack = new Button( this );
            buttonTrack.Text = "Track";
        }

        void workingTimer_OnTick()
        {
            workingRotation -= 10;
            Draw();
        }

        protected override bool OnKeyDown( Key key )
        {
            if ( key == Key.Escape )
            {
                OnCancel();
            }

            return base.OnKeyDown( key );
        }

        protected override void OnLoad( Context context )
        {
            base.OnLoad( context );

            workingBitmap = new Bitmap( context, workingIcon.Source );
        }

        protected override void OnUpdate( Context context )
        {
            base.OnUpdate( context );

            var boxArea = new Rectangle( new Size( 300, 30 ) );
            var textArea = new Rectangle( new Size( 60, 30 ) );
            textArea = new Rectangle( Client.Left, Client.Top, Client.Left + 60, Client.Top + 30 );
            boxArea = new Rectangle( textArea.Right, textArea.Top, Client.Right, textArea.Bottom );

            //boxArea = boxArea.Move( Client.Right - boxArea.Right, Client.Top - boxArea.Top );
            //textArea = textArea.Move( boxArea.Left - textArea.Right, Client.Top - textArea.Top );

            boxTitle.Area = boxArea;
            buttonTitle.Area = textArea;

            boxArea = boxArea.Move( 0, 30 );
            textArea = textArea.Move( 0, 30 );
            boxArtist.Area = boxArea;
            buttonArtist.Area = textArea;

            boxArea = boxArea.Move( 0, 30 );
            textArea = textArea.Move( 0, 30 );
            boxAlbum.Area = boxArea;
            buttonAlbum.Area = textArea;

            boxArea = boxArea.Move( 0, 30 );
            textArea = textArea.Move( 0, 30 );
            boxArea = boxArea.Adjust( -200, 0f );
            boxYear.Area = boxArea;
            buttonYear.Area = textArea;

            boxArea = boxArea.Move( 0, 30 );
            textArea = textArea.Move( 0, 30 );
            boxTrack.Area = boxArea;
            buttonTrack.Area = textArea;

            boxArea = boxArea.Move( 0, 30 );
            textArea = textArea.Move( 0, 30 );
            boxArea = boxArea.Adjust( 200, 0 );
            boxGenre.Area = boxArea;
            buttonGenre.Area = textArea;

            buttonCancel.Area = new Rectangle( Client.Right - 80, Client.Bottom - 30, Client.Right, Client.Bottom );
            buttonSave.Area = new Rectangle( buttonCancel.Area.Left - 80, buttonCancel.Area.Top, buttonCancel.Area.Left, buttonCancel.Area.Bottom );
            buttonExplorer.Area = new Rectangle( textArea.Left, Client.Bottom - 30, buttonSave.Area.Left, Client.Bottom );
        }

        protected override void OnPostDraw( Context context )
        {
            if ( editState == EditState.Working )
            {
                context.AntialiasMode = AntialiasMode.PerPrimitive;
                SharedBrush.Opacity = 0.8f;
                SharedBrush.Color = SharedStyle.Background.Color;

                var overlayArea = new Rectangle( Client.Left, Client.Top, Client.Right, buttonCancel.Area.Top );
                context.FillRectangle( overlayArea, SharedBrush );
                overlayArea = new Rectangle( Client.Left, buttonCancel.Area.Top, buttonCancel.Area.Left, Client.Bottom );
                context.FillRectangle( overlayArea, SharedBrush );

                var iconArea = new Rectangle( new Size( 64, 64 ) ).CenterTo( Client );
                var oldTransform = context.GetTransform();
                context.AntialiasMode = AntialiasMode.Aliased;
                context.SetTransform( Matrix3x2.Rotation( workingRotation, new Point( iconArea.Left + iconArea.Width / 2, iconArea.Top + iconArea.Height / 2 ) ) );
                SharedBrush.Color = Color.IndianRed;
                context.FillOpacityMask( workingBitmap, SharedBrush, iconArea );
                context.SetTransform( oldTransform );
            }
        }

        protected override void OnDraw( Context context )
        {
            base.OnDraw( context );

            var clip = new Rectangle( Area.Dimensions );

            context.AntialiasMode = AntialiasMode.PerPrimitive;
            SharedBrush.Opacity = 1;

            SharedStyle.Align( Client );
            context.FillRectangle( Client, SharedStyle.Background );

            if ( editState == EditState.Error )
            {
                SharedBrush.Color = Color.IndianRed;
                TextStyle.Alignment = TextStyleAlignment.CenterCenter;
                context.DrawText( errorMessage, TextStyle, Client, SharedBrush );
            }
        }

        void Hide()
        {
            Visible = false;
            OnClosed?.Invoke();
        }

        void HideControls()
        {
            foreach ( var control in Controls )
                control.Visible = false;

            buttonCancel.Visible = true;
        }

        void LockControls()
        {
            foreach ( var control in Controls )
                control.Locked = true;

            buttonCancel.Locked = false;
        }

        void UnlockControls()
        {
            foreach ( var control in Controls )
                control.Locked = false;
        }

        bool ValidateFields()
        {
            errorMessage = string.Empty;

            uint dummy;
            bool mustFail = false;

            if ( !string.IsNullOrEmpty( boxYear.Text ) && !uint.TryParse( boxYear.Text, out dummy ) )
            {
                mustFail = true;
                errorMessage += "\n[Year] can only contain numbers.";
            }

            if ( !string.IsNullOrEmpty( boxTrack.Text ) && !uint.TryParse( boxTrack.Text, out dummy ) )
            {
                mustFail = true;
                errorMessage += "\n[Track] can only contain numbers.";
            }

            return (!mustFail);
        }

        void OnExplorerClick()
        {
            if ( singleEdit )
            {
                System.Diagnostics.Process.Start( System.IO.Path.GetDirectoryName( id.Data ) );
            }
        }

        async void OnSave()
        {
            if ( !ValidateFields() )
            {
                editState = EditState.Error;

                buttonCancel.Text = "OK";

                HideControls();

                Draw();
            }
            else if ( singleEdit )
            {
                var info = new AudioInfo( id.Data );
                info.Title = boxTitle.Text;
                info.Artist = boxArtist.Text;
                info.Album = boxAlbum.Text;
                info.Year = boxYear.Text;
                info.Track = boxTrack.Text;
                info.Genre = boxGenre.Text;

                var status = player.WriteStreamInfo( info );

                if ( status == AudioStatus.Ok )
                {
                    database.Set( info.ToAudioItem() );
                }

                Hide();
            }
            else
            {
                editState = EditState.Working;
                LockControls();
                workingTimer.Start();

                foreach ( var id in ids )
                {
                    if ( editState != EditState.Working )
                        break;

                    var info = AudioDatabase.ReadStreamInfo( id.Data );

                    if ( buttonTitle.Toggled )
                        info.Title = boxTitle.Text;
                    if ( buttonArtist.Toggled )
                        info.Artist = boxArtist.Text;
                    if ( buttonAlbum.Toggled )
                        info.Album = boxAlbum.Text;
                    if ( buttonYear.Toggled )
                        info.Year = boxYear.Text;
                    if ( buttonTrack.Toggled )
                        info.Track = boxTrack.Text;
                    if ( buttonGenre.Toggled )
                        info.Genre = boxGenre.Text;

                    var status = await player.WriteStreamInfoAsync( info );

                    if ( status == AudioStatus.Ok )
                    {
                        database.Set( info.ToAudioItem() );
                    }
                }

                editState = EditState.Normal;
                UnlockControls();
                workingTimer.Stop();
                Draw();
                Hide();
            }
        }

        void OnCancel()
        {
            if ( editState == EditState.Error )
            {
                editState = EditState.Normal;
                errorMessage = string.Empty;
                buttonCancel.Text = "Cancel";

                buttonExplorer.Visible = true;
                buttonSave.Visible = true;
                boxTitle.Visible = true;
                boxArtist.Visible = true;
                boxAlbum.Visible = true;
                boxGenre.Visible = true;
                boxYear.Visible = true;
                boxTrack.Visible = true;
                buttonTitle.Visible = true;
                buttonArtist.Visible = true;
                buttonAlbum.Visible = true;
                buttonGenre.Visible = true;
                buttonYear.Visible = true;
                buttonTrack.Visible = true;

                Draw();
            }
            else if ( editState == EditState.Working )
            {
                editState = EditState.Normal;
            }
            else
            {
                Hide();
            }
        }
    }
}
