﻿using System;
using System.Collections.Generic;

using KodoFM.Assets;

using Kodo.Graphics;
using Kodo.Graphics.Style;
using Kodo.Utilities.Logging;

namespace KodoFM
{
    public class LogView : ListView<LogEvent>
    {
        static readonly Size IconSize = new Size( 30, 30 );
        const float wantedItemHeight = 50;
        const float BorderSpacer = 2;
        const float ClipSpacer = 10;

        Bitmap infoBitmap;
        Bitmap warningBitmap;
        Bitmap errorBitmap;

        BitmapAsset infoAsset = new BitmapAsset( Resources.round52, IconSize );
        BitmapAsset warningAsset = new BitmapAsset( Resources.warning37, IconSize );
        BitmapAsset errorAsset = new BitmapAsset( Resources.report, IconSize );

        Log log;

        public Log Log
        {
            get { return log; }
            set
            {
                if ( log != value )
                {
                    if ( log != null )
                        log.OnEvent -= OnEvent;

                    log = value;

                    if ( log != null )
                        log.OnEvent += OnEvent;

                    OnEvent( null );
                }
            }
        }

        public LogView( Window window )
            : base( window )
        {
        }

        protected override void OnLoad( Context context )
        {
            infoBitmap = new Bitmap( context, infoAsset.Source );
            warningBitmap = new Bitmap( context, warningAsset.Source );
            errorBitmap = new Bitmap( context, errorAsset.Source );
        }

        protected override void OnDrawItem( Context context, LogEvent item, Rectangle area, bool selected, bool active )
        {
            //var ev = log[ i ];

            var eventIconClip = new Rectangle( area.Left + BorderSpacer, area.Top + BorderSpacer, area.Left + BorderSpacer + IconSize.Width, area.Top + BorderSpacer + IconSize.Height );
            eventIconClip = eventIconClip.Move( 0, (area.Height - 2 * BorderSpacer - eventIconClip.Height) / 2 );
            var eventNameClip = new Rectangle( eventIconClip.Right + ClipSpacer, area.Top, area.Right - ClipSpacer - 60, area.Top + 30 );
            eventNameClip = eventNameClip.Move( 0, (area.Height - 30) / 2 );
            var eventTimeClip = new Rectangle( eventNameClip.Right + ClipSpacer, area.Top, area.Right - BorderSpacer, area.Bottom );

            SharedBrush.Color = Color.Black;
            context.DrawRectangle( area, SharedBrush, 2 );

            TextStyle.Alignment = TextStyleAlignment.RightCenter;
            SharedBrush.Color = Color.GhostWhite;
            context.DrawText( item.Time.ToLongTimeString(), TextStyle, eventTimeClip, SharedBrush );

            TextStyle.Alignment = TextStyleAlignment.LeftTop;
            SharedBrush.Color = Color.GhostWhite;
            context.DrawText( item.Name, TextStyle, eventNameClip, SharedBrush );

            TextStyle.Alignment = TextStyleAlignment.LeftBottom;
            SharedBrush.Color = Color.SlateGray;
            context.DrawText( item.Brief, TextStyle, eventNameClip, SharedBrush );

            context.AntialiasMode = AntialiasMode.Aliased;

            switch ( item.Severity )
            {
                case LogEventSeverity.Information:
                    SharedBrush.Color = Color.GhostWhite;
                    context.FillOpacityMask( infoBitmap, SharedBrush, eventIconClip );
                    break;
                case LogEventSeverity.Warning:
                    SharedBrush.Color = Color.Goldenrod;
                    context.FillOpacityMask( warningBitmap, SharedBrush, eventIconClip );
                    break;
                case LogEventSeverity.Error:
                    SharedBrush.Color = Color.OrangeRed;
                    context.FillOpacityMask( errorBitmap, SharedBrush, eventIconClip );
                    break;
            }

            context.AntialiasMode = AntialiasMode.PerPrimitive;
        }

        void OnEvent( LogEvent ev )
        {
            Reload();
        }

        protected override float ActualItemHeight
        {
            get { return wantedItemHeight; }
        }
        public override bool AllowMoving
        {
            get { return false; }
            set { }
        }
        public override ListViewSelectMode SelectionMode
        {
            get { return ListViewSelectMode.Single; }
            set { }
        }
        protected override LogEvent Get( int i ) { return log[ i ]; }
        protected override int GetCount() { return log.Count; }
        public override int GetActiveIndex() { return InvalidIndex; }
        protected override void SetActiveIndex( int i ) { }
        protected override void Swap( int x, int y ) { }
        protected override void Remove( List<LogEvent> items ) { }
        protected override bool Match( int index, string search ) { return false; }
    }

    public class KodoFMLogWindow : Window
    {
        LogView logView;
        Trackbar logBar;
        Button buttonOK;

        public event DefaultEventHandler OnClosed;

        public KodoFMLogWindow( WindowManager manager, WindowSettings settings )
            : base( manager, settings )
        {
            buttonOK = new Button( this );
            buttonOK.Text = "OK";
            buttonOK.OnClick += OnOK;

            logBar = new Trackbar( this );
            logBar.Style = TrackbarStyle.TrackFull;
            logBar.Orientation = TrackbarOrientation.Vertical;

            logView = new LogView( this );
            logView.Log = Global.Log;
            logView.Trackbar = logBar;
        }

        protected override bool OnKeyDown( Key key )
        {
            if ( key == Key.Escape )
            {
                OnOK();
            }

            return base.OnKeyDown( key );
        }

        void Hide()
        {
            Visible = false;
            OnClosed?.Invoke();
        }

        void OnOK()
        {
            Hide();
        }

        protected override void OnUpdate( Context context )
        {
            base.OnUpdate( context );

            buttonOK.Area = new Rectangle( Client.Left, Client.Bottom - 30, Client.Right, Client.Bottom );

            logView.Area = new Rectangle( Client.Left, Client.Top, Client.Right - 10, buttonOK.Area.Top );
            logBar.Area = new Rectangle( logView.Area.Right, Client.Top, Client.Right, logView.Area.Bottom );
        }

        protected override void OnDraw( Context context )
        {
            base.OnDraw( context );

            context.AntialiasMode = AntialiasMode.PerPrimitive;
            SharedBrush.Opacity = 1;

            SharedStyle.Align( Client );
            context.FillRectangle( Client, SharedStyle.Background );
        }
    }
}
