# kodo-fm #

A pretty simple music player application with last.fm scrobbling support.

### Why? ###

This is just my personal project for fun.
No tests, no guidelines, no plans, just my organically growing and changing project.
Currently has 1 active user in addition to myself and as such all features of the app are dictated by us.

### How do I get set up? ###

If you really are interested in getting this set up... shouldn't be too much work.

You will need the following repos/binaries from my profile:

**kodo-graphics-2**

**kodo-graphics-2-style**

**kodo-json**

Once you have those set up, you will only need to update the references in the projects to match your directory structure and everything should workout just fine. 

### Contribution guidelines ###

Not looking for any contributions.

### Who do I talk to? ###

Email me at **joonas.anttonen@edu.turkuamk.fi** for any questions.