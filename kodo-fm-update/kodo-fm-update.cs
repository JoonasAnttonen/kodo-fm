﻿using System;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;

//
// Assembly information.
//
[assembly: Guid( "625ac147-eaec-4016-9432-8252dd1f62d9" )]
[assembly: AssemblyTitle( "kodo-fm-update" )]
[assembly: AssemblyVersion( "1.0.0.*" )]
[assembly: AssemblyCopyright( "Copyright © Joonas Anttonen 2016" )]

namespace kodo_fm_update
{
    internal class Program
    {
        static readonly string TargetGuid ="3c85459f-408b-4db4-bfa8-58b6e3676adb";
        static readonly string Target = "kodo-fm";
        static readonly string TargetPath = $"..\\";
        static readonly string TargetExecutablePath = $"{TargetPath}{ Target }.exe";
        static readonly string TargetBackupPath = $"{ Target }-backup.zip";
        static readonly string TargetUpdatePath = $"{ Target }-update.zip";

        static bool IsUniqueInstance( Mutex mutex )
        {
            try
            {
                if ( !mutex.WaitOne( 0, false ) )
                {
                    return false;
                }
            }
            catch ( AbandonedMutexException ) { }

            return true;
        }

        static void Remove( string path )
        {
            if ( File.Exists( path ) )
            {
                Console.WriteLine( $"Removing { path }" );
                File.Delete( path );
            }
        }

        static void Main( string[] args )
        {
            using ( var mutex = new Mutex( false, TargetGuid ) )
            {
                Console.WriteLine( "Kodo.Updater" );
                Console.WriteLine( "Copyright © Joonas Anttonen 2016" );
                Console.WriteLine();

                if ( args.Length > 0 &&
                     args[ 0 ] == "wait" )
                {
                    Console.WriteLine( $"Waiting for { Target } to shutdown ..." );

                    try
                    {
                        if ( !mutex.WaitOne( (int)TimeSpan.FromSeconds( 20 ).TotalMilliseconds, false ) )
                        {
                            Console.WriteLine( $"Failed! ({ Target } refused to shutdown)" );
                            Console.WriteLine( "Press ENTER to exit ..." );
                            Console.ReadLine();
                            return;
                        }
                    }
                    catch ( AbandonedMutexException ) { }

                    Thread.Sleep( 3000 );
                }

                if ( IsUniqueInstance( mutex ) )
                {
                    Console.WriteLine( $"Updating { Target } ..." );

                    try
                    {
                        if ( File.Exists( TargetUpdatePath ) )
                        {
                            using ( var backupStream = new FileStream( TargetBackupPath, FileMode.Create ) )
                            using ( var backupArchive = new ZipArchive( backupStream, ZipArchiveMode.Update ) )
                            using ( var archive = ZipFile.OpenRead( TargetUpdatePath ) )
                            {
                                foreach ( var entry in archive.Entries )
                                {
                                    var entryPath = Path.Combine( TargetPath, entry.Name );

                                    if ( File.Exists( entryPath ) )
                                    {
                                        backupArchive.CreateEntryFromFile( entryPath, entry.Name );
                                    }

                                    Remove( entryPath );

                                    Console.WriteLine( $"Extracting { entryPath }" );
                                    entry.ExtractToFile( entryPath );
                                }
                            }

                            Remove( TargetUpdatePath );

                            Console.WriteLine( "Succeeded!" );
                        }
                        else
                        {
                            Console.WriteLine( $"Failed! (Update data not found)" );
                            Console.WriteLine( $"Press any key to run { Target } ..." );
                            Console.ReadLine();
                        }
                    }
                    catch ( Exception e )
                    {
                        Console.WriteLine( $"Failed! ({ e.Message })" );
                        Console.WriteLine( $"Press any key to run { Target } ..." );
                        Console.ReadLine();
                    }
                }
                else
                {
                    Console.WriteLine( $"Failed! ({ Target } was running)" );
                    Console.WriteLine( "Press ENTER to exit ..." );
                    Console.ReadLine();
                    return;
                }
            }

            Console.WriteLine( $"Starting { Path.GetFullPath( TargetExecutablePath ) } ..." );

            if ( File.Exists( Path.GetFullPath( TargetExecutablePath ) ) )
            {
                var processStartInfo = new ProcessStartInfo();
                processStartInfo.FileName = Path.GetFullPath( TargetExecutablePath );
                processStartInfo.WorkingDirectory = TargetPath;
                Process.Start( processStartInfo );
            }
            else
            {
                Console.WriteLine( $"Failed! ({ Target } did not exist?)" );
                Console.WriteLine( "Press ENTER to exit ..." );
                Console.ReadLine();
            }
        }
    }
}
